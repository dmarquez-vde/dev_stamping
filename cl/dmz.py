# -*- coding: utf-8 -*-

from funciones import *
from suds.client import Client

#el_json = [{u'shippingAddress': {u'province': u'REGION METROPOLITANA DE SANTIAGO', u'address': u'DDDD222', u'consignee': u'A1 DD', u'street': None, u'orderNo': u'28010118522', u'id': 5755599, u'city': u'QUILICURA', u'district': None, u'zipcode': None, u'type': 0, u'email': None, u'changeAskId': None, u'orderId': 8114729, u'description': None, u'deleteFlag': 0, u'lastUpdatedBy': u'system', u'statecode': None, u'createdBy': u'system', u'ruleName': u'CLHW001normal51', u'creationDate': u'2020-07-16 20:15:07+0800', u'mobile': u'333333333', u'country': u'Chile', u'lastUpdateDate': u'2020-07-16 20:15:07+0800', u'systemName': u'hic', u'companyCode': None, u'dimensionCode': u'28010118522'}, u'paymentItems': [{u'orderId': 8114729, u'description': None, u'paymentNo': u'11111111111', u'lastUpdateDate': u'2020-07-16 20:15:06+0800', u'companyCode': None, u'deleteFlag': 0, u'paymentType': u'0', u'amount': 0, u'lastUpdatedBy': u'system', u'systemName': u'hic', u'paymentDate': u'2020-07-16 16:37:27+0800', u'createdBy': u'system', u'ruleName': u'CLHW001normal51', u'orderNo': u'28010118522', u'creationDate': u'2020-07-16 20:15:06+0800', u'id': 4153213, u'dimensionCode': u'28010118522'}], u'trackId': 3920703, u'company': {u'defaultApplyStatus': 99, u'registeredOfficeAddress': None, u'channelId': u'b8f4fc686dd448948079c8ee19f92aaf', u'companyRegistrationNumber': None, u'seCode': None, u'contactEmail': None, u'createdBy': None, u'registeredAddressStateCode': None, u'id': 163, u'companyRea': None, u'companyTradeRegistrationNo': None, u'idealCompanyCode': u'IN-KP000014', u'companyDistrict': None, u'companyRegisteredTelephone': None, u'gstin': None, u'companyRegisteredCapital': None, u'parentCompanyName': None, u'pan': None, u'status': 1, u'companyCity': None, u'description': u'\u667a\u5229', u'companyName': u'Huawei (Chile) S.A.', u'companyRegisteredAddress': None, u'companyDepositBank': None, u'lastUpdatedBy': None, u'companyRegisterionNo': None, u'companyBankAccount': None, u'companyTaxRegistrationNo': None, u'creationDate': None, u'companyCountry': None, u'companyCode': u'CLHW001', u'companyAptNo': None, u'cin': None, u'companyProvince': None, u'lastUpdateDate': None, u'contactTelephone': None, u'companyWebsite': None}, u'originalTrackId': None, u'header': {u'status': 0, u'billingMasterId': u'CLHW001', u'pdfGetStatus': 0, u'sentDate': None, u'invoiceDate': None, u'ruleName': u'CLHW001normal51', u'taxRegistrationNo': None, u'orderTime': None, u'invoiceAttr': u'1', u'invoiceNo': None, u'warehouseCode': None, u'scenario': None, u'pickingNoEnd': None, u'orderNo': u'28010118522', u'shippingAddressId': None, u'id': 3920703, u'description': None, u'creationDateEnd': None, u'creationDate': u'2020-07-25 05:31:00+0800', u'invoiceCode': None, u'sourceFlag': None, u'invoiceNoCreatetime': None, u'pickingNoStart': None, u'totalGrossNtaxAmount': 65546.22, u'countryCode': None, u'totalNtaxAmount': 65546.22, u'tbUploadStatus': 0, u'receiptAmount': None, u'returnType': None, u'systemName': u'hic', u'paymentMethod': None, u'customerInfoId': None, u'totalNoTaxTotalDiscount': 0, u'changeAskId': None, u'orderId': 8114729, u'businessType': 1, u'customerName': None, u'changeInvoiceAppCode': None, u'billingDimensionCode': None, u'errorMessage': None, u'invoiceAddressId': None, u'deleteFlag': 0, u'lastUpdateDate': u'2020-07-17 08:51:36+0800', u'invoiceDrawer': None, u'creationDateStart': None, u'createdBy': u'system', u'multiId': None, u'invoiceDateStart': None, u'refundFlag': 0, u'sourceInvoiceId': None, u'remark': None, u'paymentForm': u'2', u'language': u'en-US', u'totalGrossAmount': 78000, u'currency': u'CLP', u'totalDiscountAmount': 0, u'type': 51, u'userid': u'4130056000000000163', u'einvoiceStatus': u'0', u'statusDescription': None, u'batchId': None, u'url': None, u'invoiceDateEnd': None, u'lastUpdatedBy': u'system', u'applyId': 8929567, u'changeInvoiceAppStatus': None, u'payDate': u'2020-07-16 16:37:27+0800', u'totalItaxAmount': 78000, u'totalTaxAmount': 12453.78, u'dimensionCode': u'28010118522'}, u'originalInvoiceNo': None, u'shipment': [{u'warehouseCode': u'WMWHSE7', u'sentDate': u'2020-07-17 08:48:48+0800', u'orderNo': u'28010118522', u'id': 6399845, u'reviewDate': None, u'location': None, u'shipmentNo': u'7028010118522S01', u'carNo': None, u'orderId': 8114729, u'supplierCompanyCode': u'BLUE', u'description': None, u'logisticsNumbers': u'6671199791', u'shippingStatus': u'20', u'deleteFlag': 0, u'lastUpdatedBy': u'system', u'createdBy': u'system', u'ruleName': u'CLHW001normal51', u'creationDate': u'2020-07-16 20:15:07+0800', u'companyCode': None, u'pickingNo': None, u'lastUpdateDate': u'2020-07-17 08:51:32+0800', u'systemName': u'hic', u'businessDate': u'2020-07-17 08:48:48+0800', u'dimensionCode': u'28010118522'}], u'items': [{u'taxRate': 0.19, u'spartName': u'CLHW_testPhone01A', u'grossAmount': 78000, u'unitOfMeasure': None, u'cgstAmount': None, u'priceNtax': 65546.21849, u'orderNo': u'28010118522', u'id': 8759783, u'unit': u'PCS', u'rowNums': u'1', u'noTaxAmount': 65546.22, u'specification': None, u'ean': u'CLHWtestGBOM01', u'noTaxDiscountAmount': 0, u'lineType': 1, u'cgstRate': None, u'sgstAmount': None, u'sourceLineId': u'1EDBC4LE20AYFPBBUEC01', u'payUnitPrice': 78000, u'description': None, u'igstAmount': None, u'unitPriceNtax': 65546.22, u'price': 78000, u'hsn': u'', u'imeis': None, u'deleteFlag': 0, u'copyTax': None, u'incTaxAmount': 78000, u'createdBy': u'system', u'ruleName': u'CLHW001normal51', u'grossNoTaxAmount': 65546.22, u'skuCode': u'80560110030000301', u'creationDate': u'2020-07-17 08:51:37+0800', u'taxAmount': 12453.78, u'companyCode': None, u'igstRate': None, u'sgstRate': None, u'lastUpdateDate': u'2020-07-17 08:51:37+0800', u'lastUpdatedBy': u'system', u'systemName': u'hic', u'dimensionCode': u'28010118522', u'productType': u'10', u'invoiceHeaderId': 3920703, u'discountAmount': 0, u'orderLineId': 16636933, u'quantity': 1}], u'businessInfo': {u'invoiceExtendList': [{u'paramValue': u'REGION DE TARAPACA', u'paramCode': u'province'}, {u'paramValue': u'IQUIQUE', u'paramCode': u'city'}, {u'paramValue': u'ABCDE', u'paramCode': u'industry'}], u'isEinvoice': 0, u'registeredEmail': u'test_user_22961862@testuser.com', u'orderNo': u'28010118522', u'id': 7053879, u'idCardNumber': None, u'companyRegisteredNo': None, u'documentUse': u'', u'partyTaxSchemeName': None, u'registeredTelephone': None, u'applyId': None, u'pan': None, u'registeredDepositBank': None, u'orderId': 8114729, u'description': None, u'idType': None, u'deleteFlag': 0, u'lastUpdatedBy': u'system', u'createdBy': u'system', u'ruleName': u'CLHW001normal51', u'gstn': None, u'creationDate': u'2020-07-16 20:15:06+0800', u'registeredAddress': u'ddddd', u'name': u'test', u'companyCode': None, u'userid': u'4130056000000000163', u'systemName': u'hic', u'lastUpdateDate': u'2020-07-16 20:15:06+0800', u'taxRegistrationNo': u'11111111-1', u'dimensionCode': u'28010118522', u'registeredBankAccount': None}, u'billingAddress': {u'province': None, u'address': None, u'consignee': None, u'street': None, u'orderNo': u'28010118522', u'id': 4936073, u'city': None, u'district': None, u'zipcode': None, u'vatLastName': None, u'orderId': 8114729, u'description': None, u'deleteFlag': 0, u'lastUpdatedBy': u'system', u'vatFirstName': None, u'statecode': None, u'createdBy': u'system', u'ruleName': u'CLHW001normal51', u'creationDate': u'2020-07-16 20:15:07+0800', u'mobile': None, u'country': u'', u'lastUpdateDate': u'2020-07-16 20:15:07+0800', u'systemName': u'hic', u'companyCode': None, u'dimensionCode': u'28010118522'}}, {u'shippingAddress': {u'province': u'REGION METROPOLITANA DE SANTIAGO', u'address': u'DDDD222', u'consignee': u'A1 DD', u'street': None, u'orderNo': u'28010118515', u'id': 5755581, u'city': u'QUILICURA', u'district': None, u'zipcode': None, u'type': 0, u'email': None, u'changeAskId': None, u'orderId': 8114711, u'description': None, u'deleteFlag': 0, u'lastUpdatedBy': u'system', u'statecode': None, u'createdBy': u'system', u'ruleName': u'CLHW001normal50', u'creationDate': u'2020-07-16 19:22:41+0800', u'mobile': u'333333333', u'country': u'Chile', u'lastUpdateDate': u'2020-07-16 19:22:41+0800', u'systemName': u'hic', u'companyCode': None, u'dimensionCode': u'28010118515'}, u'paymentItems': [{u'orderId': 8114711, u'description': None, u'paymentNo': u'1111111111111', u'lastUpdateDate': u'2020-07-16 19:22:41+0800', u'companyCode': None, u'deleteFlag': 0, u'paymentType': u'0', u'amount': 0, u'lastUpdatedBy': u'system', u'systemName': u'hic', u'paymentDate': u'2020-07-16 16:32:40+0800', u'createdBy': u'system', u'ruleName': u'CLHW001normal50', u'orderNo': u'28010118515', u'creationDate': u'2020-07-16 19:22:41+0800', u'id': 4153195, u'dimensionCode': u'28010118515'}], u'trackId': 3920705, u'company': {u'defaultApplyStatus': 99, u'registeredOfficeAddress': None, u'channelId': u'b8f4fc686dd448948079c8ee19f92aaf', u'companyRegistrationNumber': None, u'seCode': None, u'contactEmail': None, u'createdBy': None, u'registeredAddressStateCode': None, u'id': 163, u'companyRea': None, u'companyTradeRegistrationNo': None, u'idealCompanyCode': u'IN-KP000014', u'companyDistrict': None, u'companyRegisteredTelephone': None, u'gstin': None, u'companyRegisteredCapital': None, u'parentCompanyName': None, u'pan': None, u'status': 1, u'companyCity': None, u'description': u'\u667a\u5229', u'companyName': u'Huawei (Chile) S.A.', u'companyRegisteredAddress': None, u'companyDepositBank': None, u'lastUpdatedBy': None, u'companyRegisterionNo': None, u'companyBankAccount': None, u'companyTaxRegistrationNo': None, u'creationDate': None, u'companyCountry': None, u'companyCode': u'CLHW001', u'companyAptNo': None, u'cin': None, u'companyProvince': None, u'lastUpdateDate': None, u'contactTelephone': None, u'companyWebsite': None}, u'originalTrackId': None, u'header': {u'status': 0, u'billingMasterId': u'CLHW001', u'pdfGetStatus': 0, u'sentDate': None, u'invoiceDate': None, u'ruleName': u'CLHW001normal50', u'taxRegistrationNo': None, u'orderTime': None, u'invoiceAttr': u'1', u'invoiceNo': None, u'warehouseCode': None, u'scenario': None, u'pickingNoEnd': None, u'orderNo': u'28010118515', u'shippingAddressId': None, u'id': 3920705, u'description': None, u'creationDateEnd': None, u'creationDate': u'2020-07-25 05:31:00+0800', u'invoiceCode': None, u'sourceFlag': None, u'invoiceNoCreatetime': None, u'pickingNoStart': None, u'totalGrossNtaxAmount': 65546.22, u'countryCode': None, u'totalNtaxAmount': 65546.22, u'tbUploadStatus': 0, u'receiptAmount': None, u'returnType': None, u'systemName': u'hic', u'paymentMethod': None, u'customerInfoId': None, u'totalNoTaxTotalDiscount': 0, u'changeAskId': None, u'orderId': 8114711, u'businessType': 1, u'customerName': None, u'changeInvoiceAppCode': None, u'billingDimensionCode': None, u'errorMessage': None, u'invoiceAddressId': None, u'deleteFlag': 0, u'lastUpdateDate': u'2020-07-17 08:51:36+0800', u'invoiceDrawer': None, u'creationDateStart': None, u'createdBy': u'system', u'multiId': None, u'invoiceDateStart': None, u'refundFlag': 0, u'sourceInvoiceId': None, u'remark': None, u'paymentForm': u'2', u'language': u'en-US', u'totalGrossAmount': 78000, u'currency': u'CLP', u'totalDiscountAmount': 0, u'type': 50, u'userid': u'4130056000000000163', u'einvoiceStatus': u'0', u'statusDescription': None, u'batchId': None, u'url': None, u'invoiceDateEnd': None, u'lastUpdatedBy': u'system', u'applyId': 8929549, u'changeInvoiceAppStatus': None, u'payDate': u'2020-07-16 16:32:40+0800', u'totalItaxAmount': 78000, u'totalTaxAmount': 12453.78, u'dimensionCode': u'28010118515'}, u'originalInvoiceNo': None, u'shipment': [{u'warehouseCode': u'WMWHSE7', u'sentDate': u'2020-07-17 08:48:47+0800', u'orderNo': u'28010118515', u'id': 6399827, u'reviewDate': None, u'location': None, u'shipmentNo': u'7028010118515S01', u'carNo': None, u'orderId': 8114711, u'supplierCompanyCode': u'BLUE', u'description': None, u'logisticsNumbers': u'6670448562', u'shippingStatus': u'20', u'deleteFlag': 0, u'lastUpdatedBy': u'system', u'createdBy': u'system', u'ruleName': u'CLHW001normal50', u'creationDate': u'2020-07-16 19:22:42+0800', u'companyCode': None, u'pickingNo': None, u'lastUpdateDate': u'2020-07-17 08:51:32+0800', u'systemName': u'hic', u'businessDate': u'2020-07-17 08:48:47+0800', u'dimensionCode': u'28010118515'}], u'items': [{u'taxRate': 0.19, u'spartName': u'CLHW_testPhone01A', u'grossAmount': 78000, u'unitOfMeasure': None, u'cgstAmount': None, u'priceNtax': 65546.21849, u'orderNo': u'28010118515', u'id': 8759785, u'unit': u'PCS', u'rowNums': u'1', u'noTaxAmount': 65546.22, u'specification': None, u'ean': u'CLHWtestGBOM01', u'noTaxDiscountAmount': 0, u'lineType': 1, u'cgstRate': None, u'sgstAmount': None, u'sourceLineId': u'1EDBBURBT0AYFPBBUEC00', u'payUnitPrice': 78000, u'description': None, u'igstAmount': None, u'unitPriceNtax': 65546.22, u'price': 78000, u'hsn': u'', u'imeis': None, u'deleteFlag': 0, u'copyTax': None, u'incTaxAmount': 78000, u'createdBy': u'system', u'ruleName': u'CLHW001normal50', u'grossNoTaxAmount': 65546.22, u'skuCode': u'80560110030000301', u'creationDate': u'2020-07-17 08:51:37+0800', u'taxAmount': 12453.78, u'companyCode': None, u'igstRate': None, u'sgstRate': None, u'lastUpdateDate': u'2020-07-17 08:51:37+0800', u'lastUpdatedBy': u'system', u'systemName': u'hic', u'dimensionCode': u'28010118515', u'productType': u'10', u'invoiceHeaderId': 3920705, u'discountAmount': 0, u'orderLineId': 16636899, u'quantity': 1}], u'businessInfo': {u'invoiceExtendList': [{u'paramValue': u'REGION DE TARAPACA', u'paramCode': u'province'}, {u'paramValue': u'IQUIQUE', u'paramCode': u'city'}, {u'paramValue': u'ABCDE', u'paramCode': u'industry'}], u'isEinvoice': 0, u'registeredEmail': u'test_user_22961862@testuser.com', u'orderNo': u'28010118515', u'id': 7053861, u'idCardNumber': None, u'companyRegisteredNo': None, u'documentUse': None, u'partyTaxSchemeName': None, u'registeredTelephone': None, u'applyId': None, u'pan': None, u'registeredDepositBank': None, u'orderId': 8114711, u'description': None, u'idType': None, u'deleteFlag': 0, u'lastUpdatedBy': u'system', u'createdBy': u'system', u'ruleName': u'CLHW001normal50', u'gstn': None, u'creationDate': u'2020-07-16 19:22:41+0800', u'registeredAddress': None, u'name': u'Personal', u'companyCode': None, u'userid': u'4130056000000000163', u'systemName': u'hic', u'lastUpdateDate': u'2020-07-16 19:22:41+0800', u'taxRegistrationNo': None, u'dimensionCode': u'28010118515', u'registeredBankAccount': None}, u'billingAddress': {u'province': None, u'address': None, u'consignee': None, u'street': None, u'orderNo': u'28010118515', u'id': 4936055, u'city': None, u'district': None, u'zipcode': None, u'vatLastName': None, u'orderId': 8114711, u'description': None, u'deleteFlag': 0, u'lastUpdatedBy': u'system', u'vatFirstName': None, u'statecode': None, u'createdBy': u'system', u'ruleName': u'CLHW001normal50', u'creationDate': u'2020-07-16 19:22:41+0800', u'mobile': None, u'country': u'', u'lastUpdateDate': u'2020-07-16 19:22:41+0800', u'systemName': u'hic', u'companyCode': None, u'dimensionCode': u'28010118515'}}]
#listQueryInvoice = []
#for inv in el_json:
    #listQueryInvoice.append(json_to_utf8(inv))

#print listQueryInvoice
#from xml.dom import minidom

## parse an xml file by name
#mydoc = minidom.parse('mario.xml')
#frmt = mydoc.getElementsByTagName('FRMT')[0].firstChild.data
#TmstFirma = mydoc.getElementsByTagName('TmstFirma')[0].firstChild.data
#TmstFirma = TmstFirma + "-0400"

#print frmt
#print TmstFirma

urlPDF = 'https://cloudqa.azuriandte.com/dte-ws-services/services/VisualizacionService?wsdl'
urlXML = "http://cloudqa.azuriandte.com/dte-webservices/services/VisualizacionXmlClienteService?wsdl"
urlState = "http://cloudqa.azuriandte.com/dte-ws-services/services/EstadoDTEService?wsdl"
apiKey = 'f33421193dc341b296ae10bbfa8c50bb6434e38dfce79d3bc66240a53581'
_folio = 115
azurian_type = 61
#azurian_type = 33
#azurian_type = 39

try:
    cliente_azurian = Client(urlState)
except:
    log.error("ERROR CREANDO CLIENT AZURIAN EstadoDTE")
DTErequest = [{'apiKey': apiKey, 'numeroFolio': _folio, 'tipoDocumento': azurian_type, 'resolucionSii': 0, 'rutEmpresa': 99535120}]
try:
    azurian_response = cliente_azurian.service.getEstadoDTE(DTErequest)
except:
    log.error("ERROR CONSUMIENDO WS AZURIAN EstadoDTE")
print "azurian_response ", azurian_response


#try:
    #cliente_azurian = Client(url)
#except:
    #print("ERROR CREANDO CLIENT AZURIAN EstadoDTE")
#DTErequest = [{'apiKey': apiKey, 'numeroFolio': _folio, 'idTipoDocumento': azurian_type, 'resolucionSii': 0, 'rutEmpresa': 99535120}]
#try:
    #azurian_response = cliente_azurian.service.consultaDocumento(DTErequest)
#except:
    #print("ERROR CONSUMIENDO WS AZURIAN EstadoDTE")

#print "azurian_response ", azurian_response


'''try:
    cliente_azurian = Client(urlPDF)
except:
    log.error("ERROR CREANDO CLIENT AZURIAN")
DTErequest = [{'apiKey': apiKey, 'numeroFolio': _folio, 'tipoDocumento': azurian_type, 'resolucionSii': 0, 'rutEmpresa': 99535120}]
try:
    azurian_response = cliente_azurian.service.visualizacionPDF(DTErequest)
except:
    log.error("ERROR CONSUMIENDO WS AZURIAN ")
print "azurian_response_pdf ", azurian_response'''

#from odooclient import client
#import json

#date_start = "2020-05-01"
#date_end = "2020-05-31"
#tipo = 1
#odooClient = client.OdooClient(host="hwco.vde-suite.com", port="4369", dbname="hwco", saas=True, debug=True)
##print "odoo_____ ", odooClient
##print "info ", odooClient.ServerInfo()
#if odooClient.Authenticate("admin", "controlVDE"):
    #print "logged"
    #criteria = [('date_entry','>=',date_start),('date_entry','<=',date_end),('typeDoc','=',tipo),('state','in',['success','success_wo_xml'])]
    #doctos = odooClient.SearchRead('hw.cfdi', criteria, ['vde_cfdi_assoc','uuid','hw_no_invoice'])
    ##print "doctos ", doctos
    #reports = []
    #for docto in doctos:
        ##print "docto"
        #_json = json.loads(docto['vde_cfdi_assoc'].replace("'",'"').replace('True','true').replace('False','false'))
        ##print "json ", _json
        #report = {
            #'cufe': docto['uuid'],
            #'order': docto['hw_no_invoice'],
            #'type': _json.get('invoiceType'),
            #'invoice_no': _json.get('customerInvoiceId'),
            #'customer': _json.get('customer').get('name')
        #}
        #reports.append(report)
    #print reports