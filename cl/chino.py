# -*- coding: utf-8 -*-
from funciones import *
import re


def search_chino(invoice):
    vals = []
    nodes = []
    for item in invoice.get('items'):
        unit = item.get('unit') if item.get('unit') else ''
        imeis = item.get('imeis') if item.get('imeis') else ''
        spartName = item.get('spartName') if item.get('spartName') else ''
    if invoice.get('billingAddress').get('vatFirstName'):
        name = invoice.get('billingAddress').get('vatFirstName')
    else:
        name = ''
    if invoice.get('billingAddress').get('vatLastName'):
        last_name = invoice.get('billingAddress').get('vatLastName')
    else:
        last_name = ''
    desc = invoice.get('header').get('description') if invoice.get('header').get('description') else ''
    businessInfo_name = invoice.get('businessInfo').get('name') if invoice.get('businessInfo').get('name') else ''
    registeredAddress = invoice.get('businessInfo').get('registeredAddress') if invoice.get('businessInfo').get('registeredAddress') else ''
    for extend in invoice.get('businessInfo').get('invoiceExtendList'):
        if extend.get('paramCode') == 'province':
            ciudad = extend.get('paramValue') if extend.get('paramValue') else ''
        if extend.get('paramCode') == 'city':
            comuna = extend.get('paramValue') if extend.get('paramValue') else ''
        if extend.get('paramCode') == 'industry':
            giro = extend.get('paramValue') if extend.get('paramValue') else ''
    shippingAddres = invoice.get('shippingAddress').get('address') if invoice.get('shippingAddress').get('address') else ''
    city = invoice.get('shippingAddress').get('city') if invoice.get('shippingAddress').get('city') else ''
    province = invoice.get('shippingAddress').get('province') if invoice.get('shippingAddress').get('province') else ''
    vals.append(('header->description', desc))
    vals.append(('billingAddress->vatFirstName', name))
    vals.append(('billingAddress->vatLastName', last_name))
    vals.append(('item->unit', unit))
    vals.append(('item->imeis', imeis))
    vals.append(('item->spartName', spartName))
    vals.append(('businessInfo->name', businessInfo_name))
    vals.append(('businessInfo->registeredAddress', registeredAddress))
    vals.append(('businessInfo->invoiceExtendList->paramCode["province"]->paramValue', ciudad))
    vals.append(('businessInfo->invoiceExtendList->paramCode["city"]->paramValue', comuna))
    vals.append(('businessInfo->invoiceExtendList->paramCode["industry"]->paramValue', giro))
    vals.append(('shippingAddress->address', shippingAddres))
    vals.append(('shippingAddress->city',city))
    vals.append(('shippingAddress->province',province))
    #print vals
    for cadena in vals:
        res = re.findall(ur'[\u4e00-\u9fff]+', cadena[1])
        if len(res) > 0:
            #return True
            #return {'node': cadena[0]}
            nodes.append({'node': cadena[0]})
        else:
            continue
    if len(nodes)>0:
        return nodes
    else:
        return False


if __name__ == "__main__":
    text_ = [{u'shippingAddress': {u'province': u'REGION METROPOLITANA DE SANTIAGO', u'address': u'El Juncal 050, Edificio B, Piso 2 Maria Teresa Torres', u'consignee': u'XIUHAO GE', u'street': None, u'orderNo': u'CL4364451411', u'id': 5759921, u'city': u'QUILICURA', u'district': None, u'zipcode': None, u'type': 0, u'email': None, u'changeAskId': None, u'orderId': 8119215, u'description': None, u'deleteFlag': 0, u'lastUpdatedBy': u'system', u'statecode': None, u'createdBy': u'system', u'ruleName': u'CLHW001normal51', u'creationDate': u'2020-08-18 05:11:22+0800', u'mobile': u'845645645', u'country': u'Chile', u'lastUpdateDate': u'2020-08-18 05:11:22+0800', u'systemName': u'hic', u'companyCode': None, u'dimensionCode': u'CL4364451411'}, u'paymentItems': [{u'orderId': 8119215, u'description': None, u'paymentNo': u'testCL4364451411', u'lastUpdateDate': u'2020-08-18 05:11:22+0800', u'companyCode': None, u'deleteFlag': 0, u'paymentType': u'0', u'amount': 0, u'lastUpdatedBy': u'system', u'systemName': u'hic', u'paymentDate': u'2020-07-17 21:17:20+0800', u'createdBy': u'system', u'ruleName': u'CLHW001normal51', u'orderNo': u'CL4364451411', u'creationDate': u'2020-08-18 05:11:22+0800', u'id': 4157941, u'dimensionCode': u'CL4364451411'}], u'trackId': 3927719, u'company': {u'defaultApplyStatus': 99, u'registeredOfficeAddress': None, u'channelId': u'b8f4fc686dd448948079c8ee19f92aaf', u'companyRegistrationNumber': None, u'seCode': None, u'contactEmail': None, u'createdBy': None, u'registeredAddressStateCode': None, u'id': 163, u'companyRea': None, u'companyTradeRegistrationNo': None, u'idealCompanyCode': u'IN-KP000014', u'companyDistrict': None, u'companyRegisteredTelephone': None, u'gstin': None, u'companyRegisteredCapital': None, u'parentCompanyName': None, u'pan': None, u'status': 1, u'companyCity': None, u'description': u'\u667a\u5229', u'companyName': u'Huawei (Chile) S.A.', u'companyRegisteredAddress': None, u'companyDepositBank': None, u'lastUpdatedBy': None, u'companyRegisterionNo': None, u'companyBankAccount': None, u'companyTaxRegistrationNo': None, u'creationDate': None, u'companyCountry': None, u'companyCode': u'CLHW001', u'companyAptNo': None, u'cin': None, u'companyProvince': None, u'lastUpdateDate': None, u'contactTelephone': None, u'companyWebsite': None}, u'originalTrackId': None, u'header': {u'status': 0, u'billingMasterId': u'CLHW001', u'pdfGetStatus': 0, u'sentDate': None, u'invoiceDate': None, u'ruleName': u'CLHW001normal51', u'taxRegistrationNo': None, u'orderTime': None, u'invoiceAttr': u'1', u'invoiceNo': None, u'warehouseCode': None, u'scenario': None, u'pickingNoEnd': None, u'orderNo': u'CL4364451411', u'shippingAddressId': None, u'id': 3927719, u'description': None, u'creationDateEnd': None, u'creationDate': u'2020-08-18 05:13:44+0800', u'invoiceCode': None, u'sourceFlag': None, u'invoiceNoCreatetime': None, u'pickingNoStart': None, u'totalGrossNtaxAmount': 890336.13, u'countryCode': None, u'totalNtaxAmount': 540756.3, u'tbUploadStatus': 0, u'receiptAmount': None, u'returnType': None, u'systemName': u'hic', u'paymentMethod': None, u'customerInfoId': None, u'totalNoTaxTotalDiscount': 349579.83, u'changeAskId': None, u'orderId': 8119215, u'businessType': 1, u'customerName': None, u'changeInvoiceAppCode': None, u'billingDimensionCode': None, u'errorMessage': None, u'invoiceAddressId': None, u'deleteFlag': 0, u'lastUpdateDate': u'2020-08-18 06:47:05+0800', u'invoiceDrawer': None, u'creationDateStart': None, u'createdBy': u'system', u'multiId': None, u'invoiceDateStart': None, u'refundFlag': 0, u'sourceInvoiceId': None, u'remark': None, u'paymentForm': u'2', u'language': u'en-US', u'totalGrossAmount': 1059500, u'currency': u'CLP', u'totalDiscountAmount': 416000, u'type': 51, u'userid': u'260086000024841790', u'einvoiceStatus': u'0', u'statusDescription': None, u'batchId': None, u'url': None, u'invoiceDateEnd': None, u'lastUpdatedBy': u'system', u'applyId': 8939663, u'changeInvoiceAppStatus': None, u'payDate': u'2020-07-17 21:17:20+0800', u'totalItaxAmount': 643500, u'totalTaxAmount': 102743.7, u'dimensionCode': u'CL4364451411'}, u'originalInvoiceNo': None, u'shipment': [{u'warehouseCode': u'WMWHSE7', u'sentDate': None, u'orderNo': u'CL4364451411', u'id': 6403457, u'reviewDate': None, u'location': None, u'shipmentNo': u'70CL4364451411S01', u'carNo': None, u'orderId': 8119215, u'supplierCompanyCode': u'BLUE', u'description': None, u'logisticsNumbers': None, u'shippingStatus': u'20', u'deleteFlag': 0, u'lastUpdatedBy': u'system', u'createdBy': u'system', u'ruleName': u'CLHW001normal51', u'creationDate': u'2020-08-18 05:11:22+0800', u'companyCode': None, u'pickingNo': None, u'lastUpdateDate': u'2020-08-18 05:11:22+0800', u'systemName': u'hic', u'businessDate': u'2020-07-17 09:35:36+0800', u'dimensionCode': u'CL4364451411'}], u'items': [{u'taxRate': 0.19, u'spartName': u'CLHW_testAcc01A', u'grossAmount': 403500, u'unitOfMeasure': None, u'cgstAmount': None, u'priceNtax': 113025.21008, u'orderNo': u'CL4364451411', u'id': 8784531, u'unit': u'PCS', u'rowNums': u'1', u'noTaxAmount': 339075.63, u'specification': None, u'ean': u'CLHWtestAccGBOM01', u'noTaxDiscountAmount': 0, u'lineType': 1, u'cgstRate': None, u'sgstAmount': None, u'sourceLineId': u'1EDD6BKHK0AEKDGPWZZ04', u'payUnitPrice': 134500, u'description': None, u'igstAmount': None, u'unitPriceNtax': 113025.21, u'price': 134500, u'hsn': u'', u'imeis': None, u'deleteFlag': 0, u'copyTax': None, u'incTaxAmount': 403500, u'createdBy': u'system', u'ruleName': u'CLHW001normal51', u'grossNoTaxAmount': 339075.63, u'skuCode': u'80560113020000201', u'creationDate': u'2020-08-18 05:13:44+0800', u'taxAmount': 64424.37, u'companyCode': None, u'igstRate': None, u'sgstRate': None, u'lastUpdateDate': u'2020-08-18 05:13:44+0800', u'lastUpdatedBy': u'system', u'systemName': u'hic', u'dimensionCode': u'CL4364451411', u'productType': u'11', u'invoiceHeaderId': 3927719, u'discountAmount': 0, u'orderLineId': 16653561, u'quantity': 3}, {u'taxRate': 0.19, u'spartName': u'CLHW_testPhone01A', u'grossAmount': 156000, u'unitOfMeasure': None, u'cgstAmount': None, u'priceNtax': 65546.21849, u'orderNo': u'CL4364451411', u'id': 8784533, u'unit': u'PCS', u'rowNums': u'2', u'noTaxAmount': 117647.06, u'specification': None, u'ean': u'CLHWtestGBOM01', u'noTaxDiscountAmount': 13445.38, u'lineType': 1, u'cgstRate': None, u'sgstAmount': None, u'sourceLineId': u'1EDD6BKHK0AEKDGPWZZ00', u'payUnitPrice': 70000, u'description': None, u'igstAmount': None, u'unitPriceNtax': 58823.53, u'price': 78000, u'hsn': u'', u'imeis': None, u'deleteFlag': 0, u'copyTax': None, u'incTaxAmount': 140000, u'createdBy': u'system', u'ruleName': u'CLHW001normal51', u'grossNoTaxAmount': 131092.44, u'skuCode': u'80560110030000301', u'creationDate': u'2020-08-18 05:13:44+0800', u'taxAmount': 22352.94, u'companyCode': None, u'igstRate': None, u'sgstRate': None, u'lastUpdateDate': u'2020-08-18 05:13:44+0800', u'lastUpdatedBy': u'system', u'systemName': u'hic', u'dimensionCode': u'CL4364451411', u'productType': u'10', u'invoiceHeaderId': 3927719, u'discountAmount': 16000, u'orderLineId': 16653557, u'quantity': 2}, {u'taxRate': 0.19, u'spartName': u'CLHW_testAcc01B \u4e2d\u6587\u4e2d\u6587', u'grossAmount': 500000, u'unitOfMeasure': None, u'cgstAmount': None, u'priceNtax': 420168.06723, u'orderNo': u'CL4364451411', u'id': 8784535, u'unit': u'PCS', u'rowNums': u'3', u'noTaxAmount': 84033.61, u'specification': None, u'ean': u'CLHWtestAccGBOM01', u'noTaxDiscountAmount': 336134.45, u'lineType': 1, u'cgstRate': None, u'sgstAmount': None, u'sourceLineId': u'1EDD6BKHK0AEKDGPWZZ02', u'payUnitPrice': 100000, u'description': None, u'igstAmount': None, u'unitPriceNtax': 84033.61, u'price': 500000, u'hsn': u'', u'imeis': None, u'deleteFlag': 0, u'copyTax': None, u'incTaxAmount': 100000, u'createdBy': u'system', u'ruleName': u'CLHW001normal51', u'grossNoTaxAmount': 420168.06, u'skuCode': u'80560113020000202', u'creationDate': u'2020-08-18 05:13:44+0800', u'taxAmount': 15966.39, u'companyCode': None, u'igstRate': None, u'sgstRate': None, u'lastUpdateDate': u'2020-08-18 05:13:44+0800', u'lastUpdatedBy': u'system', u'systemName': u'hic', u'dimensionCode': u'CL4364451411', u'productType': u'11', u'invoiceHeaderId': 3927719, u'discountAmount': 400000, u'orderLineId': 16653559, u'quantity': 1}], u'businessInfo': {u'invoiceExtendList': [{u'paramValue': u'REGION DE TARAPACA', u'paramCode': u'province'}, {u'paramValue': u'IQUIQUE', u'paramCode': u'city'}, {u'paramValue': u'ABCDE', u'paramCode': u'industry'}], u'isEinvoice': 0, u'registeredEmail': u'gexiuhao@huawei.com', u'orderNo': u'CL4364451411', u'id': 7058355, u'idCardNumber': None, u'companyRegisteredNo': None, u'documentUse': None, u'partyTaxSchemeName': None, u'registeredTelephone': None, u'applyId': None, u'pan': None, u'registeredDepositBank': None, u'orderId': 8119215, u'description': None, u'idType': None, u'deleteFlag': 0, u'lastUpdatedBy': u'system', u'createdBy': u'system', u'ruleName': u'CLHW001normal51', u'gstn': None, u'creationDate': u'2020-08-18 05:11:22+0800', u'registeredAddress': u'\xc1\xc0\xcc\xcb\xcf\xd6\xe1\xe9\xed\xf3\xfa\xfb\xfc\xff detail company address', u'name': u'TEST COMPANY', u'companyCode': None, u'userid': u'260086000024841790', u'systemName': u'hic', u'lastUpdateDate': u'2020-08-18 05:11:22+0800', u'taxRegistrationNo': u'96913280-K', u'dimensionCode': u'CL4364451411', u'registeredBankAccount': None}, u'billingAddress': {u'province': None, u'address': u'El Juncal 050, Edificio B, Piso 2 Maria Teresa Torres', u'consignee': u'XIUHAO GE', u'street': None, u'orderNo': u'CL4364451411', u'id': 4940533, u'city': None, u'district': None, u'zipcode': None, u'vatLastName': u'GE', u'orderId': 8119215, u'description': None, u'deleteFlag': 0, u'lastUpdatedBy': u'system', u'vatFirstName': u'XIUHAO', u'statecode': None, u'createdBy': u'system', u'ruleName': u'CLHW001normal51', u'creationDate': u'2020-08-18 05:11:22+0800', u'mobile': u'845645645', u'country': u'', u'lastUpdateDate': u'2020-08-18 05:11:22+0800', u'systemName': u'hic', u'companyCode': None, u'dimensionCode': u'CL4364451411'}}]
    for inv in text_:
        text = json_to_utf8(inv)
    valida = search_chino(text)
    if isinstance(valida, bool):
        print valida
    else:
        print valida
        #for n in re.findall(ur'[\u4e00-\u9fff]+', cadena):
            #print n