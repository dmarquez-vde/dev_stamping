# -*- coding: utf-8 -*-
from odooclient import client
import requests
from requests.auth import HTTPBasicAuth
import datetime
import json, ast
from funciones import *
import os.path
from os import path
import base64
import time
import fcntl
from suds.client import Client
from xml.dom import minidom
from pytz import reference
localtime = reference.LocalTimezone()
tz = localtime.tzname(datetime.datetime.now())


def getLogs(_env):
    log = get_logger("callback", "log/callback.log", True)
    log.info("Iniciando getLogs")
    # Consulta Documentos
    args = {'order': 'date_entry desc'}
    criteria = [('state','=','sent'), ('folio','!=',''), ('put_result_code','=',False)]
    #criteria = [('state','=','sent'), ('id','=',1085)]
    try:
        odooCfdi = odooClient.SearchRead('hw.cfdi', criteria,['id','typeDoc','folio','hw_invoice_head', 'hw_invoice_items', 'hw_no_invoice', 'hw_track_id', 'date_stamp'],[], **args)
    except:
        "ERROR EN METODO SEARCH_READ hw.cfdi "
    try:
        odooWebBaseUrl = odooClient.SearchRead('ir.config_parameter',[('key','=','web.base.url')],['value'])
    except:
        log.error("ERROR EN METODO SEARCH_READ ir.config_parameter ")
    odooWebBaseUrl = odooWebBaseUrl[0].get('value')
    for cfdi in odooCfdi:
        doctos = []
        _idDocto = cfdi.get('id')
        _docto = ''
        _folio = cfdi.get('folio')
        _typeDoct = cfdi.get('typeDoc')
        if _typeDoct in ['3', 3]:
            _docto = 'Boleta'
            _tipo = "bol/"
            azurian_type = 39
        elif _typeDoct in ['1', 1]:
            _docto = 'Factura'
            _tipo = "inv/"
            azurian_type = 33
        elif _typeDoct in ['4', 4]:
            _docto = 'NC_boleta'
            _tipo = "nc_bol/"
            azurian_type = 61
        elif _typeDoct in ['2', 2]:
            _docto = 'NC_factura'
            _tipo = "nc_inv/"
            azurian_type = 61

        if _env == 'TEST' or _env == 'test' or _env == 'Test':
            urlPDF = 'https://cloudqa.azuriandte.com/dte-ws-services/services/VisualizacionService?wsdl'
            urlXML = "http://cloudqa.azuriandte.com/dte-webservices/services/VisualizacionXmlClienteService?wsdl"
            urlState = "http://cloudqa.azuriandte.com/dte-ws-services/services/EstadoDTEService?wsdl"
            apiKey = 'f33421193dc341b296ae10bbfa8c50bb6434e38dfce79d3bc66240a53581'
            resol = 0
        if _env == 'PROD' or _env == 'prod' or _env == 'Prod':
            urlPDF = 'https://cloud.azuriandte.com/dte-ws-services/services/VisualizacionService?wsdl'
            urlXML = "http://cloud.azuriandte.com/dte-webservices/services/VisualizacionXmlClienteService?wsdl"
            urlState = "http://cloud.azuriandte.com/dte-ws-services/services/EstadoDTEService?wsdl"
            apiKey = '79ccbfd5d17a754280f60b3569e1b00ef9d581612590262a4d514f14c9fb'
            resol = 80
        try:
            cliente_azurian = Client(urlState)
        except:
            log.error("ERROR CREANDO CLIENT AZURIAN EstadoDTE")
        DTErequest = [{'apiKey': apiKey, 'numeroFolio': _folio, 'tipoDocumento': azurian_type, 'resolucionSii': resol, 'rutEmpresa': 99535120}]
        try:
            azurian_response = cliente_azurian.service.getEstadoDTE(DTErequest)
        except:
            log.error("ERROR CONSUMIENDO WS AZURIAN EstadoDTE")
        #print "azurian_response ", azurian_response
        log.info("azurian_response " + str(azurian_response))
        msg = azurian_response['estadoEnvioSII'].encode('utf-8').strip()
        if (_typeDoct in [1,'1',2,'2',4,'4'] and  msg in ['Aceptado con reparo SII', 'Aceptado por el SII'] ) or (_typeDoct in [3,'3'] and msg == 'No aplica envío a SII. Enviar a Cliente'):
        #if (_typeDoct in [1,'1',2,'2']) or (_typeDoct in [3,'3'] and msg == 'No aplica envío a SII'):
            _path = "/opt/vde/dev_stamping/Cl/"
            _ruta = "files/" + str(_tipo)
            docXML = _ruta + str(_folio) + ".xml"
            docPDF = _ruta + str(_folio) + ".pdf"
            filess = _path + _ruta + str(_folio) + ".xml"
            #Consulta, almacena y crea PDF para envio a HW
            if _typeDoct in [1,'1',2,'2',3,'3',4,'4']:
                try:
                    cliente_azurian = Client(urlPDF)
                except:
                    log.error("ERROR CREANDO CLIENT AZURIAN")
                DTErequest = [{'apiKey': apiKey, 'numeroFolio': _folio, 'tipoDocumento': azurian_type, 'resolucionSii': resol, 'rutEmpresa': 99535120}]
                #print "DTErequest ",DTErequest
                try:
                    azurian_response = cliente_azurian.service.visualizacionPDF(DTErequest)
                except:
                    log.error("ERROR CONSUMIENDO WS AZURIAN ")
                #print "azurian_response_pdf ", azurian_response
                log.info("azurian_response_pdf " + str(azurian_response))
                if azurian_response['codigoRespuesta'] in [0, '0']:
                    pdf_response = azurian_response['pdf64Documento']
                    pdf_response = pdf_response.encode('utf-8').strip()
                    try:
                        if pdf_response:
                            with open(docPDF, "w") as filePDF:
                                filePDF.write(base64.b64decode(pdf_response))
                    except:
                        log.error("ERROR AL ESCRIBIR PDF ")
                    try:
                        data = {
                            'name': str(_folio) + ".pdf",
                            'datas_fname': str(_folio) + ".pdf",
                            'type': 'binary',
                            'datas': pdf_response,
                            'db_datas': pdf_response,
                            'mimetype': 'application/pdf',
                            'res_model': 'hw.cfdi',
                            'res_field': 'pdf_attachment_id',
                            'res_id': _idDocto,
                            }
                        attachmentPDFId = odooClient.Create('ir.attachment',data)
                    except:
                        log.error("ERROR EN METODO CREATE ir.attachment")
                    try:
                        #local_urls = odooClient.SearchRead('ir.attachment',['|',('id','=',attachmentPDFId),('id','=',attachmentXmlId)],['local_url'])
                        local_urls = odooClient.SearchRead('ir.attachment',[('id','=',attachmentPDFId)],['local_url'])
                    except:
                        log.error("ERROR EN METODO SEARCH_READ ir.attachment ")
                    urls = {}
                    for local_url in local_urls:
                        urls.update({local_url.get('id'): local_url.get('local_url')})
                    try:
                        odooClient.Write('hw.cfdi',[_idDocto], {
                            'pdf_attachment_id': attachmentPDFId,
                            'url_pdf': odooWebBaseUrl + urls.get(attachmentPDFId),
                            })
                    except:
                        log.error("ERROR EN METODO WRITE hw.cfdi,[pdf] ")
                    try:
                        pdf_datas = odooClient.SearchRead('ir.attachment',[('id','=',attachmentPDFId)],['datas'])
                    except:
                        log.error("ERROR EN METODO SEARCH_READ ir.attachment, datas")
                    doctos.append({
                        'fileType': '1',
                        'fileSuffix': 'pdf',
                        'invoiceByte': pdf_datas[0].get('datas')
                    })
            #Consulta, almacena y crea XML para envio a HW
            if _typeDoct in [1,'1',2,'2',4,'4']:
                #print "#DESCOMENTAR CUANDO SE TENGA EL WS DE XML"
                try:
                    cliente_azurian = Client(urlXML)
                except:
                    log.error("ERROR CREANDO CLIENT AZURIAN")
                DTErequest = [{'apiKey': apiKey, 'numeroFolio': _folio, 'idTipoDocumento': azurian_type, 'resolucionSii': resol, 'rutEmpresa': 99535120}]
                try:
                    azurian_response = cliente_azurian.service.consultaDocumento(DTErequest)
                except:
                    log.error("ERROR CONSUMIENDO WS AZURIAN ")
                #print "azurian_response ", azurian_response
                log.info("azurian_response " + str(azurian_response))
                if azurian_response['codigoRespuesta'] in [0, '0']:
                    xml_response = azurian_response['xml64Document']
                    xml_response = xml_response.encode('utf-8').strip()
                    try:
                        if xml_response:
                            with open(docXML, "w") as fileXML:
                                fileXML.write(base64.b64decode(xml_response))
                    except:
                        log.error("ERROR AL ESCRIBIR XML ")
                    try:
                        data = {
                            'name': str(_folio) + ".xml",
                            'datas_fname': str(_folio) + ".xml",
                            'type': 'binary',
                            'datas': xml_response,
                            'db_datas': xml_response,
                            'mimetype': 'application/xml',
                            'res_model': 'hw.cfdi',
                            'res_field': 'xml_attachment_id',
                            'res_id': _idDocto,
                            }
                        attachmentXMLId = odooClient.Create('ir.attachment',data)
                    except:
                        log.error("ERROR EN METODO CREATE ir.attachment")
                    try:
                        #local_urls = odooClient.SearchRead('ir.attachment',['|',('id','=',attachmentPDFId),('id','=',attachmentXmlId)],['local_url'])
                        local_urls = odooClient.SearchRead('ir.attachment',[('id','=',attachmentXMLId)],['local_url'])
                    except:
                        log.error("ERROR EN METODO SEARCH_READ ir.attachment ")
                    urls = {}
                    for local_url in local_urls:
                        urls.update({local_url.get('id'): local_url.get('local_url')})
                    try:
                        odooClient.Write('hw.cfdi',[_idDocto], {
                            'xml_attachment_id': attachmentXMLId,
                            'url_xml': odooWebBaseUrl + urls.get(attachmentXMLId),
                            })
                    except:
                        log.error("ERROR EN METODO WRITE hw.cfdi,[pdf] ")
                    try:
                        xml_datas = odooClient.SearchRead('ir.attachment',[('id','=',attachmentXMLId)],['datas'])
                    except:
                        log.error("ERROR EN METODO SEARCH_READ ir.attachment, datas")
                    doctos.append({
                        'fileType': '2',
                        'fileSuffix': 'xml',
                        'invoiceByte': xml_datas[0].get('datas')
                    })
            if _typeDoct in [1,'1',2,'2',4,'4']:
                mydoc = minidom.parse(docXML)
                #frmt = mydoc.getElementsByTagName('FRMT')[0].firstChild.data
                frmt = _folio
                TmstFirma = mydoc.getElementsByTagName('TmstFirma')[0].firstChild.data
                stamp = TmstFirma.split("T")
                stamp = stamp[0] + " " + stamp[1]
                TmstFirma = TmstFirma + "-0400"
                stamp = time_zone_long_utc(TmstFirma)
                hw_invoice_head = cfdi.get('hw_invoice_head')
                _new_hw_invoice_head = hw_invoice_head.replace('%CUFE%', frmt).replace('%STAMPING%', TmstFirma)
                _new_hw_invoice_head = json.loads(_new_hw_invoice_head)
                _new_hw_invoice_items = cfdi.get('hw_invoice_items').replace("\n",'').replace("\r",'')
                _new_hw_invoice_items = json.loads(_new_hw_invoice_items)
            elif _typeDoct in [3,'3']:
                hw_invoice_head = cfdi.get('hw_invoice_head')
                frmt = _folio
                date_stmp = stamp = cfdi.get('date_stamp')
                #print "cfdi ", cfdi
                date_stmp = time_zone_long(date_stmp+"-0000")
                date_stmp = date_stmp.split(" ")
                TmstFirma = date_stmp[0]+"T"+date_stmp[1]+"-0400"
                _new_hw_invoice_head = hw_invoice_head.replace('%CUFE%', _folio).replace('%STAMPING%', TmstFirma)
                _new_hw_invoice_head = json.loads(_new_hw_invoice_head)
                _new_hw_invoice_items = cfdi.get('hw_invoice_items').replace("\n",'').replace("\r",'')
                _new_hw_invoice_items = json.loads(_new_hw_invoice_items)
            _trackId = cfdi.get('hw_track_id')
            _orderNo = cfdi.get('hw_no_invoice')
            callBack, params = putInvoiceResultLog(_trackId, _orderNo, frmt, pdf_datas[0].get('datas'),False,doctos, _new_hw_invoice_head,_new_hw_invoice_items, _env)
            try:
                odooClient.Write('hw.cfdi',[_idDocto],
                    {'put_result_code': callBack.get('resultCode'),
                        'put_message': callBack.get('message'),
                        #'put_date': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        'put_date': odoo_date(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), tz),
                        'uuid': frmt,
                        'date_stamp': stamp,
                        'result': 'Stamped and Sent successfully'
                        })
            except:
                log.error("ERROR EN METODO WRITE hw.cfdi, callback")
            if callBack.get('resultCode') == '000000':
                try:
                    odooClient.Write('hw.cfdi', [_idDocto], {'state': 'success'})
                except:
                    log.error("ERROR EN METODO WRITE hw.cfdi,state ")
        elif azurian_response['idEstadoEnvioSII'] in [-1,-2,-3,-4,-5,-6,-7,-8]:
            try:
                odooClient.Write('hw.cfdi', [_idDocto], {'result': msg})
            except:
                log.error("ERROR AL ESCRIBIR HW.CFDI, result")
            try:
                odooClient.Write('hw.cfdi', [_idDocto], {'state': 'error'})
            except:
                log.error("ERROR EN METODO WRITE hw.cfdi,state ")
try:
    env = "PROD"
    dbname = "hwcl" if env == "PROD" else "hwcl-test" if env=="TEST" else ''
    with open('/tmp/call_cl', 'a') as flock:
        fcntl.flock(flock, fcntl.LOCK_EX | fcntl.LOCK_NB)
        odooClient = client.OdooClient(host="hwco.vde-suite.com", port="4369", dbname=dbname, saas=True, debug=True)
        if odooClient.Authenticate("admin", "controlVDE"):
            getLogs(env)
except IOError as e:
    pass
