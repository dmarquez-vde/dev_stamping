# -*- coding: utf-8 -*-
from odooclient import client
import requests
from requests.auth import HTTPBasicAuth
import datetime
import json, ast
from funciones import *
import base64
import fcntl
import time
from suds.client import Client
from pytz import reference

log = get_logger("inv", "log/bol.log", True)
localtime = reference.LocalTimezone()
tz = localtime.tzname(datetime.datetime.now())

def procesaDoctosInv(guid, tipoDoc, fechainicio, fechafin, tipoDocL, nextpage, _env):

    log.info("Iniciando ProcesaDoctosInv")
    _invoiceType = 'FACTURA'
    _operationCode = '10'
    log.info("ENV: "+str(_env))
    if _env == 'TEST' or _env == 'test' or _env== 'Test':
        _pointOfSale = 'SETT'
    elif _env == 'PROD' or _env == 'prod' or _env== 'Prod':
        _pointOfSale = 'PROD'
    _customerInvoiceId = '2000020'
    env = _env
    data={
        "country": "CL",
        "environment": env,
        "guid": guid}
    token_response = requests.post(
        "http://localhost:8869/HWKeys/public/api/getToken", data=data)
    urlHW_I = json_to_utf8(token_response.json().get('url'))
    appKey = json_to_utf8(token_response.json().get('appKey'))
    token= json_to_utf8(token_response.json().get('token'))
    data = {
        'Type': tipoDoc,
        'StartTime': fechainicio,
        'EndTime': fechafin,
        'Guid': guid,
        'NextPage': nextpage
    }
    params_response = requests.post(
        "http://localhost:8869/HWKeys/public/api/getParams",data=data)
    params = params_response.json()
    hash_ = params.get('hash')
    str_json = params.get('json')
    headers = {
        'X-HW-ID': 'com.huawei.invoice_cloud',
        'X-HW-APPKEY': appKey,
        'Content-Type': 'application/json'
    }
    datos_result = {
        'companyCode':'CLHW_VDE',
        'params': hash_,
        'token': token
    }
    datos_json = json.dumps(datos_result)
    ##### odoo create hw.execution
    datas = {
        'typeDoc': tipoDocL,
        #'dateStart': datetime.datetime.strptime(fechainicio[:-5], "%Y-%m-%d %H:%M:%S") + datetime.timedelta(hours=6),
        'dateStart': odoo_date(fechainicio[:-5], tz),
        #'dateEnd': datetime.datetime.strptime(fechafin[:-5], "%Y-%m-%d %H:%M:%S")  + datetime.timedelta(hours=6),
        'dateEnd': odoo_date(fechafin[:-5], tz),
        #'dateCron': (fstart  + datetime.timedelta(hours=6)).strftime("%Y-%m-%d %H:%M:%S"),
        'dateCron': odoo_date(fstart.strftime("%Y-%m-%d %H:%M:%S"), tz),
        'dateExecution': odoo_date(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), tz),
        'state': '0',
        'token': token,
        'params_crypt': params.get('hash'),
        'params': str_json,
        }
    try:
        odooExecutionId = odooClient.Create('hw.execution', datas)
    except Exception, e:
        log.error("ERROR EN METODO CREATE hw.execution ")
    hw_response = requests.post(urlHW_I, headers=headers, data=datos_json)
    hw_response = hw_response.json()
    _hw_res = hw_response.get('resultCode')
    _hw_data = hw_response.get('resultData')
    _hw_np = hw_response.get('nextPage') if hw_response.get('nextPage') else False
    _hw_mes = hw_response.get('message')
    if _hw_res == '500001':
        log.info("resultCode " + _hw_res)
        log.info("Llama a siguiente pagina " + str(_hw_np))
        datas = {
            'resultCode': _hw_res,
            'resultData': False,
            'nextPage': False if _hw_np is False  else _hw_np,
            'message': _hw_mes,
            'execution_id': odooExecutionId,
            'guid': guid,
            #'json': json.dumps(listQueryInvoice),
        }
        try:
            odooResultId = odooClient.Create('hw.result', datas)
        except:
            log.error("ERROR EN METODO CREATE hw.result " + datas)
        procesaDoctosInv(guid, tipoDoc, fechainicio, fechafin, tipoDocL,
        _hw_np, env)
    elif _hw_res=='000000':
        log.info("resultCode " + str(_hw_res))
        data = {
            'Data': _hw_data,
            'Guid': guid
        }
        decrypt_response = requests.post(
            "http://localhost:8869/HWKeys/public/api/decrypt",
            data=data)
        listQueryInvoice = []
        for inv in decrypt_response.json().get('json'):
            listQueryInvoice.append(json_to_utf8(inv))
        datas = {
            'resultCode': _hw_res,
            #'resultData': hw_response.get('resultData'),
            'nextPage': False if _hw_np is False else _hw_np,
            'message': _hw_mes,
            'execution_id': odooExecutionId,
            'guid': guid,
            #'json': json.dumps(listQueryInvoice).decode('unicode-escape').encode('utf8'),
        }
        try:
            odooResultId = odooClient.Create('hw.result', datas)
        except:
            log.error("ERROR EN METODO CREATE hw.result "+datas)
        if len(listQueryInvoice) > 0:
            log.info("No. Documents " + str(len(listQueryInvoice)))
            for invoice in listQueryInvoice:
                #if invoice.get('header').get('orderNo') in ['CL4543554546','CL5562434456']:
                _type = invoice.get('header').get('type')
                #print "_type ", _type
                if _type in [51, '51']:
                    _originalTrackId = invoice.get('originalTrackId')
                    _originalInvoiceNo = invoice.get('originalInvoiceNo')
                    _trackId = str(invoice.get('trackId'))
                    _orderNo = _orderNo = invoice.get('header').get('orderNo')
                    _orderNo = str(_orderNo)
                    _creationDate = invoice.get('header').get('creationDate')
                    _send_mail_account = invoice.get('businessInfo').get('registeredEmail')
                    if invoice.get('billingAddress').get('vatFirstName'):
                        name = invoice.get('billingAddress').get('vatFirstName')
                    else:
                        name = ''
                    if invoice.get('billingAddress').get('vatLastName'):
                        last_name = invoice.get('billingAddress').get('vatLastName')
                    else:
                        last_name = ''
                    _send_mail_name =  name + " " + last_name
                    _hash_cfdi = _trackId + "  "+_orderNo
                    log.info("######### " + _hash_cfdi + " ########")
                    #######search.read hw.cfdi
                    criteria = [('hw_no_invoice','=',_orderNo),('hw_track_id','=',_trackId),('state','in',['success','sent','fail'])]
                    log.info("criteria " + str(criteria))
                    try:
                        odooCfdi = odooClient.SearchRead('hw.cfdi',criteria,['uuid'])
                    except:
                        log.error("ERROR EN METODO SEARCH_READ hw.cfdi ")
                    if len(odooCfdi)==0:
                        log.info("PROCESAR FACTURAS")
                        ######create hw.cfdi
                        datas = {
                            'name': _hash_cfdi,
                            'hw_no_invoice': _orderNo,
                            'hw_track_id': _trackId,
                            'state': 'pending',
                            'date_entry': odoo_date(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), tz),
                            'date_push': invoice.get('header').get('creationDate'),
                            'hw_json': json.dumps(invoice).decode('unicode-escape').encode('utf8'),
                            'execution_id': odooExecutionId
                        }
                        try:
                            odooCFDIId = odooClient.Create('hw.cfdi',datas)
                        except Exception as e:
                            log.error("ERROR EN METODO CREATE hw.cfdi ")
                        log.info("VALIDA CHINO")
                        valida = search_chino(invoice)
                        if not isinstance(valida, bool):
                            chinoErrors = ""
                            cont = 1
                            for node in valida:
                                chinoErrors = chinoErrors + "[" + str(cont) + "] => Chinese character found at: "+ node.get('node') + "\n"
                            invoice_header = {'orderNo': _orderNo}
                            try:
                                odooClient.Write('hw.cfdi',[odooCFDIId],
                                    {'result':chinoErrors,'state':'error'})
                            except:
                                log.error( "ERROR EN METODO WRITE hw.cfdi,[result,state]")
                            #### CALLBACK
                            callBack, params = putInvoiceResult(_trackId, _orderNo,False, False,chinoErrors,False, invoice_header,False, env)
                            try:
                                odooClient.Write('hw.cfdi',[odooCFDIId],
                                    {'put_result_code': callBack.get('resultCode'),
                                        'put_message': callBack.get('message'),
                                        'put_date': odoo_date(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), tz),
                                        #'hw_callback': params,
                                        })
                            except:
                                log.error("ERROR EN METODO WRITE hw.cfdi, callback")
                            continue
                        list_error = {}
                        list_error_desc = {}
                        customer_type = validate_json('type', _type)
                        customer_rut = validate_json('rut',invoice.get('businessInfo').get('taxRegistrationNo') if invoice.get('businessInfo').get('taxRegistrationNo') else '' )
                        list_error['rut'], list_error_desc['rut'] = customer_rut
                        if False in list_error.values():
                            odooErrors = ""
                            for error in list_error_desc:
                                if isinstance(list_error_desc.get(error), dict):
                                    odooErrors = odooErrors + "["+list_error_desc.get(error).get('Code')+"] => "+list_error_desc.get(error).get('info')+"\n"
                            invoice_header = {'orderNo': _orderNo}
                            try:
                                odooClient.Write('hw.cfdi',[odooCFDIId],
                                    {'result':odooErrors,'state':'error'})
                            except:
                                log.error( "ERROR EN METODO WRITE hw.cfdi,[result,state]")
                            #### CALLBACK
                            callBack, params = putInvoiceResult(_trackId, _orderNo,False, False,odooErrors,False, invoice_header,False, env)
                            try:
                                odooClient.Write('hw.cfdi',[odooCFDIId],
                                    {'put_result_code': callBack.get('resultCode'),
                                        'put_message': callBack.get('message'),
                                        'put_date': odoo_date(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), tz),
                                        #'hw_callback': params,
                                        })
                            except:
                                log.error("ERROR EN METODO WRITE hw.cfdi, callback")
                        else:
                            file_completed, txt, invoice_items = create_txt(invoice, _type, False, _hash_cfdi, _env, log)
                            invoice_header = {
                                'orderNo': _orderNo,
                                'invoiceAttr': '1',
                                'invoiceNo': "%CUFE%",
                                'invoiceDate': "%STAMPING%",
                                'createdBy': invoice.get('header').get('createdBy'),
                                'totalItaxAmount': invoice.get('header').get('totalItaxAmount'),
                                'totalTaxAmount': invoice.get('header').get('totalTaxAmount'),
                                'description': invoice.get('header').get('description'),
                            }
                            invoice_header1 = {
                                'orderNo': _orderNo,
                                'invoiceAttr': '1',
                                'invoiceNo': '',
                                'invoiceDate': '',
                                'createdBy': invoice.get('header').get('createdBy'),
                                'totalItaxAmount': invoice.get('header').get('totalItaxAmount'),
                                'totalTaxAmount': invoice.get('header').get('totalTaxAmount'),
                                'description': invoice.get('header').get('description'),
                            }
                            datas = {
                                'vde_cfdi_assoc': txt,
                                'send_mail_account': _send_mail_account,
                                'send_mail_name': _send_mail_name,
                                'hw_invoice_head': json.dumps(invoice_header).decode('unicode-escape').encode('utf8'),
                                'hw_invoice_items': json.dumps(invoice_items).decode('unicode-escape').encode('utf8'),
                            }
                            try:
                                odooClient.Write('hw.cfdi',[odooCFDIId], datas)
                            except Exception:
                                log("ERROR EN METODO WRITE hw.cfdi,vde_cfdi_assoc")
                            if env == 'TEST' or env == 'test' or env == 'Test':
                                url = 'https://cloudqa.azuriandte.com/dte-ws-services/services/ProcessDTEService?wsdl'
                                apiKey = 'f33421193dc341b296ae10bbfa8c50bb6434e38dfce79d3bc66240a53581'
                                resol = 0
                                #apiKey = 'f33421193dc341b296ae10bbfa8c50bb6434e38dfce79d3bc66240a53581false'
                            if env == 'PROD' or env == 'prod' or env == 'Prod':
                                #url = 'https://cloudqa.azuriandte.com/dte-ws-services/services/ProcessDTEService?wsdl'
                                url = 'http://cloud.azuriandte.com/dte-ws-services/services/ProcessDTEService?wsdl'
                                apiKey = '79ccbfd5d17a754280f60b3569e1b00ef9d581612590262a4d514f14c9fb'
                                resol = 80
                            ####### AQUI MANDA A AZURIAN
                            try:
                                data = open("txt/inv/"+file_completed,"r").read()
                                b64 = base64.b64encode(data)
                            except:
                                log.error("ERROR CODIFICANDO BASE64")
                            try:
                                cliente_azurian = Client(url)
                            except:
                                log.error("ERROR CREANDO CLIENT AZURIAN")
                            DTErequest = [{'apiKey': apiKey, 'archivo': file_completed, 'data': b64, 'resolucionSii': resol, 'rutEmpresa': 99535120}]
                            try:
                                azurian_response = cliente_azurian.service.importarDocumentos(DTErequest)
                            except:
                                log.error("ERROR CONSUMIENDO WS AZURIAN ")
                            #print "azurian_response ", azurian_response
                            log.info("azurian_response ", str(azurian_response))
                            if azurian_response['codigoRespuesta'] and azurian_response['codigoRespuesta'] in [1,'1']:
                                log.error("ERROR TIMBRADO")
                                odooErrors = azurian_response['descripcionRespuesta'].encode('utf-8').strip()
                                try:
                                    odooClient.Write('hw.cfdi',[odooCFDIId],{'result': odooErrors, 'state':'error'})
                                except:
                                    log.error("ERROR EN METODO WRITE hw.cfdi,[result,state] ")
                                #######CALLBACK
                                callBack, params = putInvoiceResult(_trackId, _orderNo,False, False,odooErrors,False, invoice_header1,invoice_items, env)
                                try:
                                    odooClient.Write('hw.cfdi',[odooCFDIId],
                                        {'put_result_code': callBack.get('resultCode'),
                                            'put_message': callBack.get('message'),
                                            'put_date': odoo_date(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"), tz),
                                            #'hw_callback': params,
                                            })
                                except:
                                    log.error("ERROR EN METODO WRITE hw.cfdi, callback")
                            if azurian_response['codigoRespuesta'] and azurian_response['codigoRespuesta'] in [0,'0']:
                                log.info("TIMBRADO CORRECTO")
                                try:
                                    odooClient.Write('hw.cfdi', [odooCFDIId], {
                                        'folio': azurian_response['numeroFolio'],
                                        'state': 'sent'
                                    })
                                except:
                                    log.error("ERROR EN METODO WRITE hw.cfdi,[folio,serie,state] ")
                    else:
                        log.error("FACTURA YA EXISTE " + _trackId + " | " + _orderNo)
        if hw_response.get('nextPage'):
            log.info("RECURSIVO, NUEVA PAGINA ", str(hw_response.get('nextPage')))
            procesaDoctosInv(guid, tipoDoc, fechainicio, fechafin, tipoDocL, hw_response.get('nextPage'), env)

try:
    env = "PROD"
    dbname = "hwcl" if env == "PROD" else "hwcl-test" if env=="TEST" else ''
    with open('/tmp/inv_cl', 'a') as flock:
        fcntl.flock(flock, fcntl.LOCK_EX | fcntl.LOCK_NB)
        odooClient = client.OdooClient(host="hwco.vde-suite.com", port="4369", dbname=dbname, saas=True, debug=True)
        #print "odoo_____ ", odooClient
        log.info("odoo_____ " + str(odooClient))
        if odooClient.Authenticate("admin", "controlVDE"):
            guid_response = requests.get(
                "http://localhost:8869/HWKeys/public/api/guid")
            guid = guid_response.json().get('guid')
            tipoDoc = 1
            tipoDocL = 1
            fstart = datetime.datetime.now()
            finicio = fstart - datetime.timedelta(hours=419) - datetime.timedelta(minutes=55)
            fechainicio = finicio.strftime("%Y-%m-%d %H:%M:%S") + tz
            fechainicio = time_zone_long(fechainicio)
            fechainicio = fechainicio + "-0400"
            finicio = finicio.strftime("%Y-%m-%d %H:%M:%S")
            ffin = fstart - datetime.timedelta(minutes=1)
            fechafin = ffin.strftime("%Y-%m-%d %H:%M:%S") + tz
            fechafin = time_zone_long(fechafin)
            fechafin = fechafin + "-0400"
            ffin = ffin.strftime("%Y-%m-%d %H:%M:%S")
            procesaDoctosResult = procesaDoctosInv(guid, tipoDoc, fechainicio, fechafin, tipoDocL,'',env)
            fcntl.flock(flock, fcntl.LOCK_UN)
except IOError as e:
    pass

