# -*- coding: utf-8 -*-
from dateutil import parser
import pytz
from validations import *
import ast
import json
import logging
from logging.handlers import RotatingFileHandler
import time
from catalogs import vdeCity, vdeDepartment, vdeCountryCode
import locale
import requests
from requests.auth import HTTPBasicAuth
import json
import time
import datetime
import re
from itertools import cycle
#from structure import *

#log = get_logger("inv", "log/bol.log", True)

def odoo_date(fecha, tz):
    server_datetime = fecha + tz
    chile_datetime = datetime.datetime.strptime(time_zone_long(server_datetime), "%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d %H:%M:%S")
    utc_datetime = datetime.datetime.strptime(time_zone_long_utc(chile_datetime + "-0400"), "%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d %H:%M:%S")
    return utc_datetime

def validarRut(rut):
    if rut == '0-0':
        return False
    rut = rut.upper();
    rut = rut.replace("-","")
    rut = rut.replace(".","")
    aux = rut[:-1]
    dv = rut[-1:]
    revertido = map(int, reversed(str(aux)))
    factors = cycle(range(2,8))
    s = sum(d * f for d, f in zip(revertido,factors))
    res = (-s)%11
    if str(res) == dv:
        return True
    elif dv=="K" and res==10:
        return True
    else:
        return False


def search_chino(invoice):
    vals = []
    nodes = []
    ciudad = ''
    comuna = ''
    giro = ''
    for item in invoice.get('items'):
        unit = item.get('unit') if item.get('unit') else ''
        imeis = item.get('imeis') if item.get('imeis') else ''
        spartName = item.get('spartName') if item.get('spartName') else ''
    if invoice.get('billingAddress').get('vatFirstName'):
        name = invoice.get('billingAddress').get('vatFirstName')
    else:
        name = ''
    if invoice.get('billingAddress').get('vatLastName'):
        last_name = invoice.get('billingAddress').get('vatLastName')
    else:
        last_name = ''
    desc = invoice.get('header').get('description') if invoice.get('header').get('description') else ''
    businessInfo_name = invoice.get('businessInfo').get('name') if invoice.get('businessInfo').get('name') else ''
    registeredAddress = invoice.get('businessInfo').get('registeredAddress') if invoice.get('businessInfo').get('registeredAddress') else ''
    for extend in invoice.get('businessInfo').get('invoiceExtendList'):
        if extend.get('paramCode') == 'province':
            ciudad = extend.get('paramValue') if extend.get('paramValue') else ''
        if extend.get('paramCode') == 'city':
            comuna = extend.get('paramValue') if extend.get('paramValue') else ''
        if extend.get('paramCode') == 'industry':
            giro = extend.get('paramValue') if extend.get('paramValue') else ''
    shippingAddres = invoice.get('shippingAddress').get('address') if invoice.get('shippingAddress').get('address') else ''
    city = invoice.get('shippingAddress').get('city') if invoice.get('shippingAddress').get('city') else ''
    province = invoice.get('shippingAddress').get('province') if invoice.get('shippingAddress').get('province') else ''
    vals.append(('header->description', desc))
    vals.append(('billingAddress->vatFirstName', name))
    vals.append(('billingAddress->vatLastName', last_name))
    vals.append(('item->unit', unit))
    vals.append(('item->imeis', imeis))
    vals.append(('item->spartName', spartName))
    vals.append(('businessInfo->name', businessInfo_name))
    vals.append(('businessInfo->registeredAddress', registeredAddress))
    vals.append(('businessInfo->invoiceExtendList->paramCode["province"]->paramValue', ciudad))
    vals.append(('businessInfo->invoiceExtendList->paramCode["city"]->paramValue', comuna))
    vals.append(('businessInfo->invoiceExtendList->paramCode["industry"]->paramValue', giro))
    vals.append(('shippingAddress->address', shippingAddres))
    vals.append(('shippingAddress->city',city))
    vals.append(('shippingAddress->province',province))
    #print vals
    for cadena in vals:
        res = re.findall(ur'[\u4e00-\u9fff]+', cadena[1])
        if len(res) > 0:
            #return True
            #return {'node': cadena[0]}
            nodes.append({'node': cadena[0]})
        else:
            continue
    if len(nodes)>0:
        return nodes
    else:
        return False

def json_to_utf8(json_):
    #print json_
    inv = json_
    #for inv in json_:
    if isinstance(inv, dict):
        for key in inv.keys():
            #print "key ", key
            if isinstance(inv.get(key), dict):
                #print "es dict"
                keys = inv.get(key).keys()
                for key_ in keys:
                    #print "key_ ", key_
                    if isinstance(inv.get(key).get(key_), list):
                        #print "es list dentro de dict"
                        subs1 = []
                        for sub in inv.get(key).get(key_):
                            #print "sub ",sub
                            recurs = json_to_utf8(sub)
                            #print "recurs ", recurs
                            subs1.append(recurs)
                        inv.get(key).update({key_:subs1})
                    else:
                        val = inv.get(key).get(key_)
                        if  not (isinstance(val, float) or isinstance(val, int)):
                            if val == None:
                                inv.get(key).update({key_: False})
                            else:
                                inv.get(key).update({key_: unicode(val).encode('utf-8')})
            elif isinstance(inv.get(key), list):
                #print "es lista"
                inv.get(key)
                subs = []
                for sub in inv.get(key):
                    #print "sub ",sub
                    recurs = json_to_utf8(sub)
                    #print "recurs ", recurs
                    subs.append(recurs)
                inv.update({key:subs})
            else:
                val = inv.get(key)
                if  not (isinstance(val, float) or isinstance(val, int)):
                    if val == None:
                        inv.update({key: False})
                    else:
                        #print "val ", val
                        #print type(val)
                        inv.update({key: val})
        #print "limpio ", json_
        return inv
    else:
        if json_ is None:
            #print "es None"
            return False
        #print "no es dict ", json_
        if  not (isinstance(json_, float) or isinstance(json_, int)):
            #print "no es num "
            if json_ == None:
                return False
            else:
                return unicode(json_).encode('utf-8')

def ajusta_monto(monto, enteros, decimales):
    monto = round(monto, decimales)
    monto_s = str(monto)
    entero = monto_s.split(".")[0]
    decimal = monto_s.split(".")[1]
    new_entero = new_decimal= concat = ""
    for key in range(enteros - len(entero)):
        concat = concat + "0"
    new_entero = concat + entero
    concat = ""
    if decimales > 0:
        for key in range(decimales - len(decimal)):
            concat = concat + "0"
        new_decimal = decimal + concat
    return new_entero + new_decimal

def create_txt(invoice, _type, folio_actual, _hash_cfdi,  _env, log):
    #print "create_txt", type(invoice)
    invoiceAttr = invoice.get('header').get('invoiceAttr')
    #print('Tipo Docto',invoiceAttr)
    # Tipo de Documeto 1
    if invoiceAttr in [1,'1']:

        if _type in [50,'50']:
            _path ="txt/bol/"
        elif _type in [51,'51']:
            _path ="txt/inv/"
        encabezado, totales, glosas = get_encabezado(invoice,_type,folio_actual, _env)
        #print "encabezado ", encabezado
        itemss, neto, exento, impuestos, invoice_items, recargos = get_items(invoice, _type,  _env)
        #print "impuestos ", impuestos
        enc = ""
        ite = ""
        tot = ""
        rec = ""
        glos = ""
        # Boletas
        if _type in [50,'50']:
            # Neto
            #bad_neto_ = ajusta_monto(round(invoice.get('header').get('totalItaxAmount'),0), 18, 0)
            #neto_ = ajusta_monto(round(invoice.get('header').get('totalGrossAmount'),0), 18, 0)
            neto_ = ajusta_monto(round(0,0), 18, 0)
            # Exento
            exento_ = ajusta_monto(round(exento,0), 18, 0)
            # Monto Informado
            monInf_ = ajusta_monto(0.0, 12, 6)
            # TasaIva
            tasaIva_ = ajusta_monto(impuestos.get('gravado').get('iva'),1, 4)
            # MontoIva
            #montoIva_ = ajusta_monto(round(invoice.get('header').get('totalTaxAmount'),0),18, 0)
            montoIva_ = ajusta_monto(round(0,0),18, 0)
            # MontoTotal
            #montoTot_ = ajusta_monto(neto + exento + impuestos.get('gravado').get('monto'),12, 6)
            montoTot_ = ajusta_monto(round(invoice.get('header').get('totalItaxAmount') + exento,0),18, 0)
            # MontoPeriodo
            #montoPe_ = ajusta_monto(neto + exento + impuestos.get('gravado').get('monto'),12, 6)
            montoPe_ = ajusta_monto(round(invoice.get('header').get('totalItaxAmount') + exento,0),18, 0)

        # Facturas
        elif _type in [51,'51']:
            # Neto
            neto_ = ajusta_monto(round(invoice.get('header').get('totalNtaxAmount'),0), 18, 0)
            #neto_ = ajusta_monto(round(invoice.get('header').get('totalGrossNtaxAmount'),0), 18, 0)
            # Exento
            exento_ = ajusta_monto(round(exento,0), 18, 0)
            # Monto Informado
            monInf_ = ajusta_monto(0.0, 12, 6)
            # TasaIva
            tasaIva_ = ajusta_monto(impuestos.get('gravado').get('iva'),1, 4)
            # MontoIva
            montoIva_ = ajusta_monto(round(invoice.get('header').get('totalTaxAmount'),0),18, 0)
            # MontoTotal
            montoTot_ = ajusta_monto(round(invoice.get('header').get('totalItaxAmount'),0),18, 0)
            # MontoPeriodo
            montoPe_ = ajusta_monto(round(invoice.get('header').get('totalItaxAmount'),0),18, 0)

        encabezado.update({
            #'MontoNeto': spaces(neto__,18),
            'MontoNeto': neto_,
            'MontoExento': exento_,
            #'MontoExento': spaces('',18),
            'MontoInformado': monInf_,
            #'MontoInformado': spaces(0,18),
            'TasaIva': tasaIva_,
            #'TasaIva': spaces('',5),
            'MontoIva': montoIva_,
            #'MontoIva': spaces('',18),
            'MontoTotal': montoTot_,
            #'MontoTotal': spaces('',18),
            'MontoPeriodo': montoPe_,
            #'MontoPeriodo': spaces('',18),

        })
        enc = enc + encabezado.get('TipoRegistro')
        enc = enc + encabezado.get('TipoDocumento')
        enc = enc + encabezado.get('NumeroFolio')
        enc = enc + encabezado.get('FechaEmision')
        enc = enc + encabezado.get('IndNoRebaja')
        enc = enc + encabezado.get('TipoDespacho')
        enc = enc + encabezado.get('IndTipoTrasBien')
        enc = enc + encabezado.get('IndServPeri')
        enc = enc + encabezado.get('IndMonBru')
        enc = enc + encabezado.get('FormaPago')
        enc = enc + encabezado.get('FechaCancelacion')
        enc = enc + encabezado.get('PeriodoDesde')
        enc = enc + encabezado.get('PeriodoHasta')
        enc = enc + encabezado.get('MedioPago')
        enc = enc + encabezado.get('TerminosPagoC')
        enc = enc + encabezado.get('TerminosPagoD')
        enc = enc + encabezado.get('FechaVencimiento')
        enc = enc + encabezado.get('RutEmisor')
        enc = enc + encabezado.get('DigVerEmi')
        enc = enc + encabezado.get('NumRes')
        enc = enc + encabezado.get('NombreEmisor')
        enc = enc + encabezado.get('GiroComercialEmisor')
        enc = enc + encabezado.get('CodigoActEconomica')
        enc = enc + encabezado.get('SucEmiDoc')
        enc = enc + encabezado.get('CodSucReg')
        enc = enc + encabezado.get('DireccionOrigen')
        enc = enc + encabezado.get('ComunaOrigen')
        enc = enc + encabezado.get('CiudadOrigen')
        enc = enc + encabezado.get('CodigoVendedor')
        enc = enc + encabezado.get('RutMandante')
        enc = enc + encabezado.get('DigVerDemandante')
        enc = enc + encabezado.get('RutReceptor')
        enc = enc + encabezado.get('DigVerReceptor')
        enc = enc + encabezado.get('CodIntReceptor')
        enc = enc + encabezado.get('NombreReceptor')
        enc = enc + encabezado.get('GiroNegReceptor')
        enc = enc + encabezado.get('ContReceptor')
        #print "encabezado.get('DirReceptor') ", encabezado.get('DirReceptor')
        #time.sleep(10)
        enc = enc + encabezado.get('DirReceptor')
        enc = enc + encabezado.get('ComunaReceptor')
        enc = enc + encabezado.get('CiudadReceptor')
        enc = enc + encabezado.get('DirPosReceptor')
        enc = enc + encabezado.get('ComunaPosReceptor')
        enc = enc + encabezado.get('CiudadPosReceptor')
        enc = enc + encabezado.get('RutSolicitante')
        enc = enc + encabezado.get('DigSolFactura')
        enc = enc + encabezado.get('PatenteTransporte')
        enc = enc + encabezado.get('RutTransportista')
        enc = enc + encabezado.get('DigTransportista')
        enc = enc + encabezado.get('DirDestino')
        enc = enc + encabezado.get('ComunaDestino')
        enc = enc + encabezado.get('CiudadDestino')
        enc = enc + encabezado.get('MontoNeto')
        enc = enc + encabezado.get('MontoExento')
        enc = enc + encabezado.get('MontoInformado')
        enc = enc + encabezado.get('TasaIva')
        enc = enc + encabezado.get('MontoIva')
        enc = enc + encabezado.get('MontoIvaRetenido')
        enc = enc + encabezado.get('CreditoEspecial')
        enc = enc + encabezado.get('GarantiaDeposito')
        enc = enc + encabezado.get('MontoTotal')
        enc = enc + encabezado.get('MontoNoFacturable')
        enc = enc + encabezado.get('MontoPeriodo')
        enc = enc + encabezado.get('SaldoAnterior')
        enc = enc + encabezado.get('ValorPagar')
        enc = enc + encabezado.get('Sistema')
        enc = enc + encabezado.get('Usuario')
        enc = enc + encabezado.get('TemplateImpresion')
        enc = enc + encabezado.get('VersionTempImpre')
        enc = enc + encabezado.get('Impresora')
        enc = enc + encabezado.get('ReceptorElectronico')
        enc = enc + encabezado.get('FormaAceptacion')
        enc = enc + encabezado.get('TipoFoleacion')
        enc = enc + encabezado.get('MontoIvaPropio')
        enc = enc + encabezado.get('MontoIvaTerceros')
        enc = enc + encabezado.get('CopiasImprimir')
        enc = enc + encabezado.get('CodigoActEconomica2')
        enc = enc + encabezado.get('IdenficadorInterno')
        enc = enc + encabezado.get('ImprimirCedible')
        enc = enc + "\n"
        cont = 1
        tot = tot + totales.get('TipoRegistroT')
        tot = tot + totales.get('FolioPrimerR')
        tot = tot + totales.get('FolioUltimoR')
        tot = tot + totales.get('CantidadDoctos')
        tot = tot + totales.get('RutFirmaDoc')
        tot = tot + totales.get('DVFirmaDoc')
        tot = tot + totales.get('AreaFirmaDoc')
        tot = tot + "\n"
        rec = rec + recargos.get('TipoRegistroR')
        rec = rec + recargos.get('NumLinea')
        rec = rec + recargos.get('TipoMov')
        rec = rec + recargos.get('Glosa')
        rec = rec + recargos.get('TipoValor')
        rec = rec + recargos.get('Valor')
        rec = rec + recargos.get('Indicador')
        rec = rec + recargos.get('ValorOtraMoneda')
        rec = rec + "\n"
        for glosa in glosas:
            glos = glos + glosa.get('TipoRegistroG')
            glos = glos + glosa.get('NumeroLineaG')
            glos = glos + glosa.get('DescriptorGlosa')
            glos = glos + glosa.get('TextoGlosa')
            glos = glos + "\n"
        for items in itemss:
            #print "items ", items
            ite = ite + items.get(str(cont)).get('TipoRegistroP')
            ite = ite + items.get(str(cont)).get('NoLinea')
            ite = ite + items.get(str(cont)).get('NoLineaSII')
            ite = ite + items.get(str(cont)).get('TipoCodigo')
            ite = ite + items.get(str(cont)).get('CodigoItem')
            ite = ite + items.get(str(cont)).get('TipoCodigo2')
            ite = ite + items.get(str(cont)).get('CodigoItem2')
            ite = ite + items.get(str(cont)).get('TipoCodigo3')
            ite = ite + items.get(str(cont)).get('CodigoItem3')
            ite = ite + items.get(str(cont)).get('TipoCodigo4')
            ite = ite + items.get(str(cont)).get('CodigoItem4')
            ite = ite + items.get(str(cont)).get('TipoCodigo5')
            ite = ite + items.get(str(cont)).get('CodigoItem5')
            ite = ite + items.get(str(cont)).get('IndicadorFact')
            ite = ite + items.get(str(cont)).get('NombreItem')
            ite = ite + items.get(str(cont)).get('CantidadRef')
            ite = ite + items.get(str(cont)).get('UnidadRef')
            ite = ite + items.get(str(cont)).get('PrecioRef')
            ite = ite + items.get(str(cont)).get('CantidadItem')
            ite = ite + items.get(str(cont)).get('Subcantidad')
            ite = ite + items.get(str(cont)).get('DescSubcantidad')
            ite = ite + items.get(str(cont)).get('Subcantidad2')
            ite = ite + items.get(str(cont)).get('DescSubcantidad2')
            ite = ite + items.get(str(cont)).get('Subcantidad3')
            ite = ite + items.get(str(cont)).get('DescSubcantidad3')
            ite = ite + items.get(str(cont)).get('Subcantidad4')
            ite = ite + items.get(str(cont)).get('DescSubcantidad4')
            ite = ite + items.get(str(cont)).get('Subcantidad5')
            ite = ite + items.get(str(cont)).get('DescSubcantidad5')
            ite = ite + items.get(str(cont)).get('FechaElaboracion')
            ite = ite + items.get(str(cont)).get('FechaVencimientoP')
            ite = ite + items.get(str(cont)).get('UnidadMedida')
            ite = ite + items.get(str(cont)).get('PrecioUnitarioItem')
            ite = ite + items.get(str(cont)).get('PrecioUniOtraMoneda')
            ite = ite + items.get(str(cont)).get('CodigoOtraMoneda')
            ite = ite + items.get(str(cont)).get('FactorConversion')
            ite = ite + items.get(str(cont)).get('DescuentoPorcentaje')
            ite = ite + items.get(str(cont)).get('DescuentoMonto')
            ite = ite + items.get(str(cont)).get('TipoSubdescuento')
            ite = ite + items.get(str(cont)).get('ValorSubdescuento')
            ite = ite + items.get(str(cont)).get('TipoSubdescuento2')
            ite = ite + items.get(str(cont)).get('ValorSubdescuento2')
            ite = ite + items.get(str(cont)).get('TipoSubdescuento3')
            ite = ite + items.get(str(cont)).get('ValorSubdescuento3')
            ite = ite + items.get(str(cont)).get('TipoSubdescuento4')
            ite = ite + items.get(str(cont)).get('ValorSubdescuento4')
            ite = ite + items.get(str(cont)).get('TipoSubdescuento5')
            ite = ite + items.get(str(cont)).get('ValorSubdescuento5')
            ite = ite + items.get(str(cont)).get('RecargoPorcentage')
            ite = ite + items.get(str(cont)).get('MontoRecargo')
            ite = ite + items.get(str(cont)).get('TipoSubrecargo')
            ite = ite + items.get(str(cont)).get('ValorSubrecargo')
            ite = ite + items.get(str(cont)).get('TipoSubrecargo2')
            ite = ite + items.get(str(cont)).get('ValorSubrecargo2')
            ite = ite + items.get(str(cont)).get('TipoSubrecargo3')
            ite = ite + items.get(str(cont)).get('ValorSubrecargo3')
            ite = ite + items.get(str(cont)).get('TipoSubrecargo4')
            ite = ite + items.get(str(cont)).get('ValorSubrecargo4')
            ite = ite + items.get(str(cont)).get('TipoSubrecargo5')
            ite = ite + items.get(str(cont)).get('ValorSubrecargo5')
            ite = ite + items.get(str(cont)).get('CodigoImpuesto')
            ite = ite + items.get(str(cont)).get('MontoItem')
            ite = ite + items.get(str(cont)).get('DescripcionAdicional')
            ite = ite + items.get(str(cont)).get('TipoDocLiquida')
            ite = ite + items.get(str(cont)).get('ValorDetOtrMoneda')
            ite = ite + items.get(str(cont)).get('RecargoOtrMoneda')
            ite = ite + items.get(str(cont)).get('DescOtraMoneda')
            ite = ite + items.get(str(cont)).get('TipoCodSubcantidad1')
            ite = ite + items.get(str(cont)).get('TipoCodSubcantidad2')
            ite = ite + items.get(str(cont)).get('TipoCodSubcantidad3')
            ite = ite + items.get(str(cont)).get('TipoCodSubcantidad4')
            ite = ite + items.get(str(cont)).get('TipoCodSubcantidad5')
            ite = ite + items.get(str(cont)).get('TipoCodigoSumple')
            ite = ite + items.get(str(cont)).get('IndAgenteRetenedor')
            ite = ite + '\n'
            cont = cont + 1
        file_name = str(_hash_cfdi) + '.txt'
        if recargos.get('Valor') != '000000000000000000':
            #print "enc ", enc
            txt_ = enc + ite + rec + glos + tot
            txt_ = txt_.encode('iso-8859-15').strip()
            try:
                with open(_path + file_name, 'w') as txt:
                    txt.write(txt_)
            except:
                #print('ERROR CREANDO FILE TXT')
                log.info("ERROR CREANDO FILE TXT")
            #time.sleep(20)
            txt_ = enc + ite + rec + glos + tot
            txt_ = txt_.encode('utf-8').strip()
        else:
            txt_ = enc + ite + glos + tot
            txt_ = txt_.encode('iso-8859-15').strip()
            try:
                with open(_path + file_name, 'w') as txt:
                    txt.write(txt_)
            except:
                #print('ERROR CREANDO FILE TXT')
                log.info("ERROR CREANDO FILE TXT")
            txt_ = enc + ite + glos + tot
            txt_ = txt_.encode('utf-8').strip()
        return file_name, txt_, invoice_items
    # Tipo de Documeto 2
    elif invoiceAttr in [2,'2']:
        if _type in [50,'50']:
            _path ="txt/b-nc/"
        elif _type in [51,'51']:
            _path ="txt/i-nc/"
        encabezado, totales, glosas = get_encabezado(invoice,_type,folio_actual, _env)
        #print "encabezado ", encabezado
        itemss, neto, exento, impuestos, invoice_items, recargos = get_items(invoice, _type,  _env)
        #print "impuestos ", impuestos

        # Codigo Referencia
        #codigoref = invoice.get('header').get('businessType')
        #if codigoref in [13,'13',17,'17']:
            #codref = 1
            #motref = 'Anula Documento'
        #elif codigoref in [20,'20']:
            #codref = 2
            #motref = 'Corrige Texto'
        nc_totalItaxAmount = invoice.get('header').get('totalItaxAmount')
        inv_totalItaxAmount = json.loads(folio_actual.get('json')).get('header').get('totalItaxAmount')
        if nc_totalItaxAmount < inv_totalItaxAmount:
            codref = 3
            motref = 'Corrige Montos'
        elif nc_totalItaxAmount == inv_totalItaxAmount:
            codref = 1
            motref = 'Anula Documento'
        enc = ""
        ite = ""
        ref = ""
        tot = ""
        rec = ""
        glos = ""
        # Nota de Credito - Boletas
        if _type in [50,'50']:
            # Neto
            neto_ = ajusta_monto(round(invoice.get('header').get('totalNtaxAmount'),0), 18, 0)
            #neto_ = ajusta_monto(round(invoice.get('header').get('totalGrossNtaxAmount'),0), 18, 0)
            # Exento
            exento_ = ajusta_monto(round(exento,0), 18, 0)
            # Monto Informado
            monInf_ = ajusta_monto(0.0, 12, 6)
            # TasaIva
            tasaIva_ = ajusta_monto(impuestos.get('gravado').get('iva'),1, 4)
            # MontoIva
            montoIva_ = ajusta_monto(round(invoice.get('header').get('totalTaxAmount'),0),18, 0)
            # MontoTotal
            #montoTot_ = ajusta_monto(neto + exento + impuestos.get('gravado').get('monto'),12, 6)
            montoTot_ = ajusta_monto(round(invoice.get('header').get('totalItaxAmount') + exento,0),18, 0)
            # MontoPeriodo
            #montoPe_ = ajusta_monto(neto + exento + impuestos.get('gravado').get('monto'),12, 6)
            montoPe_ = ajusta_monto(round(invoice.get('header').get('totalItaxAmount') + exento,0),18, 0)

            # Referencia (Obligatorio en NC)
            referencia ={
                'TipoRegistroF': spaces('F',1), # Siempre F
                'NumeroLineaF': spaces('1',2),
                'TipoDocRef': spaces('39',3),#39= Boleta
                'IndRefGlobal': spaces(0,1),# 0 unico Docto, 1 Muchos Doctos
                'FolioDocRef': spaces('',10),# Folio de Odoo que nos entrega Azurian
                'RutOtroCont': spaces('',8),
                'DigVerOtrCont': spaces('',1),
                'FechaRef': spaces(str(folio_actual.get('fecha')),8),# Fecha del Doc Referenciado (aaaammdd)
                'CodigoRef': spaces(codref,1), # 1=Anula Documento, 2= Corrige Texto, 3= Corrige montos
                'RazonRef': spaces(motref,30), # Descripcion o Motivo de la NC
                'CodigoRefEmp': spaces('',10),
                'TextoRazonRef': spaces('',90),
                'IndRefInterna': spaces('',1),
                'FolioDocRefAdi': spaces(folio_actual.get('folio'),18),
            }

        # Nota de Credito - Facturas
        elif _type in [51,'51']:
            # Neto
            neto_ = ajusta_monto(round(invoice.get('header').get('totalNtaxAmount'),0), 18, 0)
            #neto_ = ajusta_monto(round(invoice.get('header').get('totalGrossNtaxAmount'),0), 18, 0)
            # Exento
            exento_ = ajusta_monto(round(exento,0), 18, 0)
            # Monto Informado
            monInf_ = ajusta_monto(0.0, 12, 6)
            # TasaIva
            tasaIva_ = ajusta_monto(impuestos.get('gravado').get('iva'),1, 4)
            # MontoIva
            montoIva_ = ajusta_monto(round(invoice.get('header').get('totalTaxAmount'),0),18, 0)
            # MontoTotal
            montoTot_ = ajusta_monto(round(invoice.get('header').get('totalItaxAmount'),0),18, 0)
            # MontoPeriodo
            montoPe_ = ajusta_monto(round(invoice.get('header').get('totalItaxAmount'),0),18, 0)

            # Referencia (Obligatorio en NC)
            referencia ={
                'TipoRegistroF': spaces('F',1), # Siempre F
                'NumeroLineaF': spaces('1',2),
                'TipoDocRef': spaces('33',3),#33= Factura Electronica
                'IndRefGlobal': spaces(0,1),# 0 unico Docto, 1 Muchos Doctos
                'FolioDocRef': spaces('',10),# Folio de Odoo que nos entrega Azurian
                'RutOtroCont': spaces('',8),
                'DigVerOtrCont': spaces('',1),
                'FechaRef': spaces(folio_actual.get('fecha'),8),# Fecha del Doc Referenciado (aaaammdd)
                'CodigoRef': spaces(codref,1), # 1=Anula Documento, 2= Corrige Texto, 3= Corrige montos
                'RazonRef': spaces(motref,30),
                'CodigoRefEmp': spaces('',10),
                'TextoRazonRef': spaces('',90),
                'IndRefInterna': spaces('',1),
                'FolioDocRefAdi': spaces(folio_actual.get('folio'),18),
            }

        encabezado.update({
            #'MontoNeto': spaces(neto__,18),
            'MontoNeto': neto_,
            'MontoExento': exento_,
            #'MontoExento': spaces('',18),
            'MontoInformado': monInf_,
            #'MontoInformado': spaces(0,18),
            'TasaIva': tasaIva_,
            #'TasaIva': spaces('',5),
            'MontoIva': montoIva_,
            #'MontoIva': spaces('',18),
            'MontoTotal': montoTot_,
            #'MontoTotal': spaces('',18),
            'MontoPeriodo': montoPe_,
            #'MontoPeriodo': spaces('',18),

        })

        enc = enc + encabezado.get('TipoRegistro')
        enc = enc + encabezado.get('TipoDocumento')
        enc = enc + encabezado.get('NumeroFolio')
        enc = enc + encabezado.get('FechaEmision')
        enc = enc + encabezado.get('IndNoRebaja')
        enc = enc + encabezado.get('TipoDespacho')
        enc = enc + encabezado.get('IndTipoTrasBien')
        enc = enc + encabezado.get('IndServPeri')
        enc = enc + encabezado.get('IndMonBru')
        enc = enc + encabezado.get('FormaPago')
        enc = enc + encabezado.get('FechaCancelacion')
        enc = enc + encabezado.get('PeriodoDesde')
        enc = enc + encabezado.get('PeriodoHasta')
        enc = enc + encabezado.get('MedioPago')
        enc = enc + encabezado.get('TerminosPagoC')
        enc = enc + encabezado.get('TerminosPagoD')
        enc = enc + encabezado.get('FechaVencimiento')
        enc = enc + encabezado.get('RutEmisor')
        enc = enc + encabezado.get('DigVerEmi')
        enc = enc + encabezado.get('NumRes')
        enc = enc + encabezado.get('NombreEmisor')
        enc = enc + encabezado.get('GiroComercialEmisor')
        enc = enc + encabezado.get('CodigoActEconomica')
        enc = enc + encabezado.get('SucEmiDoc')
        enc = enc + encabezado.get('CodSucReg')
        enc = enc + encabezado.get('DireccionOrigen')
        enc = enc + encabezado.get('ComunaOrigen')
        enc = enc + encabezado.get('CiudadOrigen')
        enc = enc + encabezado.get('CodigoVendedor')
        enc = enc + encabezado.get('RutMandante')
        enc = enc + encabezado.get('DigVerDemandante')
        enc = enc + encabezado.get('RutReceptor')
        enc = enc + encabezado.get('DigVerReceptor')
        enc = enc + encabezado.get('CodIntReceptor')
        enc = enc + encabezado.get('NombreReceptor')
        enc = enc + encabezado.get('GiroNegReceptor')
        enc = enc + encabezado.get('ContReceptor')
        enc = enc + encabezado.get('DirReceptor')
        enc = enc + encabezado.get('ComunaReceptor')
        enc = enc + encabezado.get('CiudadReceptor')
        enc = enc + encabezado.get('DirPosReceptor')
        enc = enc + encabezado.get('ComunaPosReceptor')
        enc = enc + encabezado.get('CiudadPosReceptor')
        enc = enc + encabezado.get('RutSolicitante')
        enc = enc + encabezado.get('DigSolFactura')
        enc = enc + encabezado.get('PatenteTransporte')
        enc = enc + encabezado.get('RutTransportista')
        enc = enc + encabezado.get('DigTransportista')
        enc = enc + encabezado.get('DirDestino')
        enc = enc + encabezado.get('ComunaDestino')
        enc = enc + encabezado.get('CiudadDestino')
        enc = enc + encabezado.get('MontoNeto')
        enc = enc + encabezado.get('MontoExento')
        enc = enc + encabezado.get('MontoInformado')
        enc = enc + encabezado.get('TasaIva')
        enc = enc + encabezado.get('MontoIva')
        enc = enc + encabezado.get('MontoIvaRetenido')
        enc = enc + encabezado.get('CreditoEspecial')
        enc = enc + encabezado.get('GarantiaDeposito')
        enc = enc + encabezado.get('MontoTotal')
        enc = enc + encabezado.get('MontoNoFacturable')
        enc = enc + encabezado.get('MontoPeriodo')
        enc = enc + encabezado.get('SaldoAnterior')
        enc = enc + encabezado.get('ValorPagar')
        enc = enc + encabezado.get('Sistema')
        enc = enc + encabezado.get('Usuario')
        enc = enc + encabezado.get('TemplateImpresion')
        enc = enc + encabezado.get('VersionTempImpre')
        enc = enc + encabezado.get('Impresora')
        enc = enc + encabezado.get('ReceptorElectronico')
        enc = enc + encabezado.get('FormaAceptacion')
        enc = enc + encabezado.get('TipoFoleacion')
        enc = enc + encabezado.get('MontoIvaPropio')
        enc = enc + encabezado.get('MontoIvaTerceros')
        enc = enc + encabezado.get('CopiasImprimir')
        enc = enc + encabezado.get('CodigoActEconomica2')
        enc = enc + encabezado.get('IdenficadorInterno')
        enc = enc + encabezado.get('ImprimirCedible')
        enc = enc + "\n"
        cont = 1
        ref = ref + referencia.get('TipoRegistroF')
        ref = ref + referencia.get('NumeroLineaF')
        ref = ref + referencia.get('TipoDocRef')
        ref = ref + referencia.get('IndRefGlobal')
        ref = ref + referencia.get('FolioDocRef')
        ref = ref + referencia.get('RutOtroCont')
        ref = ref + referencia.get('DigVerOtrCont')
        ref = ref + referencia.get('FechaRef')
        ref = ref + referencia.get('CodigoRef')
        ref = ref + referencia.get('RazonRef')
        ref = ref + referencia.get('CodigoRefEmp')
        ref = ref + referencia.get('TextoRazonRef')
        ref = ref + referencia.get('IndRefInterna')
        ref = ref + referencia.get('FolioDocRefAdi')
        ref = ref + "\n"
        for glosa in glosas:
            glos = glos + glosa.get('TipoRegistroG')
            glos = glos + glosa.get('NumeroLineaG')
            glos = glos + glosa.get('DescriptorGlosa')
            glos = glos + glosa.get('TextoGlosa')
            glos = glos + "\n"
        tot = tot + totales.get('TipoRegistroT')
        tot = tot + totales.get('FolioPrimerR')
        tot = tot + totales.get('FolioUltimoR')
        tot = tot + totales.get('CantidadDoctos')
        tot = tot + totales.get('RutFirmaDoc')
        tot = tot + totales.get('DVFirmaDoc')
        tot = tot + totales.get('AreaFirmaDoc')
        tot = tot + "\n"
        rec = rec + recargos.get('TipoRegistroR')
        rec = rec + recargos.get('NumLinea')
        rec = rec + recargos.get('TipoMov')
        rec = rec + recargos.get('Glosa')
        rec = rec + recargos.get('TipoValor')
        rec = rec + recargos.get('Valor')
        rec = rec + recargos.get('Indicador')
        rec = rec + recargos.get('ValorOtraMoneda')
        rec = rec + "\n"
        for items in itemss:
            #print "items ", items
            ite = ite + items.get(str(cont)).get('TipoRegistroP')
            ite = ite + items.get(str(cont)).get('NoLinea')
            ite = ite + items.get(str(cont)).get('NoLineaSII')
            ite = ite + items.get(str(cont)).get('TipoCodigo')
            ite = ite + items.get(str(cont)).get('CodigoItem')
            ite = ite + items.get(str(cont)).get('TipoCodigo2')
            ite = ite + items.get(str(cont)).get('CodigoItem2')
            ite = ite + items.get(str(cont)).get('TipoCodigo3')
            ite = ite + items.get(str(cont)).get('CodigoItem3')
            ite = ite + items.get(str(cont)).get('TipoCodigo4')
            ite = ite + items.get(str(cont)).get('CodigoItem4')
            ite = ite + items.get(str(cont)).get('TipoCodigo5')
            ite = ite + items.get(str(cont)).get('CodigoItem5')
            ite = ite + items.get(str(cont)).get('IndicadorFact')
            ite = ite + items.get(str(cont)).get('NombreItem')
            ite = ite + items.get(str(cont)).get('CantidadRef')
            ite = ite + items.get(str(cont)).get('UnidadRef')
            ite = ite + items.get(str(cont)).get('PrecioRef')
            ite = ite + items.get(str(cont)).get('CantidadItem')
            ite = ite + items.get(str(cont)).get('Subcantidad')
            ite = ite + items.get(str(cont)).get('DescSubcantidad')
            ite = ite + items.get(str(cont)).get('Subcantidad2')
            ite = ite + items.get(str(cont)).get('DescSubcantidad2')
            ite = ite + items.get(str(cont)).get('Subcantidad3')
            ite = ite + items.get(str(cont)).get('DescSubcantidad3')
            ite = ite + items.get(str(cont)).get('Subcantidad4')
            ite = ite + items.get(str(cont)).get('DescSubcantidad4')
            ite = ite + items.get(str(cont)).get('Subcantidad5')
            ite = ite + items.get(str(cont)).get('DescSubcantidad5')
            ite = ite + items.get(str(cont)).get('FechaElaboracion')
            ite = ite + items.get(str(cont)).get('FechaVencimientoP')
            ite = ite + items.get(str(cont)).get('UnidadMedida')
            ite = ite + items.get(str(cont)).get('PrecioUnitarioItem')
            ite = ite + items.get(str(cont)).get('PrecioUniOtraMoneda')
            ite = ite + items.get(str(cont)).get('CodigoOtraMoneda')
            ite = ite + items.get(str(cont)).get('FactorConversion')
            ite = ite + items.get(str(cont)).get('DescuentoPorcentaje')
            ite = ite + items.get(str(cont)).get('DescuentoMonto')
            ite = ite + items.get(str(cont)).get('TipoSubdescuento')
            ite = ite + items.get(str(cont)).get('ValorSubdescuento')
            ite = ite + items.get(str(cont)).get('TipoSubdescuento2')
            ite = ite + items.get(str(cont)).get('ValorSubdescuento2')
            ite = ite + items.get(str(cont)).get('TipoSubdescuento3')
            ite = ite + items.get(str(cont)).get('ValorSubdescuento3')
            ite = ite + items.get(str(cont)).get('TipoSubdescuento4')
            ite = ite + items.get(str(cont)).get('ValorSubdescuento4')
            ite = ite + items.get(str(cont)).get('TipoSubdescuento5')
            ite = ite + items.get(str(cont)).get('ValorSubdescuento5')
            ite = ite + items.get(str(cont)).get('RecargoPorcentage')
            ite = ite + items.get(str(cont)).get('MontoRecargo')
            ite = ite + items.get(str(cont)).get('TipoSubrecargo')
            ite = ite + items.get(str(cont)).get('ValorSubrecargo')
            ite = ite + items.get(str(cont)).get('TipoSubrecargo2')
            ite = ite + items.get(str(cont)).get('ValorSubrecargo2')
            ite = ite + items.get(str(cont)).get('TipoSubrecargo3')
            ite = ite + items.get(str(cont)).get('ValorSubrecargo3')
            ite = ite + items.get(str(cont)).get('TipoSubrecargo4')
            ite = ite + items.get(str(cont)).get('ValorSubrecargo4')
            ite = ite + items.get(str(cont)).get('TipoSubrecargo5')
            ite = ite + items.get(str(cont)).get('ValorSubrecargo5')
            ite = ite + items.get(str(cont)).get('CodigoImpuesto')
            ite = ite + items.get(str(cont)).get('MontoItem')
            ite = ite + items.get(str(cont)).get('DescripcionAdicional')
            ite = ite + items.get(str(cont)).get('TipoDocLiquida')
            ite = ite + items.get(str(cont)).get('ValorDetOtrMoneda')
            ite = ite + items.get(str(cont)).get('RecargoOtrMoneda')
            ite = ite + items.get(str(cont)).get('DescOtraMoneda')
            ite = ite + items.get(str(cont)).get('TipoCodSubcantidad1')
            ite = ite + items.get(str(cont)).get('TipoCodSubcantidad2')
            ite = ite + items.get(str(cont)).get('TipoCodSubcantidad3')
            ite = ite + items.get(str(cont)).get('TipoCodSubcantidad4')
            ite = ite + items.get(str(cont)).get('TipoCodSubcantidad5')
            ite = ite + items.get(str(cont)).get('TipoCodigoSumple')
            ite = ite + items.get(str(cont)).get('IndAgenteRetenedor')
            ite = ite + '\n'
            cont = cont + 1
        file_name = str(_hash_cfdi) + '.txt'
        if recargos.get('Valor') != '000000000000000000':
            txt_ = enc + ite + ref + rec + glos + tot
            txt_ = txt_.encode('iso-8859-15').strip()
            try:
                with open(_path + file_name, 'w') as txt:
                    txt.write(txt_)
            except:
                #print('ERROR CREANDO FILE TXT')
                log.info("ERROR CREANDO FILE TXT")
            txt_ = enc + ite + ref + rec + glos + tot
            txt_ = txt_.encode('utf-8').strip()
        else:
            txt_ = enc + ite + ref + glos + tot
            txt_ = txt_.encode('iso-8859-15').strip()
            try:
                with open(_path + file_name, 'w') as txt:
                    txt.write(txt_)
            except:
                #print('ERROR CREANDO FILE TXT')
                log.info("ERROR CREANDO FILE TXT")
            txt_ = enc + ite + ref + glos + tot
            txt_ = txt_.encode('utf-8').strip()
        return file_name, txt_, invoice_items


def get_logger(logger_name, archivo, create_file=False):
    # create logger for prd_ci
    log = logging.getLogger(logger_name)
    log.setLevel(level=logging.INFO)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s %(levelname)s ? %(message)s')
    if create_file:
        # create file handler for logger.
        #fh = logging.FileHandler("log/inv.log")
        fh = RotatingFileHandler(archivo, mode='a', maxBytes=1000000, encoding=None, delay=0)
        fh.setLevel(level=logging.DEBUG)
        fh.setFormatter(formatter)
    # reate console handler for logger.
    ch = logging.StreamHandler()
    ch.setLevel(level=logging.DEBUG)
    ch.setFormatter(formatter)
    # add handlers to logger.
    if create_file:
        log.addHandler(fh)
    log.addHandler(ch)
    return  log


def time_zone_long(date):
    get_date_obj = parser.parse(date)
    #print(get_date_obj.strftime("%Y-%m-%d %H:%M:%S%z"))
    bogota = pytz.timezone('America/Santiago')
    observationTime = get_date_obj.astimezone(bogota)
    #print(observationTime.strftime("%Y-%m-%d %H:%M:%S%z"))
    #print(observationTime.strftime("%Y-%m-%d %H:%M:%S"))
    return observationTime.strftime("%Y-%m-%d %H:%M:%S")


def time_zone_long_utc(date):
    get_date_obj = parser.parse(date)
    #print(get_date_obj.strftime("%Y-%m-%d %H:%M:%S%z"))
    bogota = pytz.timezone('UTC')
    observationTime = get_date_obj.astimezone(bogota)
    #print(observationTime.strftime("%Y-%m-%d %H:%M:%S%z"))
    #print(observationTime.strftime("%Y-%m-%d %H:%M:%S"))
    return observationTime.strftime("%Y-%m-%d %H:%M:%S")


def time_zone_short_(date):
    get_date_obj = parser.parse(date)
    #print(get_date_obj.strftime("%Y-%m-%d %H:%M:%S%z"))
    bogota = pytz.timezone('America/Santiago')
    observationTime = get_date_obj.astimezone(bogota)
    #print(observationTime.strftime("%Y-%m-%d %H:%M:%S%z"))
    #print(observationTime.strftime("%Y-%m-%d %H:%M:%S"))
    return observationTime.strftime("%Y-%m-%d")


def time_zone_short(date):
    get_date_obj = parser.parse(date)
    bogota = pytz.timezone('America/Santiago')
    observationTime = get_date_obj.astimezone(bogota)
    return observationTime.strftime("%Y%m%d")


def spaces(cadena, longitud):
    if isinstance(cadena, int) or isinstance(cadena, float):
        char = "0"
        is_num = True
        cadena = str(cadena)[:longitud]
    else:
        char = " "
        is_num = False
        cadena = cadena[:longitud]
    new_cadena = ""

    for key in range(longitud - len(cadena)):
        new_cadena = new_cadena + char
    if is_num is True:
        return new_cadena + str(cadena)
        #return str(cadena) + new_cadena
    elif is_num is False:
        if isinstance(cadena, int) or isinstance(cadena, float):
            return str(cadena) + new_cadena
        else:
            return cadena + new_cadena

def spacesjmm(cadena, longitud):
    if isinstance(cadena, int) or isinstance(cadena, float):
        char = "0"
        is_num = True
    else:
        char = "0"
        is_num = False
    new_cadena = ""
    for key in range(longitud - len(str(cadena))):
        new_cadena = new_cadena + char
    if is_num is True:
        return new_cadena + str(cadena)
        #return str(cadena) + new_cadena
    elif is_num is False:
        return str(cadena) + new_cadena

def methodPay(cadena):

    switcher = {
        47: "CH",
        2: "CF",
        3: "LT",
        4: "EF",
        5: "PE",
        6: "TC",
    }
    return switcher.get(cadena, "OT")

def validRut(cadena):
    switcher = {
        0: "",
    }
    return switcher.get(cadena, cadena)

def digVer(valrut,val):
    #print('Valor Rut',valrut)
    valrut = valrut.replace('.','')
    if valrut == '':
        return ''
    else:
        x = str(valrut).split('-')
        if val == 0:
            return int(x[0])
        elif val == 1:
            return str(x[1])

def get_items(invoice, customer_type, env):
    #print "get_items", type(invoice)
    items = []
    monto_neto = 0
    monto_neto_exento = 0
    discount_per = 0
    impuestos = {}
    invoice_items = []
    recargos = {}
    rec_desc = 0
    invoiceAttr = invoice.get('header').get('invoiceAttr')
    for item in invoice.get('items'):
        invoice_items.append({
            'id': item.get('id'),
            'spartName': item.get('spartName'),
            'price': item.get('unitPriceNtax'),
            'quantity': item.get('quantity'),
            'discountAmount': item.get('noTaxDiscountAmount'),
            'taxRate': item.get('taxRate'),
            'taxAmount': item.get('taxAmount'),
        })

        # Valores
        price_unit_no_tax = float(item.get('priceNtax'))
        tax_rate = float(item.get('taxRate'))
        qty = abs(item.get('quantity'))

        # Boletas
        if customer_type in ['50', 50] and invoiceAttr in [1,'1']:
            tax_code = ''
            price = float(abs(item.get('price')))
            payUnitPrice = float(abs(item.get('payUnitPrice')))
            incTaxAmount = float(round(abs(item.get('incTaxAmount')),0))
            discountAmount = float(round(abs(item.get('discountAmount')),0))
            grossAmount = float(round(abs(item.get('grossAmount')),0))
            rec_desc = rec_desc + discountAmount
            if payUnitPrice >0:
                if (payUnitPrice / price) == 1:
                    discount_per = 0
                else:
                    discount_per = ((payUnitPrice / price)-1)*(-100)
            else:
                discount_per = 0.0
            # CantidadItem
            cantIte_ = ajusta_monto(qty,12, 6)
            # PriceUnit
            priceUni_ = ajusta_monto(round(price,0),12, 6)
            # DiscountAmount
            #discountA_ = ajusta_monto(discountAmount,18, 0)
            discountA_ = spaces('0',18)
            # Descuento Porcentaje
            #descPor_ = ajusta_monto(discount_per,3,2)
            descPor_ = spaces('0',5)
            # MontoItem
            #montoIte_ = ajusta_monto(incTaxAmount,18, 0)
            montoIte_ = ajusta_monto(grossAmount, 18, 0)


        # Facturas
        elif (customer_type in ['51', 51] and invoiceAttr in [1,'1',2,'2']) or (customer_type in ['50',50] and invoiceAttr in [2,'2']):
            tax_code = 14 if tax_rate > 0 else ''
            priceNtax = float(abs(item.get('priceNtax')))
            unitPriceNtax = float(abs(item.get('unitPriceNtax')))
            noTaxDiscountAmount = float(round(abs(item.get('noTaxDiscountAmount')),0))
            noTaxAmount = float(round(abs(item.get('noTaxAmount')),0))
            grossNoTaxAmount = float(round(abs(item.get('grossNoTaxAmount')),0))
            rec_desc = rec_desc + noTaxDiscountAmount
            if noTaxDiscountAmount == 0:
                discount_per = 0.0
            elif unitPriceNtax > 0:
                if (unitPriceNtax / priceNtax) == 1:
                    discount_per
                else:
                    discount_per = ((unitPriceNtax / priceNtax)-1)*(-100)
            else:
                discount_per = 0.0
            # CantidadItem
            cantIte_ = ajusta_monto(qty,12, 6)
            # PriceUnit
            priceUni_ = ajusta_monto(round(priceNtax,0),12, 6)
            # DiscountAmount
            #discountA_ = ajusta_monto(noTaxDiscountAmount,18, 0)
            discountA_ = spaces(0,18)
            # Descuento Porcentaje
            #descPor_ = ajusta_monto(discount_per,3,2)
            descPor_ = spaces('0',5)
            # MontoItem
            #montoIte_ = ajusta_monto(noTaxAmount,18, 0)
            montoIte_ = ajusta_monto(grossNoTaxAmount,18, 0)




        #print type(item.get('imeis'))
        #print isinstance(item.get('imeis'), bool)
        cod = '' if isinstance(item.get('ean'), bool) else item.get('ean')
        desc = '' if isinstance(item.get('imeis'), bool) else item.get('imeis')
        #print "desc ", desc
        _item = {item.get('rowNums'): {
            'TipoRegistroP': spaces('D',1),
            'NoLinea': spaces(item.get('rowNums'),4),
            'NoLineaSII': spaces(item.get('rowNums'),4),
            'TipoCodigo': spaces('ean',6),
            'CodigoItem': spaces(cod,35) or spaces('',35),
            'TipoCodigo2': spaces('',6),
            'CodigoItem2': spaces('',35),
            'TipoCodigo3': spaces('',6),
            'CodigoItem3': spaces('',35),
            'TipoCodigo4': spaces('',6),
            'CodigoItem4': spaces('',35),
            'TipoCodigo5': spaces('',6),
            'CodigoItem5': spaces('',35),
            'IndicadorFact': spaces(0,1) if tax_rate > 0 else spaces(1,1),
            'NombreItem': spaces(item.get('spartName').encode('utf8').decode('utf8'),80),
            'CantidadRef': spaces(0,18),
            'UnidadRef': spaces('',4),
            'PrecioRef': spaces(0,18),
            'CantidadItem': cantIte_,
            'Subcantidad': spaces(0,18),
            'DescSubcantidad': spaces('',35),
            'Subcantidad2': spaces(0,18),
            'DescSubcantidad2': spaces('',35),
            'Subcantidad3': spaces(0,18),
            'DescSubcantidad3': spaces('',35),
            'Subcantidad4': spaces(0,18),
            'DescSubcantidad4': spaces('',35),
            'Subcantidad5': spaces(0,18),
            'DescSubcantidad5': spaces('',35),
            'FechaElaboracion': spaces(0,8),
            'FechaVencimientoP': spaces(0,8),
            'UnidadMedida': spaces(item.get('unit').encode('utf8').decode('utf8'),4),
            'PrecioUnitarioItem': priceUni_,
            'PrecioUniOtraMoneda': spaces(0,18),
            'CodigoOtraMoneda': spaces(0,18),
            'FactorConversion': spaces('',10),# 14 enteros, 4 decimales
            'DescuentoPorcentaje': spaces(descPor_,5), #3 enteros, 2 decimales
            'DescuentoMonto': discountA_,
            'TipoSubdescuento': spaces('',1),
            'ValorSubdescuento': spaces('',18),
            'TipoSubdescuento2': spaces('',1),
            'ValorSubdescuento2': spaces('',18),
            'TipoSubdescuento3': spaces('',1),
            'ValorSubdescuento3': spaces('',18),
            'TipoSubdescuento4': spaces('',1),
            'ValorSubdescuento4': spaces('',18),
            'TipoSubdescuento5': spaces('',1),
            'ValorSubdescuento5': spaces('',18),
            'RecargoPorcentage': spaces(0,5),
            'MontoRecargo': spaces(0,18),
            'TipoSubrecargo': spaces('',1),
            'ValorSubrecargo': spaces('',18),
            'TipoSubrecargo2': spaces('',1),
            'ValorSubrecargo2': spaces('',18),
            'TipoSubrecargo3': spaces('',1),
            'ValorSubrecargo3': spaces('',18),
            'TipoSubrecargo4': spaces('',1),
            'ValorSubrecargo4': spaces('',18),
            'TipoSubrecargo5': spaces('',1),
            'ValorSubrecargo5': spaces('',18),
            'CodigoImpuesto': spaces(str(tax_code),6),
            'MontoItem': montoIte_,
            'DescripcionAdicional': spaces(desc.encode('utf8').decode('utf8'),1000),
            'TipoDocLiquida': spaces('',3),
            'ValorDetOtrMoneda': spaces(0,18),
            'RecargoOtrMoneda': spaces(0,18),
            'DescOtraMoneda': spaces(0,18),
            'TipoCodSubcantidad1': spaces('',10),
            'TipoCodSubcantidad2': spaces('',10),
            'TipoCodSubcantidad3': spaces('',10),
            'TipoCodSubcantidad4': spaces('',10),
            'TipoCodSubcantidad5': spaces('',10),
            'TipoCodigoSumple': spaces(0,6),
            'IndAgenteRetenedor': spaces('',1),
            }
        }
        items.append(_item)
        if tax_rate == 0:
            monto_neto_exento = monto_neto_exento + float(montoIte_)
            iva = 'exento'
        else:
            iva = 'gravado'
            monto_neto = monto_neto + float(montoIte_)
        if not impuestos.get(tax_rate):
            impuestos.update({iva: {
                'base': item.get('noTaxAmount'),
                'iva': tax_rate,
                'monto': item.get('taxAmount')}
            })
        else:
            base = impuestos.get(tax_rate).get('base') + item.get('noTaxAmount')
            impuestos.update({iva: {
                'base': impuestos.get(iva).get('base') + base,
                'iva': tax_rate,
                'monto': impuestos.get(iva).get('monto') + item.get('taxAmount')}})
        if not impuestos.get('gravado'):
            impuestos.update(
                {'gravado': {'base':0, 'iva':.19,'monto':0}}
                )
        if not impuestos.get('exento'):
            impuestos.update(
                {'exento': {'base':0, 'iva':0,'monto':0}}
                )
    recargos.update({
        'TipoRegistroR': spaces('R', 1),
        'NumLinea': spaces('1', 2),
        'TipoMov': spaces('D', 1),
        'Glosa': spaces("Descuento", 45),
        'TipoValor': spaces("$", 1),
        'Valor': ajusta_monto(rec_desc, 16, 2),
        'Indicador': spaces('0', 1),
        #'ValorOtraMoneda': ajusta_monto(0, 18, 0)
        'ValorOtraMoneda': spaces('', 18),
    })
    return items, monto_neto, monto_neto_exento, impuestos, invoice_items, recargos


def get_encabezado(invoice, customer_type, folio, env):
    #print "get_encabezado", invoice
    ciudad = ''
    comuna = ''
    giro = ''
    invoiceAttr = invoice.get('header').get('invoiceAttr')
    #valrutR = "96913280-K"
    #valrutS = "96913280-K"
    if invoice.get('businessInfo').get('taxRegistrationNo') is not False:
        #Rut Solicitante
        valrutS = validRut(invoice.get('businessInfo').get('taxRegistrationNo'))
        rutS = digVer(valrutS, 0)
        digVS = digVer(valrutS, 1)
        #Rut Receptor
        valrutR = validRut(invoice.get('businessInfo').get('taxRegistrationNo'))
        rutR = digVer(valrutR, 0)
        digVR = digVer(valrutR, 1)
    else:
        rutS = False
        digVS = False
        rutR = False
        digVR = False
    #print('Tipo Docto',invoiceAttr)
    # Tipo de Documeto 1
    glosas = []
    if invoiceAttr in [1,'1']:
        #Validaciones Campos
        methP = int(invoice.get('header').get('paymentMethod'))

        #Codigo interno solo para BOLETAS
        codInterno = invoice.get('header').get('userid')

        #Boletas Electronicas
        if customer_type == '50' or customer_type == 50:

            encabezado = {
            'TipoRegistro': spaces('E',1),
            'TipoDocumento': spaces('39',3),
            'NumeroFolio': spaces('',10),
            #'FechaEmision': spaces(time_zone_short(invoice.get('header').get('creationDate')),8),
            'FechaEmision': spaces(datetime.datetime.now().strftime("%Y%m%d"),8),
            'IndNoRebaja': spaces('',1),
            'TipoDespacho': spaces('0',1),
            'IndTipoTrasBien': spaces('',1),
            'IndServPeri': spaces('3',1),
            'IndMonBru': spaces('0',1),
            'FormaPago': spaces('',1),
            'FechaCancelacion': spaces('00000000',8),
            'PeriodoDesde': spaces('00000000',8),
            'PeriodoHasta': spaces('00000000',8),
            'MedioPago': spaces('',2),
            'TerminosPagoC': spaces('',4),
            'TerminosPagoD': spaces('',3),
            'FechaVencimiento': spaces(time_zone_short(invoice.get('header').get('creationDate')),8),
            'RutEmisor': spaces('99535120',8),
            'DigVerEmi': spaces('K',1),
            'NumRes': spaces('80',6) if env == 'PROD' else spaces('0',6),
            'NombreEmisor': spaces('Huawei (Chile) S.A.',100),
            'GiroComercialEmisor': spaces('VENTA AL POR MENOR DE COMPUTADORES, EQUIPO PERIFERICO, PROGRAMAS INFORMATICOS',80),
            'CodigoActEconomica':  spaces('474100',5),
            'SucEmiDoc': spaces('',30),
            'CodSucReg': spaces('',9),
            'DireccionOrigen': spaces('Rosario Norte 50',60),
            'ComunaOrigen': spaces('Las Condes',20),
            'CiudadOrigen': spaces('Santiago',15),
            'CodigoVendedor': spaces('',60),
            'RutMandante': spaces('00000000',8),
            'DigVerDemandante': spaces("",1),
            'RutReceptor': spaces('0' if rutR is False else rutR,8),
            'DigVerReceptor': spaces('0' if digVR is False else digVR,1),
            'CodIntReceptor': spaces(codInterno if rutR is False else '',20),
            'NombreReceptor': spaces(invoice.get('businessInfo').get('name').decode('utf8'),100),
            'GiroNegReceptor': spaces(giro.decode('utf8'),40),
            'ContReceptor': spaces('',80),
            'DirReceptor': spaces(invoice.get('shippingAddress').get('address').decode('utf8'),60),
            'ComunaReceptor': spaces(invoice.get('shippingAddress').get('city').decode('utf8'),20),
            #'CiudadReceptor': spaces(invoice.get('shippingAddress').get('province'),15),
            'CiudadReceptor': spaces('',15),
            'DirPosReceptor': spaces('',60),
            'ComunaPosReceptor': spaces('',20),
            'CiudadPosReceptor': spaces('',15),
            'RutSolicitante': spaces('',8),
            'DigSolFactura': spaces('',1),
            'PatenteTransporte': spaces('',8),
            'RutTransportista': spaces('',8),
            'DigTransportista': spaces('',1),
            'DirDestino': spaces('',60),
            'ComunaDestino': spaces('',20),
            'CiudadDestino': spaces('',15),
            'MontoNeto': spaces(0,18),
            'MontoExento': spaces(0,18),
            'MontoInformado': spaces(0,18),
            'TasaIva': spaces(19,5),
            'MontoIva': spaces(0,18),
            'MontoIvaRetenido': spaces('',18),
            'CreditoEspecial': spaces('',18),
            'GarantiaDeposito': spaces('',18),
            'MontoTotal': 0,
            'MontoNoFacturable': spaces('',18),
            'MontoPeriodo': 0,
            'SaldoAnterior': spaces('',18),
            'ValorPagar': spaces('',18),
            'Sistema': spaces('VDESuite',10),
            'Usuario': spaces('Huawei',20),
            'TemplateImpresion': spaces('BOLETA',18),
            'VersionTempImpre': spaces('2',18),
            'Impresora': spaces('',18),
            'ReceptorElectronico': spaces(0,1),
            'FormaAceptacion': spaces('A',1),
            'TipoFoleacion': spaces(1,1),
            'MontoIvaPropio': spaces('',18),
            'MontoIvaTerceros': spaces('',18),
            'CopiasImprimir': spaces('',2),
            'CodigoActEconomica2': spaces('',6),
            'IdenficadorInterno': spaces(invoice.get('trackId'),20),
            'ImprimirCedible': spaces('',1),
        }

        #Factura Electronicas
        elif customer_type == '51'or customer_type == 51:

            #print('Entra aqui a INV')
            ##Rut Receptor
            ##valrutR = validRut(invoice.get('businessInfo').get('taxRegistrationNo'))
            #valrutR = "96913280-K"
            #rutR = digVer(valrutR,0)
            #digVR = digVer(valrutR,1)

            ##Rut Solicitante
            ##valrutS = validRut(invoice.get('businessInfo').get('taxRegistrationNo'))
            #valrutS = "96913280-K"
            #rutS = digVer(valrutS,0)
            #digVS = digVer(valrutS,1)

            for extend in invoice.get('businessInfo').get('invoiceExtendList'):
                #print "extend ", extend
                if extend.get('paramCode') == 'province':
                    ciudad = extend.get('paramValue').encode('utf-8')
                if extend.get('paramCode') == 'city':
                    comuna = extend.get('paramValue').encode('utf-8')
                if extend.get('paramCode') == 'industry':
                    giro = extend.get('paramValue').encode('utf-8')

            encabezado = {
            'TipoRegistro': spaces('E',1),
            'TipoDocumento': spaces('33',3),
            'NumeroFolio': spaces('',10),
            #'FechaEmision': spaces(time_zone_short(invoice.get('header').get('creationDate')),8),
            'FechaEmision': spaces(datetime.datetime.now().strftime("%Y%m%d"),8),
            'IndNoRebaja': spaces('',1),
            'TipoDespacho': spaces('0',1),
            'IndTipoTrasBien': spaces('',1),
            'IndServPeri': spaces('3',1),
            'IndMonBru': spaces('0',1),
            'FormaPago': spaces(invoice.get('header').get('paymentForm'),1),
            'FechaCancelacion': spaces('00000000',8),
            'PeriodoDesde': spaces('00000000',8),
            'PeriodoHasta': spaces('00000000',8),
            'MedioPago': spaces(methodPay(methP),2),
            'TerminosPagoC': spaces('',4),
            'TerminosPagoD': spaces('',3),
            'FechaVencimiento': spaces(time_zone_short(invoice.get('header').get('creationDate')),8),
            'RutEmisor': spaces('99535120',8),
            'DigVerEmi': spaces('K',1),
            'NumRes': spaces('80',6) if env == 'PROD' else spaces('0',6),
            'NombreEmisor': spaces('Huawei (Chile) S.A.',100),
            'GiroComercialEmisor': spaces('VENTA AL POR MENOR DE COMPUTADORES, EQUIPO PERIFERICO, PROGRAMAS INFORMATICOS',80),
            'CodigoActEconomica':  spaces('47410',5),
            'SucEmiDoc': spaces('',30),
            'CodSucReg': spaces('',9),
            'DireccionOrigen': spaces('Rosario Norte 50',60),
            'ComunaOrigen': spaces('Las Condes',20),
            'CiudadOrigen': spaces('Santiago',15),
            'CodigoVendedor': spaces('',60),
            'RutMandante': spaces('00000000',8),
            'DigVerDemandante': spaces("",1),
            'RutReceptor': spaces(rutR,8),
            'DigVerReceptor': spaces(digVR,1),
            'CodIntReceptor': spaces('',20),
            'NombreReceptor': spaces(invoice.get('businessInfo').get('name').decode('utf8'),100),
            'GiroNegReceptor': spaces(giro.decode('utf8'),40),
            'ContReceptor': spaces('',80),
            #'DirReceptor': spaces(invoice.get('shippingAddress').get('address'),60),
            'DirReceptor': spaces(invoice.get('businessInfo').get('registeredAddress').decode('utf8'),60),
            'ComunaReceptor': spaces(comuna.decode('utf8'),20),
            #'CiudadReceptor': spaces(ciudad,15),
            'CiudadReceptor': spaces('',15),
            'DirPosReceptor': spaces('',60),
            'ComunaPosReceptor': spaces('',20),
            'CiudadPosReceptor': spaces('',15),
            'RutSolicitante': spaces(rutS,8),
            'DigSolFactura': spaces(digVS,1),
            'PatenteTransporte': spaces('',8),
            'RutTransportista': spaces('',8),
            'DigTransportista': spaces('',1),
            'DirDestino': spaces('',60),
            'ComunaDestino': spaces('',20),
            'CiudadDestino': spaces('',15),
            'MontoNeto': spaces(0,18),
            'MontoExento': spaces(0,18),
            'MontoInformado': spaces(0,18),
            'TasaIva': spaces("01900",5),
            'MontoIva': spaces(0,18),
            'MontoIvaRetenido': spaces('',18),
            'CreditoEspecial': spaces('',18),
            'GarantiaDeposito': spaces('',18),
            'MontoTotal': 0,
            'MontoNoFacturable': spaces('',18),
            'MontoPeriodo': 0,
            'SaldoAnterior': spaces('',18),
            'ValorPagar': spaces('',18),
            'Sistema': spaces('VDESuite',10),
            'Usuario': spaces('Huawei',20),
            'TemplateImpresion': spaces('FACTURA',18),
            'VersionTempImpre': spaces('2',18),
            'Impresora': spaces('',18),
            'ReceptorElectronico': spaces(0,1),
            'FormaAceptacion': spaces('A',1),
            'TipoFoleacion': spaces(1,1),
            'MontoIvaPropio': spaces('',18),
            'MontoIvaTerceros': spaces('',18),
            'CopiasImprimir': spaces('',2),
            'CodigoActEconomica2': spaces('469000',6),
            'IdenficadorInterno': spaces(invoice.get('trackId'),20),
            'ImprimirCedible': spaces('',1),
        }

    if invoiceAttr in [2,'2']:
        #Validaciones Campos
        methP = int(invoice.get('header').get('paymentMethod'))

        #Codigo interno solo para BOLETAS
        codInterno = invoice.get('header').get('userid')

        # Nota de Credito - Boletas
        if customer_type == '50' or customer_type == 50:

            encabezado = {
            'TipoRegistro': spaces('E',1),
            'TipoDocumento': spaces('61',3),
            'NumeroFolio': spaces('',10),
            #'FechaEmision': spaces(time_zone_short(invoice.get('header').get('creationDate')),8),
            'FechaEmision': spaces(datetime.datetime.now().strftime("%Y%m%d"),8),
            'IndNoRebaja': spaces('',1),
            'TipoDespacho': spaces('0',1),
            'IndTipoTrasBien': spaces('',1),
            'IndServPeri': spaces('3',1),
            'IndMonBru': spaces('0',1),
            'FormaPago': spaces('',1),
            'FechaCancelacion': spaces('00000000',8),
            'PeriodoDesde': spaces('00000000',8),
            'PeriodoHasta': spaces('00000000',8),
            'MedioPago': spaces('',2),
            'TerminosPagoC': spaces('',4),
            'TerminosPagoD': spaces('',3),
            'FechaVencimiento': spaces(time_zone_short(invoice.get('header').get('creationDate')),8),
            'RutEmisor': spaces('99535120',8),
            'DigVerEmi': spaces('K',1),
            'NumRes': spaces('80',6) if env == 'PROD' else spaces('0',6),
            'NombreEmisor': spaces('Huawei (Chile) S.A.',100),
            'GiroComercialEmisor': spaces('VENTA AL POR MENOR DE COMPUTADORES, EQUIPO PERIFERICO, PROGRAMAS INFORMATICOS',80),
            'CodigoActEconomica':  spaces('474100',5),
            'SucEmiDoc': spaces('',30),
            'CodSucReg': spaces('',9),
            'DireccionOrigen': spaces('Rosario Norte 50',60),
            'ComunaOrigen': spaces('Las Condes',20),
            'CiudadOrigen': spaces('Santiago',15),
            'CodigoVendedor': spaces('',60),
            'RutMandante': spaces('00000000',8),
            'DigVerDemandante': spaces("",1),
            'RutReceptor': spaces('0' if rutR is False else rutR,8),
            'DigVerReceptor': spaces('0' if digVR is False else digVR,1),
            'CodIntReceptor': spaces(codInterno if rutR is False else '',20),
            'NombreReceptor': spaces(invoice.get('businessInfo').get('name').decode('utf8'),100),
            'GiroNegReceptor': spaces(giro.decode('utf8'),40),
            'ContReceptor': spaces('',80),
            'DirReceptor': spaces(invoice.get('shippingAddress').get('address').decode('utf8'),60),
            'ComunaReceptor': spaces(invoice.get('shippingAddress').get('city').decode('utf8'),20),
            'CiudadReceptor': spaces(invoice.get('shippingAddress').get('province').decode('utf8'),15),
            'DirPosReceptor': spaces('',60),
            'ComunaPosReceptor': spaces('',20),
            'CiudadPosReceptor': spaces('',15),
            'RutSolicitante': spaces('',8),
            'DigSolFactura': spaces('',1),
            'PatenteTransporte': spaces('',8),
            'RutTransportista': spaces('',8),
            'DigTransportista': spaces('',1),
            'DirDestino': spaces('',60),
            'ComunaDestino': spaces('',20),
            'CiudadDestino': spaces('',15),
            'MontoNeto': spaces(0,18),
            'MontoExento': spaces(0,18),
            'MontoInformado': spaces(0,18),
            'TasaIva': spaces(19,5),
            'MontoIva': spaces(0,18),
            'MontoIvaRetenido': spaces('',18),
            'CreditoEspecial': spaces('',18),
            'GarantiaDeposito': spaces('',18),
            'MontoTotal': 0,
            'MontoNoFacturable': spaces('',18),
            'MontoPeriodo': 0,
            'SaldoAnterior': spaces('',18),
            'ValorPagar': spaces('',18),
            'Sistema': spaces('VDESuite',10),
            'Usuario': spaces('Huawei',20),
            'TemplateImpresion': spaces('NOTACREDITO',18),
            'VersionTempImpre': spaces('2',18),
            'Impresora': spaces('',18),
            'ReceptorElectronico': spaces(0,1),
            'FormaAceptacion': spaces('A',1),
            'TipoFoleacion': spaces(1,1),
            'MontoIvaPropio': spaces('',18),
            'MontoIvaTerceros': spaces('',18),
            'CopiasImprimir': spaces('',2),
            'CodigoActEconomica2': spaces('469000',6),
            'IdenficadorInterno': spaces(invoice.get('trackId'),20),
            'ImprimirCedible': spaces('',1),
        }
        #descomentar si se necesita glosas especiales para INV o BOLETAS
        #glosas.append({})

        # Nota de Credito - Factura Electronicas
        elif customer_type == '51'or customer_type == 51:

            #print('Entra aqui a INV')
            ##Rut Receptor
            ##valrutR = validRut(invoice.get('businessInfo').get('taxRegistrationNo'))
            #valrutR = "96913280-K"
            #rutR = digVer(valrutR,0)
            #digVR = digVer(valrutR,1)

            ##Rut Solicitante
            ##valrutS = validRut(invoice.get('businessInfo').get('taxRegistrationNo'))
            #valrutS = "96913280-K"
            #rutS = digVer(valrutS,0)
            #digVS = digVer(valrutS,1)

            for extend in invoice.get('businessInfo').get('invoiceExtendList'):
                #print "extend ", extend
                if extend.get('paramCode') == 'province':
                    ciudad = extend.get('paramValue').encode('utf8')
                if extend.get('paramCode') == 'city':
                    comuna = extend.get('paramValue').encode('utf8')
                if extend.get('paramCode') == 'industry':
                    giro = extend.get('paramValue').encode('utf8')

            encabezado = {
            'TipoRegistro': spaces('E',1),
            'TipoDocumento': spaces('61',3),
            'NumeroFolio': spaces('',10),
            #'FechaEmision': spaces(time_zone_short(invoice.get('header').get('creationDate')),8),
            'FechaEmision': spaces(datetime.datetime.now().strftime("%Y%m%d"),8),
            'IndNoRebaja': spaces('',1),
            'TipoDespacho': spaces('0',1),
            'IndTipoTrasBien': spaces('',1),
            'IndServPeri': spaces('3',1),
            'IndMonBru': spaces('0',1),
            'FormaPago': spaces(invoice.get('header').get('paymentForm'),1),
            'FechaCancelacion': spaces('00000000',8),
            'PeriodoDesde': spaces('00000000',8),
            'PeriodoHasta': spaces('00000000',8),
            'MedioPago': spaces(methodPay(methP),2),
            'TerminosPagoC': spaces('',4),
            'TerminosPagoD': spaces('',3),
            'FechaVencimiento': spaces(time_zone_short(invoice.get('header').get('creationDate')),8),
            'RutEmisor': spaces('99535120',8),
            'DigVerEmi': spaces('K',1),
            'NumRes': spaces('80',6) if env == 'PROD' else spaces('0',6),
            'NombreEmisor': spaces('Huawei (Chile) S.A.',100),
            'GiroComercialEmisor': spaces('VENTA AL POR MENOR DE COMPUTADORES, EQUIPO PERIFERICO, PROGRAMAS INFORMATICOS',80),
            'CodigoActEconomica':  spaces('47410',5),
            'SucEmiDoc': spaces('',30),
            'CodSucReg': spaces('',9),
            'DireccionOrigen': spaces('Rosario Norte 50',60),
            'ComunaOrigen': spaces('Las Condes',20),
            'CiudadOrigen': spaces('Santiago',15),
            'CodigoVendedor': spaces('',60),
            'RutMandante': spaces('00000000',8),
            'DigVerDemandante': spaces("",1),
            'RutReceptor': spaces(rutR,8),
            'DigVerReceptor': spaces(digVR,1),
            'CodIntReceptor': spaces('',20),
            'NombreReceptor': spaces(invoice.get('businessInfo').get('name').decode('utf8'),100),
            'GiroNegReceptor': spaces(giro.decode('utf8'),40),
            'ContReceptor': spaces('',80),
            #'DirReceptor': spaces(invoice.get('shippingAddress').get('address'),60),
            'DirReceptor': spaces(invoice.get('businessInfo').get('registeredAddress').decode('utf8'),60),
            'ComunaReceptor': spaces(comuna.decode('utf8'),20),
            'CiudadReceptor': spaces(ciudad.decode('utf8'),15),
            'DirPosReceptor': spaces('',60),
            'ComunaPosReceptor': spaces('',20),
            'CiudadPosReceptor': spaces('',15),
            'RutSolicitante': spaces(rutS,8),
            'DigSolFactura': spaces(digVS,1),
            'PatenteTransporte': spaces('',8),
            'RutTransportista': spaces('',8),
            'DigTransportista': spaces('',1),
            'DirDestino': spaces('',60),
            'ComunaDestino': spaces('',20),
            'CiudadDestino': spaces('',15),
            'MontoNeto': spaces(0,18),
            'MontoExento': spaces(0,18),
            'MontoInformado': spaces(0,18),
            'TasaIva': spaces("01900",5),
            'MontoIva': spaces(0,18),
            'MontoIvaRetenido': spaces('',18),
            'CreditoEspecial': spaces('',18),
            'GarantiaDeposito': spaces('',18),
            'MontoTotal': 0,
            'MontoNoFacturable': spaces('',18),
            'MontoPeriodo': 0,
            'SaldoAnterior': spaces('',18),
            'ValorPagar': spaces('',18),
            'Sistema': spaces('VDESuite',10),
            'Usuario': spaces('Huawei',20),
            'TemplateImpresion': spaces('NOTACREDITO',18),
            'VersionTempImpre': spaces('2',18),
            'Impresora': spaces('',18),
            'ReceptorElectronico': spaces(0,1),
            'FormaAceptacion': spaces('A',1),
            'TipoFoleacion': spaces(1,1),
            'MontoIvaPropio': spaces('',18),
            'MontoIvaTerceros': spaces('',18),
            'CopiasImprimir': spaces('',2),
            'CodigoActEconomica2': spaces('469000',6),
            'IdenficadorInterno': spaces(invoice.get('trackId'),20),
            'ImprimirCedible': spaces('',1),
        }
        #descomentar si se necesita glosas especiales para NC
        #glosas.append({})

    totales = {
        'TipoRegistroT': spaces('T',1),
        'FolioPrimerR': spaces('',10),
        'FolioUltimoR': spaces('',10),
        'CantidadDoctos': spacesjmm(1,10),
        'RutFirmaDoc': spaces('',8),
        'DVFirmaDoc': spaces('',1),
        'AreaFirmaDoc': spaces('',30),
    }
    glosas.append({
        'TipoRegistroG': spaces('G',1),
        'NumeroLineaG': spaces('1',2),
        'DescriptorGlosa': spaces('GGUIA',20),
        'TextoGlosa': spaces(invoice.get('header').get('orderNo'),100),
    })
    return encabezado, totales, glosas


def validate_json(nodo, val):
    if nodo == 'type':
        if (val==50) or (val == "50"):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1000', 'info': ' "type" node error - the value sent "'+ str(val)+'": Values 50 or 51 only allowed'}
    if nodo == 'id':
        if (val != '' or val is not False):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1001', 'info': ' "id" node error - the value sent "'+ str(val)+'": cannot be empty'}
    if nodo == 'idType':
        if(val == '1' or val == '2' or val == 1 or val == 2):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1002',
            'info': ' "idType" node error - the value sent "'+ str(val)+'": Values 1 or 2 only allowed'}
    if nodo == 'postalCode':
        if(len(val) <= 6):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1003',
            'info': ' "postalCode" node error - the value sent "'+ str(val)+'": only length 6 digits'}
    if nodo == 'cityName':
        #if val.encode('utf-8') not in vdeCity:
        if val not in vdeCity:
            return_ = False
            return_desc = {'Code':'1004',
            'info': ' "cityName" node error - the value sent "'+ val.encode('utf-8').strip()+'": is not included into the DIAN list'}
        else:
            return_ = True
            return_desc = True
    if nodo == 'department':
        #if val.encode('utf-8') not in vdeDepartment:
        if val not in vdeDepartment:
            return_ = False
            return_desc = {'Code':'1005',
            'info': ' "department" node error - the value sent "'+ val.encode('utf-8').strip()+'": is not included into the DIAN list'}
        else:
            return_ = True
            return_desc = True
    if nodo == 'countryCode':
        if(val in vdeCountryCode):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1006',
            'info': ' "countryCode" node error - the value sent "'+ val.encode('utf-8').strip()+'": is not included into the DIAN list'}
    if nodo == 'referencedInvoiceNumber':
        if (val != '' or val is not False):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1007',
            'info': ' "referencedInvoiceNumber" node error - the value sent "'+ str(val)+'": cannot be empty'}
    if nodo == 'rut':
        rut_val = validarRut(val)
        if rut_val is True:
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1008',
            'info': ' "taxRegistrationNo" node error - the value sent "'+ str(val)+'": is not a correct RUT'}
    return return_, return_desc


def putInvoiceResult(_trackId, _orderNo, _invoiceNo, _bytePDF, _errorMessage, _invoiceFile, _invoiceHeader, _invoiceItems, _env):
    guid_response = requests.get(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/guid")
    _guid = guid_response.json().get('guid')
    #print "guid ", _guid
    data = {
        'trackId': _trackId if _trackId is not False else None,
        'orderNo': _orderNo if _orderNo is not False else None,
        'invoiceNo': _invoiceNo if _invoiceNo is not False else None,
        'bytePDF': _bytePDF if _bytePDF is not False else None,
        'errorMessage': _errorMessage if _errorMessage is not False else None,
        'invoiceFile': _invoiceFile if _invoiceFile is not False else None,
        'invoiceHeader': [_invoiceHeader] if _invoiceHeader is not False else None,
        'invoiceItems': _invoiceItems if _invoiceItems is not False else None,
        'guid': _guid,
    }
    #print "data ", data
    data = json_to_utf8(data)
    #print "data ", data
    data = json.dumps(data)
    #print "data ", data
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    params_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getParamsCallback",headers=headers, data=data)
    params = params_response.json()
    #print "params ", params
    hash_ = params.get('hash')
    data={
        "country": "CL",
        "environment": _env,
        "guid": _guid}
    #print "data ", data
    token_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getToken", data=data)
    #print "token_response ", token_response.json()
    urlHW_I = json_to_utf8(token_response.json().get('urlCallback'))
    appKey = json_to_utf8(token_response.json().get('appKey'))
    token = json_to_utf8(token_response.json().get('token'))
    data = {
        'companyCode': 'CLHW_VDE',
        'params': hash_,
        'token': token
    }
    #print "data ", data
    data = json.dumps(data)
    #print "data ", data
    headers = {
        'X-HW-ID': 'com.huawei.invoice_cloud',
        'X-HW-APPKEY': appKey,
        'Content-Type': 'application/json'
    }
    hw_response = requests.post(urlHW_I, headers=headers, data=data)
    #print "hw_response ", hw_response.text
    return hw_response.json(), params.get('json')



def putInvoiceResultLog(_trackId, _orderNo, _invoiceNo, _bytePDF, _errorMessage, _invoiceFile, _invoiceHeader, _invoiceItems, _env):
    guid_response = requests.get(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/guid")
    _guid = guid_response.json().get('guid')
    #print "guid ", _guid
    data = {
        'trackId': _trackId if _trackId is not False else None,
        'orderNo': _orderNo if _orderNo is not False else None,
        'invoiceNo': _invoiceNo if _invoiceNo is not False else None,
        'bytePDF': _bytePDF if _bytePDF is not False else None,
        'errorMessage': _errorMessage if _errorMessage is not False else None,
        'invoiceFile': _invoiceFile if _invoiceFile is not False else None,
        'invoiceHeader': [_invoiceHeader] if _invoiceHeader is not False else None,
#        'invoiceItems': [_invoiceItems] if _invoiceItems is not False else None,
        'invoiceItems': _invoiceItems if isinstance(_invoiceItems,list) else [_invoiceItems] if _invoiceItems is not False else none,
        'guid': _guid,
    }
    #print "data ", data
    data = json_to_utf8(data)
    #print "data ", data
    data = json.dumps(data)
    #print "data ", data
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    params_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getParamsCallback",headers=headers, data=data)
    params = params_response.json()
    #print "params ", params
    hash_ = params.get('hash')
    data={
        "country": "CL",
        "environment": _env,
        "guid": _guid}
    #print "data ", data
    token_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getToken", data=data)
    #print "token_response ", token_response.json()
    urlHW_I = json_to_utf8(token_response.json().get('urlCallback'))
    appKey = json_to_utf8(token_response.json().get('appKey'))
    token = json_to_utf8(token_response.json().get('token'))
    data = {
        'companyCode': 'CLHW_VDE',
        'params': hash_,
        'token': token
    }
    #print "data ", data
    data = json.dumps(data)
    #print "data ", data
    headers = {
        'X-HW-ID': 'com.huawei.invoice_cloud',
        'X-HW-APPKEY': appKey,
        'Content-Type': 'application/json'
    }
    hw_response = requests.post(urlHW_I, headers=headers, data=data)
    #print "hw_response ", hw_response.text
    return hw_response.json(), params.get('json')
