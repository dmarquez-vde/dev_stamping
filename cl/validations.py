# -*- coding: utf-8 -*-
from catalogs import vdeCity, vdeDepartment, vdeCountryCode
import locale
import requests
from requests.auth import HTTPBasicAuth


def validate_json(nodo, val):
    if nodo == 'id':
        if (val != '' or val is not False):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1001', 'info': ' "id" node error - the value sent "'+ str(val)+'": cannot be empty'}
    if nodo == 'idType':
        if(val == '1' or val == '2' or val == 1 or val == 2):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1002',
            'info': ' "idType" node error - the value sent "'+ str(val)+'": Values 1 or 2 only allowed'}
    if nodo == 'postalCode':
        if(len(val) <= 6):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1003',
            'info': ' "postalCode" node error - the value sent "'+ str(val)+'": only length 6 digits'}
    if nodo == 'cityName':
        #if val.encode('utf-8') not in vdeCity:
        if val not in vdeCity:
            return_ = False
            return_desc = {'Code':'1004',
            'info': ' "cityName" node error - the value sent "'+ val.encode('utf-8').strip()+'": is not included into the DIAN list'}
        else:
            return_ = True
            return_desc = True
    if nodo == 'department':
        #if val.encode('utf-8') not in vdeDepartment:
        if val not in vdeDepartment:
            return_ = False
            return_desc = {'Code':'1005',
            'info': ' "department" node error - the value sent "'+ val.encode('utf-8').strip()+'": is not included into the DIAN list'}
        else:
            return_ = True
            return_desc = True
    if nodo == 'countryCode':
        if(val in vdeCountryCode):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1006',
            'info': ' "countryCode" node error - the value sent "'+ val.encode('utf-8').strip()+'": is not included into the DIAN list'}
    if nodo == 'referencedInvoiceNumber':
        if (val != '' or val is not False):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1007',
            'info': ' "referencedInvoiceNumber" node error - the value sent "'+ str(val)+'": cannot be empty'}
    return return_, return_desc


def number_format(num, places=0):
    return locale.format_string("%.*f", (places, num), True)


def decimals(numero):
    numero = float(numero)
    res = number_format(numero, 4)
    ultimos = res.split(".")
    valorStr = str(ultimos[1])
    valor = valorStr[1]
    valor3 = valorStr[2]
    ultValores = valorStr[0] + valorStr[1]
    #print(valor)
    if int(valor) < 5:
        resultado = number_format(numero, 2)
        #print(resultado)
    elif int(valor) > 5 and int(valor) <= 9:
        resultado = number_format(numero, 2)
        #print(resultado)
    elif int(valor) == 5:
        if int(valor3) % 2 == 0:
            resultado = ultimos[0] + '.' + ultValores
            #print(resultado)
        else:
            resultado = number_format(numero, 2)
            #print(resultado)
    return float(resultado)

def putInvoiceResult(_trackId, _orderNo, _invoiceNo, _bytePDF, _errorMessage, _invoiceFile, _invoiceHeader, _invoiceItems, _env):
    guid_response = requests.get(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/guid")
    _guid = guid_response.json().get('guid')
    print "guid ", _guid
    data = {
        'trackId': _trackId if _trackId is not False else None,
        'orderNo': _orderNo if _orderNo is not False else None,
        'invoiceNo': _invoiceNo if _invoiceNo is not False else None,
        'bytePDF': _bytePDF if _bytePDF is not False else None,
        'errorMessage': _errorMessage if _errorMessage is not False else None,
        'invoiceFile': _invoiceFile if _invoiceFile is not False else None,
        'invoiceHeader': [_invoiceHeader] if _invoiceHeader is not False else None,
        #        'invoiceItems': [_invoiceItems] if _invoiceItems is not False else None,
        'invoiceItems': _invoiceItems if isinstance(_invoiceItems,list) else [_invoiceItems] if _invoiceItems is not False else None,
        'guid': _guid,
    }
    print "data ", data
    data = json_to_utf8(data)
    print "data ", data
    data = json.dumps(data)
    print "data ", data
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    params_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getParamsCallback",headers=headers, data=data)
    params = params_response.json()
    print "params ", params
    hash_ = params.get('hash')
    data={
        "country": "CO",
        "environment": _env,
        "guid": _guid}
    print "data ", data
    token_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getToken", data=data)
    print "token_response ", token_response.json()
    urlHW_I = json_to_utf8(token_response.json().get('urlCallback'))
    appKey = json_to_utf8(token_response.json().get('appKey'))
    token = json_to_utf8(token_response.json().get('token'))
    data = {
        'companyCode': 'COHW_VDE',
        'params': hash_,
        'token': token
    }
    print "data ", data
    data = json.dumps(data)
    print "data ", data
    headers = {
        'X-HW-ID': 'com.huawei.invoice_cloud',
        'X-HW-APPKEY': appKey,
        'Content-Type': 'application/json'
    }
    hw_response = requests.post(urlHW_I, headers=headers, data=data)
    print "hw_response ", hw_response.text
    return hw_response.json(), params.get('json')