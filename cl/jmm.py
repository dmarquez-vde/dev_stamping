# -*- coding: utf-8 -*-
from dateutil import parser
import pytz
from validations import *
import ast
import json
import logging
from logging.handlers import RotatingFileHandler

from catalogs import vdeCity, vdeDepartment, vdeCountryCode
import locale
import requests
from requests.auth import HTTPBasicAuth
import json
#from structure import *

#log = get_logger("inv", "log/bol.log", True)

def json_to_utf8(json_):
    #print json_
    inv = json_
    #for inv in json_:
    if isinstance(inv, dict):
        for key in inv.keys():
            #print "key ", key
            if isinstance(inv.get(key), dict):
                #print "es dict"
                keys = inv.get(key).keys()
                for key_ in keys:
                    #print "key_ ", key_
                    if isinstance(inv.get(key).get(key_), list):
                        #print "es list dentro de dict"
                        subs1 = []
                        for sub in inv.get(key).get(key_):
                            #print "sub ",sub
                            recurs = json_to_utf8(sub)
                            #print "recurs ", recurs
                            subs1.append(recurs)
                        inv.get(key).update({key_:subs1})
                    else:
                        val = inv.get(key).get(key_)
                        if  not (isinstance(val, float) or isinstance(val, int)):
                            if val == None:
                                inv.get(key).update({key_: False})
                            else:
                                inv.get(key).update({key_: unicode(val).encode('utf-8')})
            elif isinstance(inv.get(key), list):
                #print "es lista"
                inv.get(key)
                subs = []
                for sub in inv.get(key):
                    #print "sub ",sub
                    recurs = json_to_utf8(sub)
                    #print "recurs ", recurs
                    subs.append(recurs)
                inv.update({key:subs})
            else:
                val = inv.get(key)
                if  not (isinstance(val, float) or isinstance(val, int)):
                    if val == None:
                        inv.update({key: False})
                    else:
                        #print "val ", val
                        #print type(val)
                        inv.update({key: val})
        #print "limpio ", json_
        return inv
    else:
        if json_ is None:
            #print "es None"
            return False
        #print "no es dict ", json_
        if  not (isinstance(json_, float) or isinstance(json_, int)):
            #print "no es num "
            if json_ == None:
                return False
            else:
                return unicode(json_).encode('utf-8')

def create_txt(invoice, _type, folio_actual, _hash_cfdi,  _env):
    print "create_txt", type(invoice)
    if _type in [50,'50']:
        _path ="txt/bol/"
    elif _type in [51,'51']:
        _path ="txt/inv/"
    encabezado, totales = get_encabezado(invoice,_type,folio_actual, _env)
    #print "encabezado ", encabezado
    itemss, neto, exento, impuestos, invoice_items = get_items(invoice, _type,  _env)
    print "impuestos ", impuestos
    enc  = ""
    ite = ""
    tot = ""

    # Neto
    if _type in [50,'50']:
        neto_ = str(round(0.0,6))
    elif _type in [51,'51']:
        neto_ = str(round(neto,6))
    x = neto_.split('.')
    neto__ = int(x[0] + spacesjmm(x[1],6))

    # Exento
    exento_ = str(round(exento,6))
    ex = exento_.split('.')
    exento__ = int(ex[0] + spacesjmm(ex[1],6))

    #Monto Informado
    monInf_ = str(round(impuestos.get('gravado').get('base'),6))
    monI = monInf_.split('.')
    monInf__ = int(monI[0] + spacesjmm(monI[1],6))

    # TasaIva
    if _type in [50,'50']:
        tasaIva_ = str(round(0.0,6))
    elif _type in [51,'51']:
        tasaIva_ = str(round(impuestos.get('gravado').get('iva'),6))
    tas = tasaIva_.split('.')
    tasaIva__ = int(tas[0] + spacesjmm(tas[1],6))

    # MontoIva
    if _type in [50,'50']:
        montoIva_ = str(round(0.0,6))
    elif _type in [51,'51']:
        montoIva_ = str(round(impuestos.get('gravado').get('monto'),6))
    mon = montoIva_.split('.')
    montoIva__ = int(mon[0] + spacesjmm(mon[1],6))

    # MontoTotal
    if _type in [50,'50']:
        montoTot_ = str(round(neto,6))
    elif _type in [51,'51']:
        montoTot_ = str(round(neto + exento + impuestos.get('gravado').get('monto'),6))
    monT = montoTot_.split('.')
    montoTot__ = int(monT[0] + spacesjmm(monT[1],6))

    # MontoPeriodo
    montoPe_ = str(round(neto + exento + impuestos.get('gravado').get('monto'),6))
    monP = montoPe_.split('.')
    montoPe__ = int(monP[0] + spacesjmm(monP[1],6))


    encabezado.update({
        'MontoNeto': spaces(neto__,18),
        'MontoExento': spaces(exento__,18),
        'MontoInformado': spaces(monInf__,18),
        'TasaIva': spaces(tasaIva__,5),
        'MontoIva': spaces(montoIva__,18),
        'MontoTotal': spaces(montoTot__,18) ,
        'MontoPeriodo': spaces(montoPe__,18),
    })
    enc = enc + encabezado.get('TipoRegistro')
    enc = enc + encabezado.get('TipoDocumento')
    enc = enc + encabezado.get('NumeroFolio')
    enc = enc + encabezado.get('FechaEmision')
    enc = enc + encabezado.get('IndNoRebaja')
    enc = enc + encabezado.get('TipoDespacho')
    enc = enc + encabezado.get('IndTipoTrasBien')
    enc = enc + encabezado.get('IndServPeri')
    enc = enc + encabezado.get('IndMonBru')
    enc = enc + encabezado.get('FormaPago')
    enc = enc + encabezado.get('FechaCancelacion')
    enc = enc + encabezado.get('PeriodoDesde')
    enc = enc + encabezado.get('PeriodoHasta')
    enc = enc + encabezado.get('MedioPago')
    enc = enc + encabezado.get('TerminosPagoC')
    enc = enc + encabezado.get('TerminosPagoD')
    enc = enc + encabezado.get('FechaVencimiento')
    enc = enc + encabezado.get('RutEmisor')
    enc = enc + encabezado.get('DigVerEmi')
    enc = enc + encabezado.get('NumRes')
    enc = enc + encabezado.get('NombreEmisor')
    enc = enc + encabezado.get('GiroComercialEmisor')
    enc = enc + encabezado.get('CodigoActEconomica')
    enc = enc + encabezado.get('SucEmiDoc')
    enc = enc + encabezado.get('CodSucReg')
    enc = enc + encabezado.get('DireccionOrigen')
    enc = enc + encabezado.get('ComunaOrigen')
    enc = enc + encabezado.get('CiudadOrigen')
    enc = enc + encabezado.get('CodigoVendedor')
    enc = enc + encabezado.get('RutMandante')
    enc = enc + encabezado.get('DigVerDemandante')
    enc = enc + encabezado.get('RutReceptor')
    enc = enc + encabezado.get('DigVerReceptor')
    enc = enc + encabezado.get('CodIntReceptor')
    enc = enc + encabezado.get('NombreReceptor')
    enc = enc + encabezado.get('GiroNegReceptor')
    enc = enc + encabezado.get('ContReceptor')
    enc = enc + encabezado.get('DirReceptor')
    enc = enc + encabezado.get('ComunaReceptor')
    enc = enc + encabezado.get('CiudadReceptor')
    enc = enc + encabezado.get('DirPosReceptor')
    enc = enc + encabezado.get('ComunaPosReceptor')
    enc = enc + encabezado.get('CiudadPosReceptor')
    enc = enc + encabezado.get('RutSolicitante')
    enc = enc + encabezado.get('DigSolFactura')
    enc = enc + encabezado.get('PatenteTransporte')
    enc = enc + encabezado.get('RutTransportista')
    enc = enc + encabezado.get('DigTransportista')
    enc = enc + encabezado.get('DirDestino')
    enc = enc + encabezado.get('ComunaDestino')
    enc = enc + encabezado.get('CiudadDestino')
    enc = enc + encabezado.get('MontoNeto')
    enc = enc + encabezado.get('MontoExento')
    enc = enc + encabezado.get('MontoInformado')
    enc = enc + encabezado.get('TasaIva')
    enc = enc + encabezado.get('MontoIva')
    enc = enc + encabezado.get('MontoIvaRetenido')
    enc = enc + encabezado.get('CreditoEspecial')
    enc = enc + encabezado.get('GarantiaDeposito')
    enc = enc + encabezado.get('MontoTotal')
    enc = enc + encabezado.get('MontoNoFacturable')
    enc = enc + encabezado.get('MontoPeriodo')
    enc = enc + encabezado.get('SaldoAnterior')
    enc = enc + encabezado.get('ValorPagar')
    enc = enc + encabezado.get('Sistema')
    enc = enc + encabezado.get('Usuario')
    enc = enc + encabezado.get('TemplateImpresion')
    enc = enc + encabezado.get('VersionTempImpre')
    enc = enc + encabezado.get('Impresora')
    enc = enc + encabezado.get('ReceptorElectronico')
    enc = enc + encabezado.get('FormaAceptacion')
    enc = enc + encabezado.get('TipoFoleacion')
    enc = enc + encabezado.get('MontoIvaPropio')
    enc = enc + encabezado.get('MontoIvaTerceros')
    enc = enc + encabezado.get('CopiasImprimir')
    enc = enc + encabezado.get('CodigoActEconomica2')
    enc = enc + encabezado.get('IdenficadorInterno')
    enc = enc + encabezado.get('ImprimirCedible')
    enc = enc + "\n"
    cont = 1
    tot = tot + totales.get('TipoRegistroT')
    tot = tot + totales.get('FolioPrimerR')
    tot = tot + totales.get('FolioUltimoR')
    tot = tot + totales.get('CantidadDoctos')
    tot = tot + totales.get('RutFirmaDoc')
    tot = tot + totales.get('DVFirmaDoc')
    tot = tot + totales.get('AreaFirmaDoc')
    tot = tot + "\n"
    for items in itemss:
        print "items ", items
        ite = ite + items.get(str(cont)).get('TipoRegistroP')
        ite = ite + items.get(str(cont)).get('NoLinea')
        ite = ite + items.get(str(cont)).get('NoLineaSII')
        ite = ite + items.get(str(cont)).get('TipoCodigo')
        ite = ite + items.get(str(cont)).get('CodigoItem')
        ite = ite + items.get(str(cont)).get('TipoCodigo2')
        ite = ite + items.get(str(cont)).get('CodigoItem2')
        ite = ite + items.get(str(cont)).get('TipoCodigo3')
        ite = ite + items.get(str(cont)).get('CodigoItem3')
        ite = ite + items.get(str(cont)).get('TipoCodigo4')
        ite = ite + items.get(str(cont)).get('CodigoItem4')
        ite = ite + items.get(str(cont)).get('TipoCodigo5')
        ite = ite + items.get(str(cont)).get('CodigoItem5')
        ite = ite + items.get(str(cont)).get('IndicadorFact')
        ite = ite + items.get(str(cont)).get('NombreItem')
        ite = ite + items.get(str(cont)).get('CantidadRef')
        ite = ite + items.get(str(cont)).get('UnidadRef')
        ite = ite + items.get(str(cont)).get('PrecioRef')
        ite = ite + items.get(str(cont)).get('CantidadItem')
        ite = ite + items.get(str(cont)).get('Subcantidad')
        ite = ite + items.get(str(cont)).get('DescSubcantidad')
        ite = ite + items.get(str(cont)).get('Subcantidad2')
        ite = ite + items.get(str(cont)).get('DescSubcantidad2')
        ite = ite + items.get(str(cont)).get('Subcantidad3')
        ite = ite + items.get(str(cont)).get('DescSubcantidad3')
        ite = ite + items.get(str(cont)).get('Subcantidad4')
        ite = ite + items.get(str(cont)).get('DescSubcantidad4')
        ite = ite + items.get(str(cont)).get('Subcantidad5')
        ite = ite + items.get(str(cont)).get('DescSubcantidad5')
        ite = ite + items.get(str(cont)).get('FechaElaboracion')
        ite = ite + items.get(str(cont)).get('FechaVencimientoP')
        ite = ite + items.get(str(cont)).get('UnidadMedida')
        ite = ite + items.get(str(cont)).get('PrecioUnitarioItem')
        ite = ite + items.get(str(cont)).get('PrecioUniOtraMoneda')
        ite = ite + items.get(str(cont)).get('CodigoOtraMoneda')
        ite = ite + items.get(str(cont)).get('FactorConversion')
        ite = ite + items.get(str(cont)).get('DescuentoPorcentaje')
        ite = ite + items.get(str(cont)).get('DescuentoMonto')
        ite = ite + items.get(str(cont)).get('TipoSubdescuento')
        ite = ite + items.get(str(cont)).get('ValorSubdescuento')
        ite = ite + items.get(str(cont)).get('TipoSubdescuento2')
        ite = ite + items.get(str(cont)).get('ValorSubdescuento2')
        ite = ite + items.get(str(cont)).get('TipoSubdescuento3')
        ite = ite + items.get(str(cont)).get('ValorSubdescuento3')
        ite = ite + items.get(str(cont)).get('TipoSubdescuento4')
        ite = ite + items.get(str(cont)).get('ValorSubdescuento4')
        ite = ite + items.get(str(cont)).get('TipoSubdescuento5')
        ite = ite + items.get(str(cont)).get('ValorSubdescuento5')
        ite = ite + items.get(str(cont)).get('RecargoPorcentage')
        ite = ite + items.get(str(cont)).get('MontoRecargo')
        ite = ite + items.get(str(cont)).get('TipoSubrecargo')
        ite = ite + items.get(str(cont)).get('ValorSubrecargo')
        ite = ite + items.get(str(cont)).get('TipoSubrecargo2')
        ite = ite + items.get(str(cont)).get('ValorSubrecargo2')
        ite = ite + items.get(str(cont)).get('TipoSubrecargo3')
        ite = ite + items.get(str(cont)).get('ValorSubrecargo3')
        ite = ite + items.get(str(cont)).get('TipoSubrecargo4')
        ite = ite + items.get(str(cont)).get('ValorSubrecargo4')
        ite = ite + items.get(str(cont)).get('TipoSubrecargo5')
        ite = ite + items.get(str(cont)).get('ValorSubrecargo5')
        ite = ite + items.get(str(cont)).get('CodigoImpuesto')
        ite = ite + items.get(str(cont)).get('MontoItem')
        ite = ite + items.get(str(cont)).get('DescripcionAdicional')
        ite = ite + items.get(str(cont)).get('TipoDocLiquida')
        ite = ite + items.get(str(cont)).get('ValorDetOtrMoneda')
        ite = ite + items.get(str(cont)).get('RecargoOtrMoneda')
        ite = ite + items.get(str(cont)).get('DescOtraMoneda')
        ite = ite + items.get(str(cont)).get('TipoCodSubcantidad1')
        ite = ite + items.get(str(cont)).get('TipoCodSubcantidad2')
        ite = ite + items.get(str(cont)).get('TipoCodSubcantidad3')
        ite = ite + items.get(str(cont)).get('TipoCodSubcantidad4')
        ite = ite + items.get(str(cont)).get('TipoCodSubcantidad5')
        ite = ite + items.get(str(cont)).get('TipoCodigoSumple')
        ite = ite + items.get(str(cont)).get('IndAgenteRetenedor')
        ite = ite + '\n'
        cont = cont + 1
    file_name = str(_hash_cfdi) + '.txt'
    try:
        with open(_path + file_name, 'w') as txt:
            txt.write(enc)
            txt.write(ite)
            txt.write(tot)
        txt = enc + ite + tot
        return file_name, txt, invoice_items
    except:
        log.error('ERROR CREANDO FILE TXT')


def get_logger(logger_name, archivo, create_file=False):
    # create logger for prd_ci
    log = logging.getLogger(logger_name)
    log.setLevel(level=logging.INFO)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s %(levelname)s ? %(message)s')
    if create_file:
        # create file handler for logger.
        #fh = logging.FileHandler("log/inv.log")
        fh = RotatingFileHandler(archivo, mode='a', maxBytes=1000000, encoding=None, delay=0)
        fh.setLevel(level=logging.DEBUG)
        fh.setFormatter(formatter)
    # reate console handler for logger.
    ch = logging.StreamHandler()
    ch.setLevel(level=logging.DEBUG)
    ch.setFormatter(formatter)
    # add handlers to logger.
    if create_file:
        log.addHandler(fh)
    log.addHandler(ch)
    return  log


def time_zone_long(date):
    get_date_obj = parser.parse(date)
    #print(get_date_obj.strftime("%Y-%m-%d %H:%M:%S%z"))
    bogota = pytz.timezone('America/Chile')
    observationTime = get_date_obj.astimezone(bogota)
    #print(observationTime.strftime("%Y-%m-%d %H:%M:%S%z"))
    #print(observationTime.strftime("%Y-%m-%d %H:%M:%S"))
    return observationTime.strftime("%Y-%m-%d %H:%M:%S")


def time_zone_short_(date):
    get_date_obj = parser.parse(date)
    #print(get_date_obj.strftime("%Y-%m-%d %H:%M:%S%z"))
    bogota = pytz.timezone('America/Santiago')
    observationTime = get_date_obj.astimezone(bogota)
    #print(observationTime.strftime("%Y-%m-%d %H:%M:%S%z"))
    #print(observationTime.strftime("%Y-%m-%d %H:%M:%S"))
    return observationTime.strftime("%Y-%m-%d")


def time_zone_short(date):
    get_date_obj = parser.parse(date)
    bogota = pytz.timezone('America/Santiago')
    observationTime = get_date_obj.astimezone(bogota)
    return observationTime.strftime("%Y%m%d")


def spaces(cadena, longitud):
    if isinstance(cadena, int) or isinstance(cadena, float):
        char = "0"
        is_num = True
    else:
        char = " "
        is_num = False
    new_cadena = ""
    cadena = str(cadena)[:longitud]
    for key in range(longitud - len(cadena)):
        new_cadena = new_cadena + char
    if is_num is True:
        return new_cadena + str(cadena)
        #return str(cadena) + new_cadena
    elif is_num is False:
        return str(cadena) + new_cadena

def spacesjmm(cadena, longitud):
    if isinstance(cadena, int) or isinstance(cadena, float):
        char = "0"
        is_num = True
    else:
        char = "0"
        is_num = False
    new_cadena = ""
    for key in range(longitud - len(str(cadena))):
        new_cadena = new_cadena + char
    if is_num is True:
        return new_cadena + str(cadena)
        #return str(cadena) + new_cadena
    elif is_num is False:
        return str(cadena) + new_cadena

def methodPay(cadena):

    switcher = {
        47: "CH",
        2: "CF",
        3: "LT",
        4: "EF",
        5: "PE",
        6: "TC",
    }
    return switcher.get(cadena, "OT")

def validRut(cadena):
    switcher = {
        0: "",
    }
    return switcher.get(cadena, cadena)

def digVer(valrut,val):
    if valrut == '':
        return ''
    else:
        x = str(valrut).split('-')
        if val == 0:
            return int(x[0])
        elif val == 1:
            return str(x[1])

def get_items(invoice, customer_type, env):
    print "get_items", type(invoice)
    items = []
    monto_neto = 0
    monto_neto_exento = 0
    impuestos = {}
    invoice_items = []
    for item in invoice.get('items'):
        invoice_items.append({
            'id': item.get('id'),
            'spartName': item.get('spartName'),
            'price': item.get('unitPriceNtax'),
            'quantity': item.get('quantity'),
            'discountAmount': item.get('noTaxDiscountAmount'),
            'taxRate': item.get('taxRate'),
            'taxAmount': item.get('taxAmount'),
        })
        tax_rate = item.get('taxRate')
        price_tax = item.get('price')
        if customer_type in ['50', 50]:
            tax_code = ''
            price_no_tax = price_tax
        elif customer_type in ['51', 51]:
            tax_code = 14 if tax_rate > 0 else ''
            price_no_tax = price_tax if tax_rate == 0 else (price_tax /( 1 + tax_rate))


        price_pay = item.get('payUnitPrice')
        price_pay_no_tax = price_pay if tax_rate == 0 else (price_pay / (1 + tax_rate))
        qty = item.get('quantity')
        #discount_per = (price_pay / price_tax)-1
        discount_per = (item.get('discountAmount') * 100 / price_tax)

        discount_amt = item.get('discountAmount')
        monto_item = (qty * price_no_tax) - discount_amt

        # MontoItem
        montoIte_ = str(round((qty * price_no_tax) - discount_amt,6))
        monIte = montoIte_.split('.')
        montoIte__ = int(monIte[0] + spacesjmm(monIte[1],6))

        # PricePay
        pricePay_ = str(round(item.get('payUnitPrice'),6))
        priceP = pricePay_.split('.')
        pricePay__ = int(priceP[0] + spacesjmm(priceP[1],6))

        # Descuento Porcentaje
        if len(str(discount_per)) == 2:
            valor = discount_per + .0
        else:
            valor = discount_per
        descPor_ = str(round(valor,2))
        descP = descPor_.split('.')
        descPor__ = int(descP[0] + spacesjmm(descP[1],2))

        # DiscountAmount
        discountA_ = str(round(item.get('discountAmount'),6))
        discA = discountA_.split('.')
        discountA__ = int(discA[0] + spacesjmm(discA[1],6))


        # CantidadItem
        cantIte_ = str(round(qty,6))
        cantI = cantIte_.split('.')
        cantIte__ = int(cantI[0] + spacesjmm(cantI[1],6))



        print type(item.get('imeis'))
        print isinstance(item.get('imeis'), bool)
        cod = '' if isinstance(item.get('ean'), bool) else item.get('ean')
        desc = '' if isinstance(item.get('imeis'), bool) else item.get('imeis')
        print "desc ", desc
        _item = {item.get('rowNums'): {
            'TipoRegistroP': spaces('D',1),
            'NoLinea': spaces(item.get('rowNums'),4),
            'NoLineaSII': spaces(item.get('rowNums'),4),
            'TipoCodigo': spaces('ean',6),
            'CodigoItem': spaces(cod,35) or spaces('',35),
            'TipoCodigo2': spaces('',6),
            'CodigoItem2': spaces('',35),
            'TipoCodigo3': spaces('',6),
            'CodigoItem3': spaces('',35),
            'TipoCodigo4': spaces('',6),
            'CodigoItem4': spaces('',35),
            'TipoCodigo5': spaces('',6),
            'CodigoItem5': spaces('',35),
            'IndicadorFact': spaces(0,1) if tax_rate > 0 else spaces(1,1),
            'NombreItem': spaces(item.get('spartName'),80),
            'CantidadRef': spaces(0,18),
            'UnidadRef': spaces('',4),
            'PrecioRef': spaces(0,18),
            'CantidadItem': spaces(cantIte__,18),
            'Subcantidad': spaces(0,18),
            'DescSubcantidad': spaces('',35),
            'Subcantidad2': spaces(0,18),
            'DescSubcantidad2': spaces('',35),
            'Subcantidad3': spaces(0,18),
            'DescSubcantidad3': spaces('',35),
            'Subcantidad4': spaces(0,18),
            'DescSubcantidad4': spaces('',35),
            'Subcantidad5': spaces(0,18),
            'DescSubcantidad5': spaces('',35),
            'FechaElaboracion': spaces(0,8),
            'FechaVencimientoP': spaces(0,8),
            'UnidadMedida': spaces(item.get('unit'),4),
            #'PrecioUnitarioItem': spaces(pricePay__,18) if customer_type in ['50', 50] else spaces(price_pay_no_tax,18) if customer_type in ['51',51] else spaces(0,18),
            'PrecioUnitarioItem': spaces(pricePay__,18),
            'PrecioUniOtraMoneda': spaces(0,18),
            'CodigoOtraMoneda': spaces(0,18),
            'FactorConversion': spaces('',10),# 14 enteros, 4 decimales
            'DescuentoPorcentaje': spaces(descPor__,5), #3 enteros, 2 decimales
            'DescuentoMonto': spaces(discountA__,18),
            'TipoSubdescuento': spaces('',1),
            'ValorSubdescuento': spaces('',18),
            'TipoSubdescuento2': spaces('',1),
            'ValorSubdescuento2': spaces('',18),
            'TipoSubdescuento3': spaces('',1),
            'ValorSubdescuento3': spaces('',18),
            'TipoSubdescuento4': spaces('',1),
            'ValorSubdescuento4': spaces('',18),
            'TipoSubdescuento5': spaces('',1),
            'ValorSubdescuento5': spaces('',18),
            'RecargoPorcentage': spaces(0,5),
            'MontoRecargo': spaces(0,18),
            'TipoSubrecargo': spaces('',1),
            'ValorSubrecargo': spaces('',18),
            'TipoSubrecargo2': spaces('',1),
            'ValorSubrecargo2': spaces('',18),
            'TipoSubrecargo3': spaces('',1),
            'ValorSubrecargo3': spaces('',18),
            'TipoSubrecargo4': spaces('',1),
            'ValorSubrecargo4': spaces('',18),
            'TipoSubrecargo5': spaces('',1),
            'ValorSubrecargo5': spaces('',18),
            'CodigoImpuesto': spaces(str(tax_code),6),
            'MontoItem': spaces(montoIte__,18),
            'DescripcionAdicional': spaces(desc,1000),
            'TipoDocLiquida': spaces('',3),
            'ValorDetOtrMoneda': spaces(0,18),
            'RecargoOtrMoneda': spaces(0,18),
            'DescOtraMoneda': spaces(0,18),
            'TipoCodSubcantidad1': spaces('',10),
            'TipoCodSubcantidad2': spaces('',10),
            'TipoCodSubcantidad3': spaces('',10),
            'TipoCodSubcantidad4': spaces('',10),
            'TipoCodSubcantidad5': spaces('',10),
            'TipoCodigoSumple': spaces(0,6),
            'IndAgenteRetenedor': spaces('',1),
            }
        }
        items.append(_item)
        if tax_rate == 0:
            monto_neto_exento = monto_neto_exento + monto_item
            iva = 'exento'
        else:
            iva = 'gravado'
            monto_neto = monto_neto + monto_item
        if not impuestos.get(tax_rate):
            impuestos.update({iva: {
                'base': monto_item,
                'iva': tax_rate,
                'monto': monto_item * tax_rate}
            })
        else:
            base = impuestos.get(tax_rate).get('base') + _item.get('MontoItem')
            impuestos.update({iva: {
                'base': impuestos.get(iva).get('base') + base,
                'iva': tax_rate,
                'monto': impuestos.get(iva).get('monto') + (base * tax_rate)}})
        if not impuestos.get('gravado'):
            impuestos.update(
                {'gravado': {'base':0, 'iva':.19,'monto':0}}
                )
        if not impuestos.get('exento'):
            impuestos.update(
                {'exento': {'base':0, 'iva':0,'monto':0}}
                )
    return items, monto_neto, monto_neto_exento, impuestos, invoice_items


def get_encabezado(invoice, customer_type, folio, env):
    print "get_encabezado", invoice
    ciudad = ''
    comuna = ''
    giro = ''

    #Validaciones Campos
    methP = int(invoice.get('header').get('paymentMethod'))

    #Codigo interno solo para BOLETAS
    codInterno = invoice.get('header').get('userid')

    #Boletas Electronicas
    if customer_type == '50' or customer_type == 50:

        encabezado = {
        'TipoRegistro': spaces('E',1),
        'TipoDocumento': spaces('39',3),
        #'NumeroFolio': spaces(folio,10),
        'NumeroFolio': spaces('',10),
        'FechaEmision': spaces(time_zone_short(invoice.get('header').get('creationDate')),8),
        'IndNoRebaja': spaces('',1),
        'TipoDespacho': spaces('0',1),
        'IndTipoTrasBien': spaces('',1),
        'IndServPeri': spaces('3',1),
        'IndMonBru': spaces('0',1),
        'FormaPago': spaces('',1),
        'FechaCancelacion': spaces('00000000',8),
        'PeriodoDesde': spaces('00000000',8),
        'PeriodoHasta': spaces('00000000',8),
        'MedioPago': spaces('',2),
        'TerminosPagoC': spaces('',4),
        'TerminosPagoD': spaces('',3),
        'FechaVencimiento': spaces(time_zone_short(invoice.get('header').get('creationDate')),8),
        'RutEmisor': spaces('99535120',8),
        'DigVerEmi': spaces('K',1),
        'NumRes': spaces('80',6) if env == 'PROD' else spaces('0',6),
        'NombreEmisor': spaces('Huawei (Chile) S.A.',100),
        'GiroComercialEmisor': spaces('VENTA AL POR MENOR DE COMPUTADORES, EQUIPO PERIFERICO, PROGRAMAS INFORMATICOS',80),
        'CodigoActEconomica':  spaces('474100',5),
        'SucEmiDoc': spaces('',30),
        'CodSucReg': spaces('',9),
        'DireccionOrigen': spaces('Rosario Norte 50',60),
        'ComunaOrigen': spaces('Las Condes',20),
        'CiudadOrigen': spaces('Santiago',15),
        'CodigoVendedor': spaces('',60),
        'RutMandante': spaces('00000000',8),
        'DigVerDemandante': spaces("",1),
        #'RutReceptor': spaces(rutR,8),
        'RutReceptor': spaces('       0',8),
        #'DigVerReceptor': spaces('',1) if customer_type in ['50',50] and not invoice.get('businessInfo').get('taxRegistrationNo') else spaces(invoice.get('businessInfo').get('taxRegistrationNo').split('-')[1],1) if customer_type in ['51',51] else spaces('',1),
        'DigVerReceptor': spaces('0',1),
        #'CodIntReceptor': spaces(invoice.get('header').get('userid'), 20) if customer_type in ['50',50] and not invoice.get('businessInfo').get('taxRegistrationNo') else spaces('',20) if customer_type in ['51',51] else spaces('',20),
        'CodIntReceptor': spaces(codInterno,20),
        'NombreReceptor': spaces(invoice.get('businessInfo').get('name'),100),
        'GiroNegReceptor': spaces('',40),
        'ContReceptor': spaces('',80),
        #'DirReceptor': spaces(invoice.get('shippingAddress').get('address'),60) if customer_type in ['50',50] else spaces(invoice.get('businessInfo').get('registeredAddress'),60) if customer_type in ['51',51] else spaces('',60),
        'DirReceptor': spaces(invoice.get('shippingAddress').get('address'),60),
        #'ComunaReceptor': spaces(invoice.get('shippingAddress').get('city'),20) if customer_type in ['50',50] else spaces(comuna,20) if customer_type in ['51',51] else spaces('',20),
        'ComunaReceptor': spaces(invoice.get('shippingAddress').get('city'),20),
        #'CiudadReceptor': spaces(invoice.get('shippingAddress').get('province'),15) if customer_type in ['50',50] else spaces(ciudad,15) if customer_type in ['51',51] else spaces('',15),
        'CiudadReceptor': spaces(invoice.get('shippingAddress').get('province'),15),
        'DirPosReceptor': spaces('',60),
        'ComunaPosReceptor': spaces('',20),
        'CiudadPosReceptor': spaces('',15),
        #'RutSolicitante': spaces(invoice.get('businessInfo').get('taxRegistrationNo').split('-')[0],8) if customer_type in ['51',51] else spaces('',8),
        'RutSolicitante': spaces('',8),
        'DigSolFactura': spaces('',1),
        'PatenteTransporte': spaces('',8),
        'RutTransportista': spaces('',8),
        'DigTransportista': spaces('',1),
        'DirDestino': spaces('',60),
        'ComunaDestino': spaces('',20),
        'CiudadDestino': spaces('',15),
        'MontoNeto': spaces(0,18),
        'MontoExento': spaces(0,18),
        'MontoInformado': spaces(0,18),
        'TasaIva': spaces(19,5),
        'MontoIva': spaces(0,18),
        'MontoIvaRetenido': spaces('',18),
        'CreditoEspecial': spaces('',18),
        'GarantiaDeposito': spaces('',18),
        'MontoTotal': 0,
        'MontoNoFacturable': spaces('',18),
        'MontoPeriodo': 0,
        'SaldoAnterior': spaces('',18),
        'ValorPagar': spaces('',18),
        'Sistema': spaces('VDESuite',10),
        'Usuario': spaces('Huawei',20),
        'TemplateImpresion': spaces('BOLETA',18),
        'VersionTempImpre': spaces('1',18),
        'Impresora': spaces('',18),
        'ReceptorElectronico': spaces(1,1),
        'FormaAceptacion': spaces('A',1),
        'TipoFoleacion': spaces(1,1),
        'MontoIvaPropio': spaces('',18),
        'MontoIvaTerceros': spaces('',18),
        'CopiasImprimir': spaces('',2),
        'CodigoActEconomica2': spaces('',6),
        'IdenficadorInterno': spaces(invoice.get('trackId'),20),
        'ImprimirCedible': spaces('',1),
    }

    #Factura Electronicas
    elif customer_type == '51'or customer_type == 51:

        print('Entra aqui a INV')
        #Rut Receptor
        valrutR = validRut(invoice.get('businessInfo').get('taxRegistrationNo'))
        rutR = digVer(valrutR,0)
        digVR = digVer(valrutR,1)

        #Rut Solicitante
        valrutS = validRut(invoice.get('businessInfo').get('taxRegistrationNo'))
        rutS = digVer(valrutS,0)
        digVS = digVer(valrutS,1)
        #doc_type = 33
        for extend in invoice.get('businessInfo').get('invoiceExtendList'):
            print "extend ", extend
            if extend.get('paramCode') == 'province':
                ciudad = extend.get('paramValue')
            if extend.get('paramCode') == 'city':
                comuna = extend.get('paramValue')
            if extend.get('paramCode') == 'industry':
                giro = extend.get('paramValue')

        encabezado = {
        'TipoRegistro': spaces('E',1),
        'TipoDocumento': spaces('33',3),
        #'NumeroFolio': spaces(folio,10),
        'NumeroFolio': spaces('',10),
        'FechaEmision': spaces(time_zone_short(invoice.get('header').get('creationDate')),8),
        'IndNoRebaja': spaces('',1),
        'TipoDespacho': spaces('0',1),
        'IndTipoTrasBien': spaces('',1),
        'IndServPeri': spaces('3',1),
        'IndMonBru': spaces('0',1),
        'FormaPago': spaces(invoice.get('header').get('paymentForm'),1),
        'FechaCancelacion': spaces('00000000',8),
        'PeriodoDesde': spaces('00000000',8),
        'PeriodoHasta': spaces('00000000',8),
        'MedioPago': spaces(methodPay(methP),2),
        'TerminosPagoC': spaces('',4),
        'TerminosPagoD': spaces('',3),
        'FechaVencimiento': spaces(time_zone_short(invoice.get('header').get('creationDate')),8),
        'RutEmisor': spaces('99535120',8),
        'DigVerEmi': spaces('K',1),
        'NumRes': spaces('80',6) if env == 'PROD' else spaces('0',6),
        'NombreEmisor': spaces('Huawei (Chile) S.A.',100),
        'GiroComercialEmisor': spaces('VENTA AL POR MENOR DE COMPUTADORES, EQUIPO PERIFERICO, PROGRAMAS INFORMATICOS',80),
        'CodigoActEconomica':  spaces('47410',5),
        'SucEmiDoc': spaces('',30),
        'CodSucReg': spaces('',9),
        'DireccionOrigen': spaces('Rosario Norte 50',60),
        'ComunaOrigen': spaces('Las Condes',20),
        'CiudadOrigen': spaces('Santiago',15),
        'CodigoVendedor': spaces('',60),
        'RutMandante': spaces('00000000',8),
        'DigVerDemandante': spaces("",1),
        'RutReceptor': spaces(rutR,8),
        #'DigVerReceptor': spaces('',1) if customer_type in ['50',50] and not invoice.get('businessInfo').get('taxRegistrationNo') else spaces(invoice.get('businessInfo').get('taxRegistrationNo').split('-')[1],1) if customer_type in ['51',51] else spaces('',1),
        'DigVerReceptor': spaces(digVR,1),
        #'CodIntReceptor': spaces(invoice.get('header').get('userid'), 20) if customer_type in ['50',50] and not invoice.get('businessInfo').get('taxRegistrationNo') else spaces('',20) if customer_type in ['51',51] else spaces('',20),
        'CodIntReceptor': spaces('',20),
        'NombreReceptor': spaces(invoice.get('businessInfo').get('name'),100),
        'GiroNegReceptor': spaces('',40),
        'ContReceptor': spaces('',80),
        #'DirReceptor': spaces(invoice.get('shippingAddress').get('address'),60) if customer_type in ['50',50] else spaces(invoice.get('businessInfo').get('registeredAddress'),60) if customer_type in ['51',51] else spaces('',60),
        'DirReceptor': spaces(invoice.get('shippingAddress').get('address'),60),
        #'ComunaReceptor': spaces(invoice.get('shippingAddress').get('city'),20) if customer_type in ['50',50] else spaces(comuna,20) if customer_type in ['51',51] else spaces('',20),
        'ComunaReceptor': spaces(invoice.get('shippingAddress').get('city'),20),
        #'CiudadReceptor': spaces(invoice.get('shippingAddress').get('province'),15) if customer_type in ['50',50] else spaces(ciudad,15) if customer_type in ['51',51] else spaces('',15),
        'CiudadReceptor': spaces(invoice.get('shippingAddress').get('province'),15),
        'DirPosReceptor': spaces('',60),
        'ComunaPosReceptor': spaces('',20),
        'CiudadPosReceptor': spaces('',15),
        #'RutSolicitante': spaces(invoice.get('businessInfo').get('taxRegistrationNo').split('-')[0],8) if customer_type in ['51',51] else spaces('',8),
        'RutSolicitante': spaces(rutS,8),
        'DigSolFactura': spaces(digVS,1),
        'PatenteTransporte': spaces('',8),
        'RutTransportista': spaces('',8),
        'DigTransportista': spaces('',1),
        'DirDestino': spaces('',60),
        'ComunaDestino': spaces('',20),
        'CiudadDestino': spaces('',15),
        'MontoNeto': spaces(0,18),
        'MontoExento': spaces(0,18),
        'MontoInformado': spaces(0,18),
        'TasaIva': spaces(19,5),
        'MontoIva': spaces(0,18),
        'MontoIvaRetenido': spaces('',18),
        'CreditoEspecial': spaces('',18),
        'GarantiaDeposito': spaces('',18),
        'MontoTotal': 0,
        'MontoNoFacturable': spaces('',18),
        'MontoPeriodo': 0,
        'SaldoAnterior': spaces('',18),
        'ValorPagar': spaces('',18),
        'Sistema': spaces('VDESuite',10),
        'Usuario': spaces('Huawei',20),
        'TemplateImpresion': spaces('BOLETA',18),
        'VersionTempImpre': spaces('1',18),
        'Impresora': spaces('',18),
        'ReceptorElectronico': spaces(1,1),
        'FormaAceptacion': spaces('A',1),
        'TipoFoleacion': spaces(0,1),
        'MontoIvaPropio': spaces('',18),
        'MontoIvaTerceros': spaces('',18),
        'CopiasImprimir': spaces('',2),
        'CodigoActEconomica2': spaces('',6),
        'IdenficadorInterno': spaces(invoice.get('trackId'),20),
        'ImprimirCedible': spaces('',1),
    }

    else:
        doc_type = 0
    '''encabezado = {
        'TipoRegistro': spaces('E',1),
        'TipoDocumento': spaces(doc_type,3),
        'NumeroFolio': spaces(folio,10),
        'FechaEmision': spaces(time_zone_short(invoice.get('header').get('creationDate')),8),
        'IndNoRebaja': spaces('',1),
        'TipoDespacho': spaces('0',1),
        'IndTipoTrasBien': spaces('',1),
        'IndServPeri': spaces('0',1),
        'IndMonBru': spaces('0',1),
        'FormaPago': spaces(invoice.get('header').get('paymentForm'),1),
        'FechaCancelacion': spaces('00000000',8),
        'PeriodoDesde': spaces('00000000',8),
        'PeriodoHasta': spaces('00000000',8),
        'MedioPago': spaces(invoice.get('header').get('paymentMethod'),2),
        'TerminosPagoC': spaces('',4),
        'TerminosPagoD': spaces('',3),
        'FechaVencimiento': spaces(time_zone_short(invoice.get('header').get('creationDate')),8),
        'RutEmisor': spaces('99535120',8),
        'DigVerEmi': spaces('K',1),
        'NumRes': spaces('80',6) if env == 'PROD' else spaces('0',6),
        'NombreEmisor': spaces('Huawei (Chile) S.A.',100),
        'GiroComercialEmisor': spaces('VENTA AL POR MENOR DE COMPUTADORES, EQUIPO PERIFERICO, PROGRAMAS INFORMATICOS Y EQUIPO TELECOM',80),
        'CodigoActEconomica':  spaces('474100',5),
        'SucEmiDoc': spaces('',30),
        'CodSucReg': spaces('',9),
        'DireccionOrigen': spaces('Rosario Norte 50',60),
        'ComunaOrigen': spaces('Las Condes',20),
        'CiudadOrigen': spaces('Santiago',15),
        'CodigoVendedor': spaces('',60),
        'RutMandante': spaces('',8),
        'DigVerDemandante': spaces("",1),
        'RutReceptor': spaces('0',8) if customer_type in ['50',50] and not invoice.get('businessInfo').get('taxRegistrationNo') else spaces(invoice.get('businessInfo').get('taxRegistrationNo'),8) if customer_type in ['51',51] else spaces('',8),
        'DigVerReceptor': spaces('',1) if customer_type in ['50',50] and not invoice.get('businessInfo').get('taxRegistrationNo') else spaces(invoice.get('businessInfo').get('taxRegistrationNo').split('-')[1],1) if customer_type in ['51',51] else spaces('',1),
        'CodIntReceptor': spaces(invoice.get('header').get('userid'), 20) if customer_type in ['50',50] and not invoice.get('businessInfo').get('taxRegistrationNo') else spaces('',20) if customer_type in ['51',51] else spaces('',20),
        'NombreReceptor': spaces(invoice.get('businessInfo').get('name'),100),
        'GiroNegReceptor': spaces(giro,40),
        'ContReceptor': spaces('',80),
        'DirReceptor': spaces(invoice.get('shippingAddress').get('address'),60) if customer_type in ['50',50] else spaces(invoice.get('businessInfo').get('registeredAddress'),60) if customer_type in ['51',51] else spaces('',60),
        'ComunaReceptor': spaces(invoice.get('shippingAddress').get('city'),20) if customer_type in ['50',50] else spaces(comuna,20) if customer_type in ['51',51] else spaces('',20),
        'CiudadReceptor': spaces(invoice.get('shippingAddress').get('province'),15) if customer_type in ['50',50] else spaces(ciudad,15) if customer_type in ['51',51] else spaces('',15),
        'DirPosReceptor': spaces('',60),
        'ComunaPosReceptor': spaces('',20),
        'CiudadPosReceptor': spaces('',15),
        'RutSolicitante': spaces(invoice.get('businessInfo').get('taxRegistrationNo').split('-')[0],8) if customer_type in ['51',51] else spaces('',8),
        'DigSolFactura': spaces(invoice.get('businessInfo').get('taxRegistrationNo').split('-')[1],1) if customer_type in ['51',51] else spaces('',1),
        'PatenteTransporte': spaces('',8),
        'RutTransportista': spaces('',8),
        'DigTransportista': spaces('',1),
        'DirDestino': spaces('',60),
        'ComunaDestino': spaces('',20),
        'CiudadDestino': spaces('',15),
        'MontoNeto': 0,
        'MontoExento': 0,
        'MontoInformado': 0,
        'TasaIva': 0,
        'MontoIva': 0,
        'MontoIvaRetenido': spaces('',18),
        'CreditoEspecial': spaces('',18),
        'GarantiaDeposito': spaces('',18),
        'MontoTotal': 0,
        'MontoNoFacturable': spaces('',18),
        'MontoPeriodo': 0,
        'SaldoAnterior': spaces('',18),
        'ValorPagar': spaces('',18),
        'Sistema': spaces('VDESuite',10),
        'Usuario': spaces('Huawei',20),
        'TemplateImpresion': spaces('BOLETA',18),
        'VersionTempImpre': spaces('1',18),
        'Impresora': spaces('',18),
        'ReceptorElectronico': spaces('',1),
        'FormaAceptacion': spaces('A',1),
        'TipoFoleacion': spaces('0',1),
        'MontoIvaPropio': spaces('',18),
        'MontoIvaTerceros': spaces('',18),
        'CopiasImprimir': spaces('',2),
        'CodigoActEconomica2': spaces('',6),
        'IdenficadorInterno': spaces(invoice.get('trackId'),20),
        'ImprimirCedible': spaces('',1),
    }'''
    totales = {
        'TipoRegistroT': spaces('T',1),
        'FolioPrimerR': spaces('',10),
        'FolioUltimoR': spaces('',10),
        'CantidadDoctos': spacesjmm(1,10),
        'RutFirmaDoc': spaces('',8),
        'DVFirmaDoc': spaces('',1),
        'AreaFirmaDoc': spaces('',30),
    }
    return encabezado, totales


def validate_json(nodo, val):
    if nodo == 'type':
        if (val==50) or (val == "50"):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1000', 'info': ' "type" node error - the value sent "'+ str(val)+'": Values 50 or 51 only allowed'}
    if nodo == 'id':
        if (val != '' or val is not False):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1001', 'info': ' "id" node error - the value sent "'+ str(val)+'": cannot be empty'}
    if nodo == 'idType':
        if(val == '1' or val == '2' or val == 1 or val == 2):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1002',
            'info': ' "idType" node error - the value sent "'+ str(val)+'": Values 1 or 2 only allowed'}
    if nodo == 'postalCode':
        if(len(val) <= 6):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1003',
            'info': ' "postalCode" node error - the value sent "'+ str(val)+'": only length 6 digits'}
    if nodo == 'cityName':
        #if val.encode('utf-8') not in vdeCity:
        if val not in vdeCity:
            return_ = False
            return_desc = {'Code':'1004',
            'info': ' "cityName" node error - the value sent "'+ val.encode('utf-8').strip()+'": is not included into the DIAN list'}
        else:
            return_ = True
            return_desc = True
    if nodo == 'department':
        #if val.encode('utf-8') not in vdeDepartment:
        if val not in vdeDepartment:
            return_ = False
            return_desc = {'Code':'1005',
            'info': ' "department" node error - the value sent "'+ val.encode('utf-8').strip()+'": is not included into the DIAN list'}
        else:
            return_ = True
            return_desc = True
    if nodo == 'countryCode':
        if(val in vdeCountryCode):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1006',
            'info': ' "countryCode" node error - the value sent "'+ val.encode('utf-8').strip()+'": is not included into the DIAN list'}
    if nodo == 'referencedInvoiceNumber':
        if (val != '' or val is not False):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1007',
            'info': ' "referencedInvoiceNumber" node error - the value sent "'+ str(val)+'": cannot be empty'}
    return return_, return_desc


def putInvoiceResult(_trackId, _orderNo, _invoiceNo, _bytePDF, _errorMessage, _invoiceFile, _invoiceHeader, _invoiceItems, _env):
    guid_response = requests.get(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/guid")
    _guid = guid_response.json().get('guid')
    #print "guid ", _guid
    data = {
        'trackId': _trackId if _trackId is not False else None,
        'orderNo': _orderNo if _orderNo is not False else None,
        'invoiceNo': _invoiceNo if _invoiceNo is not False else None,
        'bytePDF': _bytePDF if _bytePDF is not False else None,
        'errorMessage': _errorMessage if _errorMessage is not False else None,
        'invoiceFile': _invoiceFile if _invoiceFile is not False else None,
        'invoiceHeader': [_invoiceHeader] if _invoiceHeader is not False else None,
        'invoiceItems': _invoiceItems if _invoiceItems is not False else None,
        'guid': _guid,
    }
    #print "data ", data
    data = json_to_utf8(data)
    #print "data ", data
    data = json.dumps(data)
    #print "data ", data
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    params_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getParamsCallback",headers=headers, data=data)
    params = params_response.json()
    #print "params ", params
    hash_ = params.get('hash')
    data={
        "country": "CL",
        "environment": _env,
        "guid": _guid}
    #print "data ", data
    token_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getToken", data=data)
    #print "token_response ", token_response.json()
    urlHW_I = json_to_utf8(token_response.json().get('urlCallback'))
    appKey = json_to_utf8(token_response.json().get('appKey'))
    token = json_to_utf8(token_response.json().get('token'))
    data = {
        'companyCode': 'CLHW_VDE',
        'params': hash_,
        'token': token
    }
    #print "data ", data
    data = json.dumps(data)
    #print "data ", data
    headers = {
        'X-HW-ID': 'com.huawei.invoice_cloud',
        'X-HW-APPKEY': appKey,
        'Content-Type': 'application/json'
    }
    hw_response = requests.post(urlHW_I, headers=headers, data=data)
    #print "hw_response ", hw_response.text
    return hw_response.json(), params.get('json')



def putInvoiceResultLog(_trackId, _orderNo, _invoiceNo, _bytePDF, _errorMessage, _invoiceFile, _invoiceHeader, _invoiceItems, _env):
    guid_response = requests.get(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/guid")
    _guid = guid_response.json().get('guid')
    #print "guid ", _guid
    data = {
        'trackId': _trackId if _trackId is not False else None,
        'orderNo': _orderNo if _orderNo is not False else None,
        'invoiceNo': _invoiceNo if _invoiceNo is not False else None,
        'bytePDF': _bytePDF if _bytePDF is not False else None,
        'errorMessage': _errorMessage if _errorMessage is not False else None,
        'invoiceFile': _invoiceFile if _invoiceFile is not False else None,
        'invoiceHeader': [_invoiceHeader] if _invoiceHeader is not False else None,
#        'invoiceItems': [_invoiceItems] if _invoiceItems is not False else None,
        'invoiceItems': _invoiceItems if isinstance(_invoiceItems,list) else [_invoiceItems] if _invoiceItems is not False else none,
        'guid': _guid,
    }
    #print "data ", data
    data = json_to_utf8(data)
    #print "data ", data
    data = json.dumps(data)
    #print "data ", data
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    params_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getParamsCallback",headers=headers, data=data)
    params = params_response.json()
    #print "params ", params
    hash_ = params.get('hash')
    data={
        "country": "CL",
        "environment": _env,
        "guid": _guid}
    #print "data ", data
    token_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getToken", data=data)
    #print "token_response ", token_response.json()
    urlHW_I = json_to_utf8(token_response.json().get('urlCallback'))
    appKey = json_to_utf8(token_response.json().get('appKey'))
    token = json_to_utf8(token_response.json().get('token'))
    data = {
        'companyCode': 'CLHW_VDE',
        'params': hash_,
        'token': token
    }
    #print "data ", data
    data = json.dumps(data)
    #print "data ", data
    headers = {
        'X-HW-ID': 'com.huawei.invoice_cloud',
        'X-HW-APPKEY': appKey,
        'Content-Type': 'application/json'
    }
    hw_response = requests.post(urlHW_I, headers=headers, data=data)
    #print "hw_response ", hw_response.text
    return hw_response.json(), params.get('json')
