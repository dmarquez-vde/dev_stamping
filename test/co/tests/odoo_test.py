import threading
import datetime
import time

def invoices():
    print "inv"
    time.sleep(3)

def thread_invoice():
    #logging.info("Thread HILO inicializado " + str(datetime.datetime.now()))
    while True:
        t1 = threading.Thread(name=datetime.datetime.now(), target=invoices)
        t1.start()
        t1.join()

def nc():
    print "NC"
    time.sleep(5)

def thread_nc():
    ##logging.info("Thread HILO NC inicializado " + str(datetime.datetime.now()))
    while True:
        t1 = threading.Thread(name=datetime.datetime.now(), target=nc)
        t1.start()
        t1.join()

def callback():
    print "callback"
    time.sleep(10)

def thread_callback():
    #logging.info("Thread HILO CALLBACK inicializado " + str(datetime.datetime.now()))
    while True:
        t1 = threading.Thread(name=datetime.datetime.now(), target=callback)
        t1.start()
        t1.join()


if __name__ == '__main__':
    t1 = threading.Thread(name="hilo invoice", target=thread_invoice)
    t1.start()
    t2 = threading.Thread(name="hilo nc", target=thread_nc)
    t2.start()
    t3 = threading.Thread(name="hilo callback", target=thread_callback)
    t3.start()