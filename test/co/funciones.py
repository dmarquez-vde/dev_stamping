# -*- coding: utf-8 -*-
from dateutil import parser
import pytz
#from validations import *
import ast
import json
import logging
from logging.handlers import RotatingFileHandler

from catalogs import vdeCity, vdeDepartment, vdeCountryCode
import locale
import requests
from requests.auth import HTTPBasicAuth
import json
#from structure import *


def get_logger(logger_name, archivo, create_file=False):
    # create logger for prd_ci
    log = logging.getLogger(logger_name)
    log.setLevel(level=logging.INFO)
    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s %(levelname)s ? %(message)s')
    if create_file:
        # create file handler for logger.
        #fh = logging.FileHandler("log/inv.log")
        fh = RotatingFileHandler(archivo, mode='a', maxBytes=1000000, encoding=None, delay=0)
        fh.setLevel(level=logging.DEBUG)
        fh.setFormatter(formatter)
    # reate console handler for logger.
    ch = logging.StreamHandler()
    ch.setLevel(level=logging.DEBUG)
    ch.setFormatter(formatter)
    # add handlers to logger.
    if create_file:
        log.addHandler(fh)
    log.addHandler(ch)
    return  log


def json_to_utf8(json_):
    #print json_
    inv = json_
    #for inv in json_:
    if isinstance(inv, dict):
        for key in inv.keys():
            #print "key ", key
            if isinstance(inv.get(key), dict):
                keys = inv.get(key).keys()
                for key_ in keys:
                    #print "key_ ", key_
                    val = inv.get(key).get(key_)
                    if  not (isinstance(val, float) or isinstance(val, int)):
                        if val == None:
                            inv.get(key).update({key_: False})
                        else:
                            inv.get(key).update({key_: unicode(val).encode('utf-8')})
            elif isinstance(inv.get(key), list):
                inv.get(key)
                subs = []
                for sub in inv.get(key):
                    #print "sub ",sub
                    recurs = json_to_utf8(sub)
                    #print "recurs ", recurs
                    subs.append(recurs)
                inv.update({key:subs})
            else:
                val = inv.get(key)
                if  not (isinstance(val, float) or isinstance(val, int)):
                    if val == None:
                        inv.update({key: False})
                    else:
                        #print "val ", val
                        #print type(val)
                        inv.update({key: val})
        #print "limpio ", json_
        return inv
    else:
        if json_ is None:
            #print "es None"
            return False
        #print "no es dict ", json_
        if  not (isinstance(json_, float) or isinstance(json_, int)):
            #print "no es num "
            if json_ == None:
                return False
            else:
                return unicode(json_).encode('utf-8')


def json_clean(json_):
    return ast.literal_eval(json.dumps(json_))

def limpia_string(cadena):
    cadena = cadena.replace('\u00c0','À').replace('\u00c1','Á').replace('\u00c2','Â').replace('\u00c3','Ã').replace('\u00c4','Ä').replace('\u00c5','Å').replace('\u00c6','Æ').replace('\u00c7','Ç').replace('\u00c8','È').replace('\u00c9','É').replace('\u00ca','Ê').replace('\u00cb','Ë').replace('\u00cc','Ì').replace('\u00cd','Í').replace('\u00ce','Î').replace('\u00cf','Ï').replace('\u00d1','Ñ').replace('\u00d2','Ò').replace('\u00d3','Ó').replace('\u00d4','Ô').replace('\u00d5','Õ').replace('\u00d6','Ö').replace('\u00d8','Ø').replace('\u00d9','Ù').replace('\u00da','Ú').replace('\u00db','Û').replace('\u00dc','Ü').replace('\u00dd','Ý').replace('\u00df','ß').replace('\u00e0','à').replace('\u00e1','á').replace('\u00e2','â').replace('\u00e3','ã').replace('\u00e4','ä').replace('\u00e5','å').replace('\u00e6','æ').replace('\u00e7','ç').replace('\u00e8','è').replace('\u00e9','é').replace('\u00ea','ê').replace('\u00eb','ë').replace('\u00ec','ì').replace('\u00ed','í').replace('\u00ee','î').replace('\u00ef','ï').replace('\u00f0','ð').replace('\u00f1','ñ').replace('\u00f2','ò').replace('\u00f3','ó').replace('\u00f4','ô').replace('\u00f5','õ').replace('\u00f6','ö').replace('\u00f8','ø').replace('\u00f9','ù').replace('\u00fa','ú').replace('\u00fb','û').replace('\u00fc','ü').replace('\u00fd','ý').replace('\u00ff','ÿ')
    return cadena

def validaID(val, types):
    if types == 'PASAPORTE':
        resu = '0'
    else:
        resu = val
    return resu


def validaEX(val, types):
    if types == 'PASAPORTE':
        resu = val
    else:
        resu = ''
    return resu


def reason(valor):
    reasons = ''
    if valor == '13' or valor == 13:
        reasons = 'DEVOLUCION'
    if valor == '20' or valor == 20:
        reasons = 'ANULACION'
    return reasons


def types(valor):
    if valor == '1':
        idType = "CEDULA_DE_CIUDADANIA"
    if valor == '2':
        idType = "PASAPORTE"
    return idType


def get_customer(listQueryInvoice):
    #_id = listQueryInvoice.get('businessInfo').get('taxRegistrationNo')
    _id = ''
    _name = listQueryInvoice.get('businessInfo').get('name')
    _idType = types(listQueryInvoice.get('businessInfo').get('idType'))
    #_idType = types('2')
    _email = listQueryInvoice.get('businessInfo').get('registeredEmail')
    _telephone = listQueryInvoice.get('businessInfo').get('registeredTelephone')
    _address = listQueryInvoice.get('billingAddress').get('address')
    _department = listQueryInvoice.get('billingAddress').get('province')
    _postalCode = listQueryInvoice.get('billingAddress').get('zipcode')
    _cityName = listQueryInvoice.get('billingAddress').get('city')
    _countryCode = listQueryInvoice.get('billingAddress').get('country')
    _firstName = listQueryInvoice.get('billingAddress').get('vatFirstName')
    _lastName = listQueryInvoice.get('billingAddress').get('vatLastName')

    if _id == "":
        _type = 'PERSONA_NATURAL'
        _id = validaID(listQueryInvoice.get('businessInfo').get('idCardNumber'),_idType)
        _idType = _idType
        _idExport	= validaEX(listQueryInvoice.get('businessInfo').get('idCardNumber'),_idType)
        _department = _department
        _cityName = _cityName
        _postalCode = _postalCode
        _address = _address
        _countryCode = _countryCode
        _email = _email
        _telephone = _telephone
        _firstName = _name
        _lastName = _lastName
    else:
        _type = 'PERSONA_JURIDICA'
        _id = _id
        _idType = _idType
        _idExport	= ''
        _department = _department
        _cityName = _cityName
        _postalCode = _postalCode
        _address = _address
        _countryCode = _countryCode
        _email = _email
        _telephone = _telephone
        _firstName = _firstName
        _lastName = _lastName
    customer = {
        'type': _type,
        'id': _id,
        'idType': _idType,
        'idExport': _idExport,
        'name': _name,
        'department': _department,
        'cityName': _cityName,
        'postalCode': _postalCode,
        'address': _address,
        'countryCode': _countryCode,
        'email': _email,
        'telephone': _telephone,
        'firstName': _firstName,
        'lastName': _lastName
    }
    return customer


def gift(valor, percent):
    if valor == '' or valor == 0.0 or valor == 0.00:
        if percent == 100.0 or percent == 100.00:
            _isGift = True
        else:
            _isGift = False
    else:
        _isGift = False
    return _isGift

def get_items(_items):
    base_exenta = 0
    base_iva = 0
    importe_iva = 0
    importe_exento = 0
    hasGift = False
    invoice_items = []
    items = []
    giftAmount = 0
    _impuesto1 = 0
    dmz_taxes = 0
    for item in _items:
        #print "item ", item
        invoice_items.append({
            'id': item.get('id'),
            'spartName': item.get('spartName'),
            #decimals function
            'price': decimals(item.get('unitPriceNtax')),
            'quantity': item.get('quantity'),
            'discountAmount': decimals(item.get('noTaxDiscountAmount')),
            'taxRate': decimals(item.get('taxRate')),
            'taxAmount': decimals(item.get('taxAmount')),
        })
        _itemCode = item.get('skuCode')
        _payUnitPrice = item.get('payUnitPrice')
        _quantity = item.get('quantity')
        _price = item.get('price')
        _name = item.get('spartName')
        _description = item.get('spartName')
        _impuesto = item.get('taxRate')
        #decimals function
        _amount = decimals(_price/(1+_impuesto))
        _discountAmount = decimals((_price - _payUnitPrice)*_quantity)
        #dmz_taxes = 0
        if item.get('taxRate') == 0:
            if _payUnitPrice == 0:
                _taxableAmount = 0
            else:
                #decimals function
                _taxableAmount = decimals(_quantity*_amount)-decimals(_discountAmount/(1+_impuesto))
            if _taxableAmount == 0:
                _discountPercent = 100
            else:
                #decimals function
                _discountPercent = decimals(((abs(_taxableAmount)/decimals(abs(_quantity)*abs(_amount)))-1)*(-100))
                if _discountPercent == -0:
                    _discountPercent = 0
            items.append({
                'itemType': 'EXENTO',
                'itemCode': _itemCode,
                'quantity': _quantity,
                'amount': _amount,
                'name': _name,
                'description': _description,
                'isGift': gift(_payUnitPrice, _discountPercent),
                'taxableAmount': _taxableAmount,
                #decimals function
                'totalTax': decimals(_taxableAmount * (_impuesto)),
                #decimals function
                'total': decimals(_taxableAmount * (1 + _impuesto)),
                'clasificationId': '',
                'clasificationName': '',
                'observation': '',
                'discountPercentage': _discountPercent,
                'discountAmount': _discountAmount,
            })
            base_exenta = base_exenta + _taxableAmount
            importe_exento = 0
            #decimals function
            giftAmount = giftAmount + decimals(_taxableAmount) + decimals(_taxableAmount * _impuesto)
        else:
            _impuesto1 = _impuesto
            if _payUnitPrice > 0:
                if _payUnitPrice == 0:
                    _taxableAmount = 0
                else:
                    #decimals function
                    _taxableAmount = decimals((_quantity * _amount)-decimals((_discountAmount)/(1 + _impuesto)))
                if _taxableAmount == 0:
                    _discountPercent = 100
                else:
                    #decimals function
                    _discountPercent =  decimals(((abs(_taxableAmount)/decimals(abs(_quantity)*abs(_amount)))-1)*(-100))
                    if _discountPercent == -0:
                        _discountPercent = 0
                items.append({
                    'itemType': 'GRAVADO',
                    'itemCode': _itemCode,
                    'quantity': _quantity,
                    'amount': _amount,
                    'name': _name,
                    'description': _description,
                    'isGift': gift(_payUnitPrice, _discountPercent),
                    'taxableAmount': _taxableAmount,
                    #decimals function
                    'totalTax': decimals(_taxableAmount * (_impuesto)),
                    #decimals function
#                    'total': decimals(_taxableAmount * (1 + _impuesto)),
                    'total': decimals(_taxableAmount * (_impuesto)) + _taxableAmount,
                    'clasificationId': '',
                    'clasificationName': '',
                    'observation': '',
                    'discountPercentage': _discountPercent,
                    #decimals function
                    'discountAmount': decimals((_discountAmount) / (1 + _impuesto)),
                    'taxes': [{
                                #{
                                    'type': 'IVA',
                                    #decimals function
                                    'amount': decimals(_taxableAmount * _impuesto),
                                    #decimals function
                                    'taxableAmount': decimals(_taxableAmount),
                                    #decimals function
                                    'percentage': decimals(_impuesto * 100)
                                #}
                            }],
                })
                dmz_taxes = dmz_taxes + decimals(_taxableAmount * _impuesto)
                base_iva = base_iva + _taxableAmount
                #decimals function
                importe_iva = importe_iva + decimals(_taxableAmount * _impuesto)
                #decimals function
                giftAmount = giftAmount + decimals(_taxableAmount) + decimals(_taxableAmount * _impuesto)
            else:
                hasGift = True
                items.append({
                    'itemType': 'GRAVADO',
                    'itemCode': _itemCode,
                    'quantity': _quantity,
                    'amount': _amount,
                    'name'	: _name,
                    'description': _description,
                    'isGift': True,
                    #decimals function
                    'taxableAmount': decimals(_quantity * _amount),
                    #decimals function
                    'totalTax': decimals(_quantity * _amount * (_impuesto)),
                    #decimals function
                    'total': decimals(_quantity * _amount * (_impuesto)),
                    #'total': decimals(_taxableAmount * (_impuesto)) + _taxableAmount,
                    'clasificationId': '',
                    'clasificationName': '',
                    'observation': '',
                    'discountPercentage': 0,
                    'discountAmount': 0,
                    'taxes': [{
                                #{
                                    'type': 'IVA',
                                    #decimals function
                                    'amount': decimals(_quantity * _amount * (_impuesto)),
                                    #decimals function
                                    'taxableAmount': decimals(_quantity * _amount),
                                    #decimals function
                                    'percentage': decimals(_impuesto * 100)
                                #}
                            }],
                })
                #decimals function
                dmz_taxes = dmz_taxes + decimals(_quantity * _amount * (_impuesto))
                base_iva = base_iva + decimals(_quantity * _amount)
                importe_iva = importe_iva + decimals(_quantity * _amount * (_impuesto))
                giftAmount = giftAmount + decimals(_quantity * _amount * (_impuesto))
    returning = {
            "exento": {"base": base_exenta, "impuesto": importe_exento},
            "iva": {"base": base_iva,
                    #decimals function
                    #"impuesto": decimals(base_iva * _impuesto1),
                    'impuesto': dmz_taxes,
                    "gift": hasGift, "with_gift_amount": giftAmount},
    }
    return items, returning, invoice_items


def time_zone(date):
    get_date_obj = parser.parse(date)
    #print(get_date_obj.strftime("%Y-%m-%d %H:%M:%S%z"))
    bogota = pytz.timezone('America/Bogota')
    observationTime = get_date_obj.astimezone(bogota)
    #print(observationTime.strftime("%Y-%m-%d %H:%M:%S%z"))
    #print(observationTime.strftime("%Y-%m-%d %H:%M:%S"))
    return observationTime.strftime("%Y-%m-%d %H:%M:%S")


def payform(valor):
    if valor == '1':
        res = "CONTADO"
    return res


def paymethod(valor):
    if valor == '47':
        res = "TRANSFERENCIA"
    return res


#########################################


def validate_json(nodo, val):
    if nodo == 'id':
        if (val != '' or val is not False):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1001', 'info': ' "id" node error - the value sent "'+ str(val)+'": cannot be empty'}
    if nodo == 'idType':
        if(val == '1' or val == '2' or val == 1 or val == 2):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1002',
            'info': ' "idType" node error - the value sent "'+ str(val)+'": Values 1 or 2 only allowed'}
    if nodo == 'postalCode':
        if(len(val) <= 6):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1003',
            'info': ' "postalCode" node error - the value sent "'+ str(val)+'": only length 6 digits'}
    if nodo == 'cityName':
        if val.encode('utf-8') not in vdeCity:
        #if val not in vdeCity:
            return_ = False
            return_desc = {'Code':'1004',
            'info': ' "cityName" node error - the value sent "'+ val.encode('utf-8').strip()+'": is not included into the DIAN list'}
        else:
            return_ = True
            return_desc = True
    if nodo == 'department':
        #if val.encode('utf-8') not in vdeDepartment:
        if val not in vdeDepartment:
            return_ = False
            return_desc = {'Code':'1005',
            'info': ' "department" node error - the value sent "'+ val.encode('utf-8').strip()+'": is not included into the DIAN list'}
        else:
            return_ = True
            return_desc = True
    if nodo == 'countryCode':
        if(val in vdeCountryCode):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1006',
            'info': ' "countryCode" node error - the value sent "'+ val.encode('utf-8').strip()+'": is not included into the DIAN list'}
    if nodo == 'referencedInvoiceNumber':
        if (val != '' or val is not False):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1007',
            'info': ' "referencedInvoiceNumber" node error - the value sent "'+ str(val)+'": cannot be empty'}
    return return_, return_desc


def number_format(num, places=0):
    return locale.format_string("%.*f", (places, num), True)


def decimals(numero):
    if numero == 0:
        return round(numero,2)
    if isinstance(numero,int):
        return round(numero,2)
    #print "numero ", numero
    #print type(numero)
    numero = str(numero)
    partes = numero.split(".")
    #print partes
    decimales = partes[1]
    if len(decimales)>3:
        if decimales[2] == '5':
            if int(decimales[3]) % 2 == 0:
                decimaless = int(decimales[:2])
            else:
                decimaless = int(decimales[:2]) + 1
            #print decimaless
            resultado = partes[0] + "." + str(decimaless)
        else:
            resultado = round(float(numero),2)
    else:
        resultado = round(float(numero),2)
    return float(resultado)

def decimals_old(numero):
    numero = float(numero)
    res = number_format(numero, 4)
    ultimos = res.split(".")
    valorStr = str(ultimos[1])
    valor = valorStr[1]
    valor3 = valorStr[2]
    ultValores = valorStr[0] + valorStr[1]
    #print(valor)
    if int(valor) < 5:
        resultado = number_format(numero, 2)
        #print(resultado)
    elif int(valor) > 5 and int(valor) <= 9:
        resultado = number_format(numero, 2)
        #print(resultado)
    elif int(valor) == 5:
        if int(valor3) % 2 == 0:
            resultado = ultimos[0] + '.' + ultValores
            #print(resultado)
        else:
            resultado = number_format(numero, 2)
            #print(resultado)
    return float(resultado)

def putInvoiceResult(_trackId, _orderNo, _invoiceNo, _bytePDF, _errorMessage, _invoiceFile, _invoiceHeader, _invoiceItems, _env):
    guid_response = requests.get(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/guid")
    _guid = guid_response.json().get('guid')
    #print "guid ", _guid
    data = {
        'trackId': _trackId if _trackId is not False else None,
        'orderNo': _orderNo if _orderNo is not False else None,
        'invoiceNo': _invoiceNo if _invoiceNo is not False else None,
        'bytePDF': _bytePDF if _bytePDF is not False else None,
        'errorMessage': _errorMessage if _errorMessage is not False else None,
        'invoiceFile': _invoiceFile if _invoiceFile is not False else None,
        'invoiceHeader': [_invoiceHeader] if _invoiceHeader is not False else None,
        'invoiceItems': _invoiceItems if _invoiceItems is not False else None,
        'guid': _guid,
    }
    #print "data ", data
    data = json_to_utf8(data)
    #print "data ", data
    data = json.dumps(data)
    #print "data ", data
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    params_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getParamsCallback",headers=headers, data=data)
    params = params_response.json()
    #print "params ", params
    hash_ = params.get('hash')
    data={
        "country": "CO",
        "environment": _env,
        "guid": _guid}
    #print "data ", data
    token_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getToken", data=data)
    #print "token_response ", token_response.json()
    urlHW_I = json_to_utf8(token_response.json().get('urlCallback'))
    appKey = json_to_utf8(token_response.json().get('appKey'))
    token = json_to_utf8(token_response.json().get('token'))
    data = {
        'companyCode': 'COHW_VDE',
        'params': hash_,
        'token': token
    }
    #print "data ", data
    data = json.dumps(data)
    #print "data ", data
    headers = {
        'X-HW-ID': 'com.huawei.invoice_cloud',
        'X-HW-APPKEY': appKey,
        'Content-Type': 'application/json'
    }
    hw_response = requests.post(urlHW_I, headers=headers, data=data)
    #print "hw_response ", hw_response.text
    return hw_response.json(), params.get('json')



def putInvoiceResultLog(_trackId, _orderNo, _invoiceNo, _bytePDF, _errorMessage, _invoiceFile, _invoiceHeader, _invoiceItems, _env):
    guid_response = requests.get(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/guid")
    _guid = guid_response.json().get('guid')
    #print "guid ", _guid
    data = {
        'trackId': _trackId if _trackId is not False else None,
        'orderNo': _orderNo if _orderNo is not False else None,
        'invoiceNo': _invoiceNo if _invoiceNo is not False else None,
        'bytePDF': _bytePDF if _bytePDF is not False else None,
        'errorMessage': _errorMessage if _errorMessage is not False else None,
        'invoiceFile': _invoiceFile if _invoiceFile is not False else None,
        'invoiceHeader': [_invoiceHeader] if _invoiceHeader is not False else None,
#        'invoiceItems': [_invoiceItems] if _invoiceItems is not False else None,
        'invoiceItems': _invoiceItems if isinstance(_invoiceItems,list) else [_invoiceItems] if _invoiceItems is not False else none,
        'guid': _guid,
    }
    #print "data ", data
    data = json_to_utf8(data)
    #print "data ", data
    data = json.dumps(data)
    #print "data ", data
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    params_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getParamsCallback",headers=headers, data=data)
    params = params_response.json()
    #print "params ", params
    hash_ = params.get('hash')
    data={
        "country": "CO",
        "environment": _env,
        "guid": _guid}
    #print "data ", data
    token_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getToken", data=data)
    #print "token_response ", token_response.json()
    urlHW_I = json_to_utf8(token_response.json().get('urlCallback'))
    appKey = json_to_utf8(token_response.json().get('appKey'))
    token = json_to_utf8(token_response.json().get('token'))
    data = {
        'companyCode': 'COHW_VDE',
        'params': hash_,
        'token': token
    }
    #print "data ", data
    data = json.dumps(data)
    #print "data ", data
    headers = {
        'X-HW-ID': 'com.huawei.invoice_cloud',
        'X-HW-APPKEY': appKey,
        'Content-Type': 'application/json'
    }
    hw_response = requests.post(urlHW_I, headers=headers, data=data)
    #print "hw_response ", hw_response.text
    return hw_response.json(), params.get('json')
