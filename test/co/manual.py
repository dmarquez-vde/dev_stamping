# -*- coding: utf-8 -*-
from odooclient import client
import requests
from requests.auth import HTTPBasicAuth
import datetime
import json, ast
from funciones import *
import base64
import fcntl


id = 779
odooClient = client.OdooClient(host="192.168.200.3", port="8069", dbname="hwcopy", saas=False, debug=True)
_env = env = 'TEST'
donde = 'error'
print "odoo ", odooClient
odooClient.ServerInfo()
if odooClient.Authenticate("admin", "controlVDE"):
    log = get_logger("inv", "log/inv.log", True)
    log.info("Iniciando ProcesaDoctosInv")
    _invoiceType = 'FACTURA'
    _operationCode = '10'
    log.info("ENV: "+str(_env))
    if _env == 'TEST' or _env == 'test' or _env== 'Test':
        _pointOfSale = 'SETT'
    elif _env == 'PROD' or _env == 'prod' or _env== 'Prod':
        _pointOfSale = 'PROD'
    criteria = [('id','=',id)]
    try:
        invoices = odooClient.SearchRead('hw.cfdi',criteria,['hw_json','folio'])
    except:
        print ("error")
#    print invoice
    for inv in invoices:
        hw_json = inv.get('hw_json')
        print type(hw_json)
        #hw_json.encode('latin1')
        #hw_json.encode('utf-8')
        listQueryInvoice = json.loads(hw_json, strict=False)
        print listQueryInvoice
        if len(hw_json)>0:
            for invoice in [listQueryInvoice]:
                print "invoice ", invoice
                _originalTrackId = invoice.get('originalTrackId')
                _originalInvoiceNo = invoice.get('originalInvoiceNo')
                _trackId = str(invoice.get('trackId'))
                _orderNo = _orderNo = invoice.get('header').get('orderNo')
                _orderNo = str(_orderNo)
                _creationDate = invoice.get('header').get('creationDate')
                _send_mail_account = invoice.get('businessInfo').get('registeredEmail')
                if invoice.get('billingAddress').get('vatFirstName'):
                    name = invoice.get('billingAddress').get('vatFirstName')
                else:
                    name = ''
                if invoice.get('billingAddress').get('vatLastName'):
                    last_name = invoice.get('billingAddress').get('vatLastName')
                else:
                    last_name = ''
                _send_mail_name =  name + " " + last_name
                _hash_cfdi = _trackId + "  "+_orderNo+"  "+_creationDate
                log.info("######### " + _hash_cfdi + " ########")
                #######search.read ir.sequence
                criteria = [('code','=','hw.cfdi,invoice')]
                try:
                    odooCfdiSeq = odooClient.SearchRead('ir.sequence',criteria,['number_next_actual','prefix','id'])
                except:
                    log.error("ERROR EN METODO SEARCH_READ ir.sequence ")
                #print "odooCfdiSeq ", odooCfdiSeq
                folio_actual = odooCfdiSeq[0]['number_next_actual']
                serie_actual = odooCfdiSeq[0]['prefix']
                log.info("folio actual" + str(folio_actual))
                #######search.read hw.cfdi
                criteria = [('hw_no_invoice','=',_orderNo),('hw_track_id','=',_trackId),('state','in',['success','sent','fail'])]
                log.info("criteria " + str(criteria))
                try:
                    odooCfdi = odooClient.SearchRead('hw.cfdi',criteria,['uuid'])
                except:
                    log.error("ERROR EN METODO SEARCH_READ hw.cfdi ")
                #print "odooCFdi ", odooCfdi
                if len(odooCfdi)==0:
                    print "len == 0"
                    log.info("PROCESAR FACTURAS")
                    ######create hw.cfdi
                    datas = {
                        'name': _hash_cfdi,
                        'hw_no_invoice': _orderNo,
                        'hw_track_id': _trackId,
                        'state': 'pending',
                        'date_entry': (datetime.datetime.now() + datetime.timedelta(hours=5)).strftime("%Y-%m-%d %H:%M:%S"),
                        'date_push': invoice.get('header').get('creationDate'),
                        'hw_json': json.dumps(invoice).decode('unicode-escape').encode('utf8'),
                        #'execution_id': odooExecutionId
                    }
                    #print "datas ", datas
                    try:
                        odooCFDIId = odooClient.Create('hw.cfdi',datas)
                    except Exception as e:
                        log.error("ERROR EN METODO CREATE hw.cfdi ")
                    #print "odooCFDIId ", odooCFDIId
                    print "VALIDACIONES"
                    list_error = {}
                    list_error_desc = {}
                    if invoice.get('businessInfo').get('taxRegistrationNo')=='':
                        customer_id = validate_json('id', invoice.get('businessInfo').get('idCardNumber'))
                    else:
                        customer_id = validate_json('id', invoice.get('businessInfo').get('taxRegistrationNo'))
                    #print "customer_id ", customer_id
                    list_error['id'], list_error_desc['id'] = customer_id

                    customer_idType = validate_json('idType',invoice.get('businessInfo').get('idType'))
                    list_error['idType'], list_error_desc['idType'] = customer_idType

                    customer_postalCode = validate_json('postalCode',invoice.get('billingAddress').get('zipcode') if invoice.get('billingAddress').get('zipcode') else '110111' )
                    list_error['postalCode'], list_error_desc['postalCode'] = customer_postalCode

                    customer_cityName = validate_json('cityName',invoice.get('billingAddress').get('city') if invoice.get('billingAddress').get('city') else 'BOGOTÁ, D.C.')
                    list_error['cityName'], list_error_desc['cityName'] = customer_cityName

                    customer_department = validate_json('department',invoice.get('billingAddress').get('province') if invoice.get('billingAddress').get('province') else 'Bogotá')
                    list_error['department'], list_error_desc['department'] = customer_department

                    customer_countryCode = validate_json('countryCode',invoice.get('billingAddress').get('country') if invoice.get('billingAddress').get('country') else 'None')
                    list_error['countryCode'], list_error_desc['countryCode'] = customer_countryCode
                    #print "list_error ", list_error
                    #print "list_error_desc ", list_error_desc
                    if False in list_error.values():
                        odooErrors = ""
                        for error in list_error_desc:
                            if isinstance(list_error_desc.get(error), dict):
                                odooErrors = odooErrors + "["+list_error_desc.get(error).get('Code')+"] => "+list_error_desc.get(error).get('info')+"\n"
                        #print "ERRORS VDE ", odooErrors
                        try:
                            odooClient.Write('hw.cfdi',[odooCFDIId],
                                {'result':odooErrors,'state':'error'})
                        except:
                            log.error( "ERROR EN METODO WRITE hw.cfdi,[result,state]")
                        #### CALLBACK
                        #callBack, params = putInvoiceResult(_trackId, _orderNo,False, False,odooErrors,False, False,False, env)
                        try:
                            odooClient.Write('hw.cfdi',[odooCFDIId],
                                {'put_result_code': callBack.get('resultCode'),
                                    'put_message': callBack.get('message'),
                                    'put_date': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                    #'hw_callback': params,
                                    })
                        except:
                            log.error("ERROR EN METODO WRITE hw.cfdi, callback")
                    else:
                        invoice_header = {
                            'orderNo': _orderNo,
                            'invoiceAttr': '1',
                            'invoiceNo': "%CUFE%",
                            'invoiceDate': "%STAMPING%",
                            'createdBy': invoice.get('header').get('createdBy'),
                            'totalItaxAmount': invoice.get('header').get('totalItaxAmount'),
                            'totalTaxAmount': invoice.get('header').get('totalTaxAmount'),
                            'description': invoice.get('header').get('description'),
                        }
                        invoice_header1 = {
                            'orderNo': _orderNo,
                            'invoiceAttr': '1',
                            'invoiceNo': '',
                            'invoiceDate': '',
                            'createdBy': invoice.get('header').get('createdBy'),
                            'totalItaxAmount': invoice.get('header').get('totalItaxAmount'),
                            'totalTaxAmount': invoice.get('header').get('totalTaxAmount'),
                            'description': invoice.get('header').get('description'),
                        }
                        _currency = invoice.get('header').get('currency')
                        _vatAmount = invoice.get('header').get('totalTaxAmount')
                        _issuedDate = invoice.get('header').get('creationDate')
                        _observation = invoice.get('header').get('description')
                        _purchaseOrder = invoice.get('header').get('orderNo')
                        _saleCondition = invoice.get('header').get('paymentForm')
                        _method = invoice.get('header').get('paymentMethod')
                        _amount = invoice.get('header').get('totalTaxAmount')
                        _percentage = 0.19
                        _items = invoice.get('items')
                        items_to_json, _base, invoice_items = get_items(_items)
                        print "_base ", _base
                        if _base.get('iva').get('gift') is False:
                            #decimals function
                            _el_total = decimals(_base.get('iva').get('base') +
                                _base.get('exento').get('base') +
                                _base.get('iva').get('impuesto'))
                        else:
                            #decimals function
                            _el_total = decimals(_base.get('iva').get('with_gift_amount'))
                            #_el_total = decimals(_base.get('exento').get('base') + _base.get('iva').get('impuesto'))
                        print "el_total ", _el_total
                        jsonToSave = {
                            'invoiceType': _invoiceType,
                            'operationCode': _operationCode,
                            'pointOfSale': _pointOfSale,
                            'customerInvoiceId': inv.get('folio'),
                            'customer': get_customer(invoice),
                            'currency': _currency,
                            'otherTaxesTotal': 0,
                            'vatAmount': decimals(_base.get('iva').get('impuesto')),
                            'exemptAmount': decimals(_base.get('exento').get('base')),
                            'taxableAmount': decimals(_base.get('iva').get('base')),
                            'total': _el_total,
                            'paid': '',
                            'taxes': [{
                                        #{
                                            'type': 'IVA',
                                            'amount': decimals(_base.get('iva').get('impuesto')),
                                            'taxableAmount': decimals(_base.get('iva').get('base')),
                                            'percentage': decimals(_percentage * 100)
                                        #}
                                    }],
                            'issuedDate': time_zone(_issuedDate),
                            'expirationDate': time_zone(_issuedDate),
                            'saleCondition': payform(_saleCondition)+" "+paymethod(_method),
                            'seller': 'XXX',
                            'observation': _observation,
                            'purchaseOrder': _purchaseOrder,
                            'area': '',
                            'order': '',
                            'referral': '',
                            'warehouse': '',
                            'note': '',
                            'items': items_to_json
                        }
                        jsonToSave_limpio = json.dumps(jsonToSave).decode('unicode-escape').encode('utf8')
                        datas = {
                                    'vde_cfdi_assoc': jsonToSave_limpio,
                                    #'vde_cfdi_assoc': json.dumps(jsonToSave).decode('unicode-escape').encode('utf8'),
                                    'send_mail_account': _send_mail_account,
                                    'send_mail_name': _send_mail_name,
                                    #'hw_invoice_head': json_clean(json.dumps(invoice_header)),
                                    'hw_invoice_head': json.dumps(invoice_header).decode('unicode-escape').encode('utf8'),
                                    #'hw_invoice_items': json_clean(json.dumps(invoice_items[0])),
                                    'hw_invoice_items': json.dumps(invoice_items).decode('unicode-escape').encode('utf8'),
                                }
                        #print "datas ", datas
                        try:
                            odooClient.Write('hw.cfdi',[odooCFDIId], datas)
                        except Exception:
                            log("ERROR EN METODO WRITE hw.cfdi,vde_cfdi_assoc")
                        doc_json = {'invoices': [json.loads(jsonToSave_limpio)]}
                        doc_json = limpia_string(json.dumps(doc_json))
                        #print "CREA ESTRUCTURA JSON"
                        #print "JSON to SIFACTURA ", doc_json
                        headers = {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        }
                        if env == 'TEST' or env == 'test' or env == 'Test':
                            urlSi = 'https://app.sifactura.co/api/v1/invoice/process/new/batch/json/HAB'
                            user = 'facturacionelectronica.espucal@gmail.com'
                            pwd = 'Password19'
                        if env == 'PROD' or env == 'prod' or env == 'Prod':
                            #urlSi = 'https://app.sifactura.co/api/v1/invoice/process/new/batch/json/PROD'
                            urlSi = 'https://app.sifactura.co/api/v1/invoice/process/new/batch/json/HAB'
                            user = 'hwco@vde-suite.com'
                            pwd = 'Password19'
                        try:
                            sifactura_response = requests.post(urlSi, headers=headers, auth=HTTPBasicAuth(user, pwd), data=doc_json)
                        except:
                            log.error("ERROR CONSUMIENDO API SIFACTURA ")
                        #log.info("sifactura_response ", sifactura_response.json())
                        sifactura_response = sifactura_response.json()
                        if (sifactura_response.get('errors') and len(sifactura_response.get('errors'))>0) or not sifactura_response.get('info'):
                            log.error("ERROR TIMBRADO")
                            odooErrors = ""
                            #if isinstance(sifactura_response.get('errors')[0].get('mensaje'), str):
                            if sifactura_response.get('errors')[0].get('mensaje'):
                                odooErrors = sifactura_response.get('errors')[0].get('mensaje')
                            if isinstance(sifactura_response.get('errors')[0].get('detalle'), list):
                                cont = 0
                                for error in sifactura_response.get('errors')[0].get('detalle'):
                                    #print "error ", error
                                    odooErrors = odooErrors + "[" + str(cont) + "] => " + error + "\n"
                                    cont = cont + 1
                            #print "odooErrors ",odooErrors
                            try:
                                odooClient.Write('hw.cfdi',[odooCFDIId],{'result': odooErrors, 'state':'error'})
                            except:
                                log.error("ERROR EN METODO WRITE hw.cfdi,[result,state] ")
                            #######CALLBACK
                            #callBack, params = putInvoiceResult(_trackId, _orderNo,False, False,odooErrors,False, invoice_header1,invoice_items, env)
                            #try:
                                #odooClient.Write('hw.cfdi',[odooCFDIId],
                                    #{'put_result_code': callBack.get('resultCode'),
                                        #'put_message': callBack.get('message'),
                                        #'put_date': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                        ##'hw_callback': params,
                                        #})
                            #except:
                                #log.error("ERROR EN METODO WRITE hw.cfdi, callback")
                        if sifactura_response.get('info') and not sifactura_response.get('errors'):
                            log.info("TIMBRADO CORRECTO")
                            try:
                                odooClient.Write('hw.cfdi', [inv.get('cfdi')], {
                                    #'folio': folio_actual,
                                    #'serie': serie_actual,
                                    'state': 'sent',
                                    'result': False
                                })
                            except:
                                log.error("ERROR EN METODO WRITE hw.cfdi,[folio,serie,state] ")
                            #folio_actual = folio_actual + 1
                            #try:
                                #odooClient.Write('ir.sequence',
                                    #[odooCfdiSeq[0]['id']], {
                                    #'number_next_actual': folio_actual,
                                #})
                            #except:
                                #log.error("ERROR EN METODO WRITE ir.sequence,[number_next_actual] ")
                else:
                    log.error("FACTURA YA EXISTE " + _trackId + " | " + _orderNo)






