# -*- coding: utf-8 -*-
import json
from funciones import *

_env = 'PROD'
_folio = 878587
hw_json = '{"shippingAddress": {"province": "HUILA", "address": "CALLE 56 # 1W-99 APTO 101 TORRE 7 CONJUNTO TORRES DE IPACARAI", "consignee": "ARNAL CUELLAR GOMEZ", "street": false, "orderNo": "91060289366", "id": 643301, "city": "NEIVA", "district": false, "zipcode": "410010", "type": 0, "email": false, "changeAskId": false, "orderId": 643301, "description": false, "deleteFlag": 0, "lastUpdatedBy": "system", "statecode": false, "createdBy": "system", "ruleName": "COHW001normal50", "creationDate": "2020-07-16 04:45:21+0800", "mobile": "3203700780", "country": "Colombia", "lastUpdateDate": "2020-07-16 04:45:21+0800", "systemName": "hic", "companyCode": false, "dimensionCode": "91060289366"}, "paymentItems": [{"orderId": 643301, "description": false, "paymentNo": "20200716TR45827088213870311701", "lastUpdateDate": "2020-07-16 04:45:21+0800", "companyCode": false, "deleteFlag": 0, "paymentType": "0", "amount": 0, "lastUpdatedBy": "system", "systemName": "hic", "paymentDate": "2020-07-16 03:53:07+0800", "createdBy": "system", "ruleName": "COHW001normal50", "orderNo": "91060289366", "creationDate": "2020-07-16 04:45:21+0800", "id": 346835, "dimensionCode": "91060289366"}], "trackId": 878587, "company": {"defaultApplyStatus": 99, "registeredOfficeAddress": false, "channelId": "26b78f6182c0475c9a26ddacd770b580", "companyRegistrationNumber": false, "contactEmail": false, "createdBy": false, "registeredAddressStateCode": false, "id": 51, "companyRea": false, "companyTradeRegistrationNo": false, "idealCompanyCode": "IN-KP000010", "companyDistrict": false, "companyRegisteredTelephone": false, "gstin": false, "companyRegisteredCapital": false, "parentCompanyName": false, "pan": false, "status": 1, "companyCity": false, "description": "哥伦比亚", "companyName": "HUAWEI TECHNOLOGIES COLOMBIA S A S", "companyRegisteredAddress": false, "companyDepositBank": false, "lastUpdatedBy": false, "companyRegisterionNo": false, "companyBankAccount": false, "companyTaxRegistrationNo": false, "creationDate": false, "companyCountry": false, "companyCode": "COHW001", "companyAptNo": false, "cin": false, "companyProvince": false, "lastUpdateDate": false, "contactTelephone": false, "companyWebsite": false}, "originalTrackId": false, "header": {"status": 0, "billingMasterId": "COHW001", "pdfGetStatus": 0, "sentDate": false, "invoiceDate": false, "ruleName": "COHW001normal50", "taxRegistrationNo": false, "orderTime": false, "invoiceAttr": "1", "invoiceNo": false, "warehouseCode": false, "scenario": false, "pickingNoEnd": false, "orderNo": "91060289366", "shippingAddressId": false, "id": 878587, "description": false, "creationDateEnd": false, "creationDate": "2020-07-16 23:05:05+0800", "invoiceCode": false, "sourceFlag": false, "invoiceNoCreatetime": false, "pickingNoStart": false, "totalGrossNtaxAmount": 6450824.37, "totalNtaxAmount": 4915630.25, "tbUploadStatus": 0, "receiptAmount": false, "returnType": false, "systemName": "hic", "paymentMethod": "47", "customerInfoId": false, "totalNoTaxTotalDiscount": 1535194.12, "changeAskId": false, "orderId": 643301, "businessType": 1, "customerName": false, "changeInvoiceAppCode": false, "billingDimensionCode": false, "errorMessage": false, "invoiceAddressId": false, "deleteFlag": 0, "lastUpdateDate": "2020-07-16 23:05:05+0800", "invoiceDrawer": false, "creationDateStart": false, "createdBy": "system", "multiId": false, "invoiceDateStart": false, "refundFlag": 0, "sourceInvoiceId": false, "remark": false, "paymentForm": "1", "language": "en-US", "totalGrossAmount": 7429500, "currency": "COP", "totalDiscountAmount": 1579900, "type": 50, "userid": "2640057000001833383", "einvoiceStatus": "0", "statusDescription": false, "batchId": false, "url": false, "invoiceDateEnd": false, "lastUpdatedBy": "system", "applyId": 0, "changeInvoiceAppStatus": false, "payDate": "2020-07-16 03:53:07+0800", "totalItaxAmount": 5849600, "totalTaxAmount": 933969.75, "dimensionCode": "91060289366"}, "originalInvoiceNo": false, "shipment": [{"warehouseCode": "WMWHSE5", "sentDate": "2020-07-16 22:54:51+0800", "orderNo": "91060289366", "id": 360071, "reviewDate": false, "location": false, "shipmentNo": "5891060289366S01", "carNo": false, "orderId": 643301, "supplierCompanyCode": "SVTG", "description": false, "logisticsNumbers": "2077693927", "shippingStatus": "20", "deleteFlag": 0, "lastUpdatedBy": "system", "createdBy": "system", "ruleName": "COHW001normal50", "creationDate": "2020-07-16 04:45:21+0800", "companyCode": false, "pickingNo": false, "lastUpdateDate": "2020-07-16 23:03:12+0800", "systemName": "hic", "dimensionCode": "91060289366"}], "items": [{"taxRate": 0.19, "spartName": "HUAWEI Band 4 (Amber Sunrise)", "grossAmount": 149900, "unitOfMeasure": false, "cgstAmount": false, "priceNtax": 125966.38655, "orderNo": "91060289366", "id": 1014641, "unit": "PCS", "rowNums": "1", "noTaxAmount": 125966.39, "specification": false, "ean": "6901443328048", "noTaxDiscountAmount": 0, "lineType": 1, "cgstRate": false, "sgstAmount": false, "sourceLineId": "10f926833f8c4fc4829e090c9cf21920", "payUnitPrice": 149900, "description": false, "igstAmount": false, "unitPriceNtax": 125966.39, "price": 149900, "hsn": "", "imeis": false, "deleteFlag": 0, "copyTax": false, "incTaxAmount": 149900, "createdBy": "system", "ruleName": "COHW001normal50", "grossNoTaxAmount": 125966.39, "skuCode": "80570114010000702", "creationDate": "2020-07-16 23:05:16+0800", "taxAmount": 23933.61, "companyCode": false, "igstRate": false, "sgstRate": false, "lastUpdateDate": "2020-07-16 23:05:16+0800", "lastUpdatedBy": "system", "systemName": "hic", "dimensionCode": "91060289366", "productType": "54111605", "invoiceHeaderId": 878587, "discountAmount": 0, "orderLineId": 997175, "quantity": 1}, {"taxRate": 0.19, "spartName": "HUAWEI P40 Pro (Deep Sea Blue) con HMS", "grossAmount": 4599900, "unitOfMeasure": false, "cgstAmount": false, "priceNtax": 3865462.18487, "orderNo": "91060289366", "id": 1014643, "unit": "PCS", "rowNums": "2", "noTaxAmount": 3865462.18, "specification": false, "ean": "6901443386161", "noTaxDiscountAmount": 0, "lineType": 1, "cgstRate": false, "sgstAmount": false, "sourceLineId": "1ce8bfb0c2504d99988e9f381012d1ec", "payUnitPrice": 4599900, "description": false, "igstAmount": false, "unitPriceNtax": 3865462.18, "price": 4599900, "hsn": "", "imeis": false, "deleteFlag": 0, "copyTax": false, "incTaxAmount": 4599900, "createdBy": "system", "ruleName": "COHW001normal50", "grossNoTaxAmount": 3865462.18, "skuCode": "80570110010000302", "creationDate": "2020-07-16 23:05:16+0800", "taxAmount": 734437.82, "companyCode": false, "igstRate": false, "sgstRate": false, "lastUpdateDate": "2020-07-16 23:05:16+0800", "lastUpdatedBy": "system", "systemName": "hic", "dimensionCode": "91060289366", "productType": "43191501", "invoiceHeaderId": 878587, "discountAmount": 0, "orderLineId": 997183, "quantity": 1}, {"taxRate": 0.19, "spartName": "HUAWEI Watch GT 2e (Graphite Black)", "grossAmount": 649900, "unitOfMeasure": false, "cgstAmount": false, "priceNtax": 546134.45378, "orderNo": "91060289366", "id": 1014645, "unit": "PCS", "rowNums": "3", "noTaxAmount": 470504.2, "specification": false, "ean": "6901443375288", "noTaxDiscountAmount": 75630.25, "lineType": 1, "cgstRate": false, "sgstAmount": false, "sourceLineId": "74ce0da4356b4bdeba46acc9ac86769b", "payUnitPrice": 559900, "description": false, "igstAmount": false, "unitPriceNtax": 470504.2, "price": 649900, "hsn": "", "imeis": false, "deleteFlag": 0, "copyTax": false, "incTaxAmount": 559900, "createdBy": "system", "ruleName": "COHW001normal50", "grossNoTaxAmount": 546134.45, "skuCode": "80570114010000401", "creationDate": "2020-07-16 23:05:16+0800", "taxAmount": 89395.8, "companyCode": false, "igstRate": false, "sgstRate": false, "lastUpdateDate": "2020-07-16 23:05:16+0800", "lastUpdatedBy": "system", "systemName": "hic", "dimensionCode": "91060289366", "productType": "54111605", "invoiceHeaderId": 878587, "discountAmount": 90000, "orderLineId": 997179, "quantity": 1}, {"taxRate": 0.19, "spartName": "HUAWEI FreeBuds 3 (carbon black)", "grossAmount": 729900, "unitOfMeasure": false, "cgstAmount": false, "priceNtax": 613361.34454, "orderNo": "91060289366", "id": 1014647, "unit": "PCS", "rowNums": "4", "noTaxAmount": 453697.48, "specification": false, "ean": "6901443346516", "noTaxDiscountAmount": 159663.87, "lineType": 1, "cgstRate": false, "sgstAmount": false, "sourceLineId": "48f2f29eacde4b1e9a68f9d9f36b1f9c", "payUnitPrice": 539900, "description": false, "igstAmount": false, "unitPriceNtax": 453697.48, "price": 729900, "hsn": "", "imeis": false, "deleteFlag": 0, "copyTax": false, "incTaxAmount": 539900, "createdBy": "system", "ruleName": "COHW001normal50", "grossNoTaxAmount": 613361.35, "skuCode": "80570113010000203", "creationDate": "2020-07-16 23:05:16+0800", "taxAmount": 86202.52, "companyCode": false, "igstRate": false, "sgstRate": false, "lastUpdateDate": "2020-07-16 23:05:16+0800", "lastUpdatedBy": "system", "systemName": "hic", "dimensionCode": "91060289366", "productType": "43222900", "invoiceHeaderId": 878587, "discountAmount": 190000, "orderLineId": 997177, "quantity": 1}, {"taxRate": 0, "spartName": "HUAWEI Y9 Prime (Esmerald Green)", "grossAmount": 1299900, "unitOfMeasure": false, "cgstAmount": false, "priceNtax": 1299900, "orderNo": "91060289366", "id": 1014649, "unit": "PCS", "rowNums": "5", "noTaxAmount": 0, "specification": false, "ean": "6901443310166", "noTaxDiscountAmount": 1299900, "lineType": 1, "cgstRate": false, "sgstAmount": false, "sourceLineId": "4508da153c1942fa921919d0111bad09", "payUnitPrice": 0, "description": false, "igstAmount": false, "unitPriceNtax": 0, "price": 1299900, "hsn": "", "imeis": false, "deleteFlag": 0, "copyTax": false, "incTaxAmount": 0, "createdBy": "system", "ruleName": "COHW001normal50", "grossNoTaxAmount": 1299900, "skuCode": "80570110010001303", "creationDate": "2020-07-16 23:05:16+0800", "taxAmount": 0, "companyCode": false, "igstRate": false, "sgstRate": false, "lastUpdateDate": "2020-07-16 23:05:16+0800", "lastUpdatedBy": "system", "systemName": "hic", "dimensionCode": "91060289366", "productType": "43191501", "invoiceHeaderId": 878587, "discountAmount": 1299900, "orderLineId": 997181, "quantity": 1}], "businessInfo": {"isEinvoice": 0, "registeredEmail": "arnalcuellar@hotmail.com", "orderNo": "91060289366", "id": 643301, "idCardNumber": "83237433", "companyRegisteredNo": false, "documentUse": false, "partyTaxSchemeName": false, "registeredTelephone": false, "applyId": false, "pan": false, "registeredDepositBank": false, "orderId": 643301, "description": false, "idType": "1", "deleteFlag": 0, "lastUpdatedBy": "system", "createdBy": "system", "ruleName": "COHW001normal50", "gstn": false, "creationDate": "2020-07-16 04:45:21+0800", "registeredAddress": false, "name": "ARNAL CUELLAR GOMEZ", "companyCode": false, "userid": "2640057000001833383", "systemName": "hic", "lastUpdateDate": "2020-07-16 04:45:21+0800", "taxRegistrationNo": false, "dimensionCode": "91060289366", "registeredBankAccount": false}, "billingAddress": {"province": "Huila", "address": "CALLE 56 # 1W-99 APTO 101 TORRE 7 CONJUNTO TORRES DE IPACARAI", "consignee": "ARNAL CUELLAR GOMEZ", "street": false, "orderNo": "91060289366", "id": 357933, "city": "NEIVA", "district": false, "zipcode": "410001", "vatLastName": "CUELLAR GOMEZ", "orderId": 643301, "description": false, "deleteFlag": 0, "lastUpdatedBy": "system", "vatFirstName": "ARNAL", "statecode": false, "createdBy": "system", "ruleName": "COHW001normal50", "creationDate": "2020-07-16 04:45:21+0800", "mobile": "3203700780", "country": "CO", "lastUpdateDate": "2020-07-16 04:45:21+0800", "systemName": "hic", "companyCode": false, "dimensionCode": "91060289366"}}'
listQueryInvoice = json.loads(hw_json, strict=False)
_invoiceType = 'FACTURA'
_operationCode = '10'
if _env == 'TEST' or _env == 'test' or _env== 'Test':
    _pointOfSale = 'SETT'
elif _env == 'PROD' or _env == 'prod' or _env== 'Prod':
    _pointOfSale = 'PROD'
for invoice in [listQueryInvoice]:
    _originalTrackId = invoice.get('originalTrackId')
    _originalInvoiceNo = invoice.get('originalInvoiceNo')
    _trackId = str(invoice.get('trackId'))
    _orderNo = _orderNo = invoice.get('header').get('orderNo')
    _orderNo = str(_orderNo)
    _creationDate = invoice.get('header').get('creationDate')
    _send_mail_account = invoice.get('businessInfo').get('registeredEmail')
    if invoice.get('billingAddress').get('vatFirstName'):
        name = invoice.get('billingAddress').get('vatFirstName')
    else:
        name = ''
    if invoice.get('billingAddress').get('vatLastName'):
        last_name = invoice.get('billingAddress').get('vatLastName')
    else:
        last_name = ''
    _send_mail_name =  name + " " + last_name
    _hash_cfdi = _trackId + "  "+_orderNo+"  "+_creationDate
    _currency = invoice.get('header').get('currency')
    _vatAmount = invoice.get('header').get('totalTaxAmount')
    _issuedDate = invoice.get('header').get('creationDate')
    _observation = invoice.get('header').get('description')
    _purchaseOrder = invoice.get('header').get('orderNo')
    _saleCondition = invoice.get('header').get('paymentForm')
    _method = invoice.get('header').get('paymentMethod')
    _amount = invoice.get('header').get('totalTaxAmount')
    _percentage = 0.19
    _items = invoice.get('items')
    items_to_json, _base, invoice_items = get_items(_items)
    print "_base ", _base
    if _base.get('iva').get('gift') is False:
        #decimals function
        _el_total = decimals(_base.get('iva').get('base') +
            _base.get('exento').get('base') +
            _base.get('iva').get('impuesto'))
    else:
        #decimals function
        _el_total = decimals(_base.get('iva').get('with_gift_amount'))
        #_el_total = decimals(_base.get('exento').get('base') + _base.get('iva').get('impuesto'))
    print "el_total ", _el_total
    jsonToSave = {
        'invoiceType': _invoiceType,
        'operationCode': _operationCode,
        'pointOfSale': _pointOfSale,
        'customerInvoiceId': _folio,
        'customer': get_customer(invoice),
        'currency': _currency,
        'otherTaxesTotal': 0,
        'vatAmount': decimals(_base.get('iva').get('impuesto')),
        'exemptAmount': decimals(_base.get('exento').get('base')),
        'taxableAmount': decimals(_base.get('iva').get('base')),
        'total': _el_total,
        'paid': '',
        'taxes': [{
                    #{
                        'type': 'IVA',
                        'amount': decimals(_base.get('iva').get('impuesto')),
                        'taxableAmount': decimals(_base.get('iva').get('base')),
                        'percentage': decimals(_percentage * 100)
                    #}
                }],
        'issuedDate': time_zone(_issuedDate),
        'expirationDate': time_zone(_issuedDate),
        'saleCondition': payform(_saleCondition)+" "+paymethod(_method),
        'seller': 'XXX',
        'observation': _observation,
        'purchaseOrder': _purchaseOrder,
        'area': '',
        'order': '',
        'referral': '',
        'warehouse': '',
        'note': '',
        'items': items_to_json
    }

    print jsonToSave