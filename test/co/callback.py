# -*- coding: utf-8 -*-
from odooclient import client
import requests
from requests.auth import HTTPBasicAuth
import datetime
import json, ast
from funciones import *
import os.path
from os import path
import base64
import time
import fcntl


def getLogs(_env):
    log = get_logger("callback", "log/callback.log", True)
    log.info("Iniciando getLogs")
    odooClient = client.OdooClient(host="localhost", port="8069", dbname="hwco", saas=False, debug=True)
    #print "odoo ", odooClient
    odooClient.ServerInfo()
    if odooClient.Authenticate("admin", "controlVDE"):
        args = {'order': 'date_entry desc'}
        criteria = [('state','=','sent'), ('folio','!=',''), ('put_result_code','=',False)]
        try:
            odooCfdi = odooClient.SearchRead('hw.cfdi', criteria,
            ['id','typeDoc','folio', 'hw_invoice_head', 'hw_invoice_items', 'hw_no_invoice', 'hw_track_id'],[], **args)
        except:
            "ERROR EN METODO SEARCH_READ hw.cfdi "
        #print "odooCfdi ", odooCfdi
        for cfdi in odooCfdi:
            _idDocto = cfdi.get('id')
            _docto = ''
            #print "_idDocto ", _idDocto
            if cfdi.get('typeDoc') == '1' or cfdi.get('typeDoc') == 1:
                _docto = 'FACTURA'
                _tipo = "inv/"
            elif cfdi.get('typeDoc') == '2' or cfdi.get('typeDoc') == 2:
                _docto = 'NOTA_CREDITO'
                _tipo = "nc/"
            elif cfdi.get('typeDoc') == '3' or cfdi.get('typeDoc') == 3:
                _docto = 'NOTA_DEBITO'
                _tipo = "nd/"
            _folio = cfdi.get('folio')
            log.info("Folio: " +str(_folio))
            log.info("_docto: " str(+_docto))
            if _env == 'TEST' or _env == 'test' or _env == 'Test':
                urlSi = 'https://app.sifactura.co/api/v1/invoice/getTransactionLog/HAB/SETT/' + str(_docto)+"/"+str(_folio)
                urlSiDoc = "https://app.sifactura.co/api/v1/invoice/%doc%/sales/HAB/SETT/" + str(_docto)+"/"+str(_folio)
                user = 'facturacionelectronica.espucal@gmail.com'
                pwd = 'Password19'
            if _env == 'PROD' or _env == 'prod' or _env == 'Prod':
                urlSi = 'https://app.sifactura.co/api/v1/invoice/getTransactionLog/PROD/PROD/' + str(_docto)+"/"+str(_folio)
                urlSiDoc = "https://app.sifactura.co/api/v1/invoice/%doc%/sales/PROD/PROD/" + str(_docto)+"/"+str(_folio)
                #urlSi = 'https://app.sifactura.co/api/v1/invoice/getTransactionLog/HAB/SETT/' + str(_docto)+"/"+str(_folio)
                #urlSiDoc = "https://app.sifactura.co/api/v1/invoice/%doc%/sales/HAB/SETT/" + str(_docto)+"/"+str(_folio)
                user = 'hwco@vde-suite.com'
                pwd = 'Password19'
            try:
                sifactura_response = requests.get(urlSi, auth=HTTPBasicAuth(user, pwd))
            except:
                log.error("ERROR CONSUMIENDO API SIFACTURA ")
            #print "sifactura_response ", sifactura_response.json()
            #print "sifactura_response ", type(sifactura_response.json())
            if len(sifactura_response.json())==0:
                continue
            else:
                log.info("Hay Facturas por procesar")
                sifactura_r = sifactura_response.json()
                if isinstance(sifactura_r, dict) is True:
                    data = {'state':sifactura_r.get('estado'),'detail':sifactura_r.get('mensaje'),'cfdi_id':cfdi.get('id')}
                    #print "data ", data
                    try:
                        odooCfdiLog = odooClient.Create('hw.cfdi.log',data)
                    except:
                        log.error("ERROR METODO CREATE hw.cfdi.log")
                    continue
                _is_autorizada = False
                _is_fallida = False
                for log in sifactura_r:
                    _transactionLogId = log.get('transactionLogId')
                    _saleInvoiceId = log.get('saleInvoiceId')
                    _username = log.get('username')
                    _detail = log.get('detail')
                    _date = log.get('date')
                    _status = log.get('status')
                    criteria = [('cfdi_id','=',_idDocto),('transaction_log_id','=',log.get('transactionLogId'))]
                    #print "criteria ", criteria
                    try:
                        odooCfdiLogs = odooClient.SearchRead('hw.cfdi.log',criteria,['transaction_log_id'])
                    except:
                        log.error("ERROR METODO CREATE hw.cfdi.log")
                    #print "odooCfdiLogs ", odooCfdiLogs
                    if len(odooCfdiLogs)==0:
                        data = {'cfdi_id':cfdi.get('id'),'transaction_log_id':_transactionLogId,'sale_invoice_id':_saleInvoiceId,'detail':_detail,'date':_date,'state':_status}
                        #print "data ", data
                        try:
                            odooCfdiLog = odooClient.Create('hw.cfdi.log',data)
                        except:
                            #print "ERROR METODO CREATE hw.cfdi.log"
                    #print type(log)
                    #log = eval(log)
                    #print "log ", log
                    #_status = log.get('status')
                    if _status == 'Fallida':
                        if _is_fallida == False:
                            data = {'state':'fail'}
                            try:
                                odooClient.Write('hw.cfdi',[_idDocto],data)
                            except:
                                log.error("ERROR METODO WRITE hw.cfdi,state")
                            _is_fallida = True
                    if _status == 'Autorizada':
                        _transactionLogId = log.get('transactionLogId')
                        _saleInvoiceId = log.get('saleInvoiceId')
                        _username = log.get('username')
                        _detail = log.get('detail')
                        _date = log.get('date')
                        _status = log.get('status')
                        _new_hw_invoice_head = ''
                        _new_hw_invoice_items = ''
                    #if _status == 'Autorizada':
                        #print "Status ",  _status
                        if _is_autorizada is False:
                            _path = "/opt/vde/dev_stamping/Co/"
                            _ruta = "files/" + str(_tipo)
                            _rutaE = "http://hwco.vde-suite.com:8869/vde-hw-co/files/"
                            docXMLEx = _rutaE + str(_folio) + ".xml"
                            docXML = _ruta + str(_folio) + ".xml"
                            docPDFEx = _rutaE + str(_folio) + ".pdf"
                            docPDF = _ruta + str(_folio) + ".pdf"
                            filess = _path + _ruta + str(_folio) + ".xml"
                            #print filess
                            file_exists = path.exists(filess)
                            if file_exists:
                                with open(filess) as f:
                                    fileCont = f.read()
                                data = base64.b64enconde(fileCont)
                                if not data:
                                    log.error("No existe archivo local ")
                                else:
                                    log.info("Si existe archivo local ")
                            else:
                                #print "DMZ"
                                try:
                                    sifactura_response_xml = requests.get(urlSiDoc.replace('%doc%','getXml'), auth=HTTPBasicAuth(user, pwd))
                                    xml_response = sifactura_response_xml.text
                                    #print "xml_response ", xml_response
                                    is_html = xml_response[0:22].replace('\n','').replace('\r','').replace(' ','')
                                    #print "'" + str(is_html) + "'"
                                except:
                                    log.error("ERROR EN GET XML SIFACTURA ")
                                try:
                                    sifactura_response_pdf = requests.get(urlSiDoc.replace('%doc%','getPdf'), auth=HTTPBasicAuth(user, pwd))
                                    pdf_response = sifactura_response_pdf.text
                                    #print "pdf response ", pdf_response
                                except:
                                    log.error("ERROR EN GET PDF SIFACTURA ")
                                if is_html == '<!DOCTYPEhtml>':
                                    data = {'state': 'success_wo_xml'}
                                    try:
                                        odooClient.Write('hw.cfdi',[_idDocto],data)
                                    except:
                                        log.error("ERROR METODO WRITE hw.cfdi,state=success_wo_xml")
                                    continue
                                try:
                                    if xml_response:
                                        with open(docXML, "w") as fileXML:
                                            fileXML.write(base64.b64decode(xml_response))
                                except:
                                    log.error("ERROR AL ESCRIBIR XML ")
                                try:
                                    if pdf_response:
                                        with open(docPDF, "w") as filePDF:
                                            filePDF.write(base64.b64decode(pdf_response))
                                except:
                                    log.error("ERROR AL ESCRIBIR PDF ")
                                if xml_response:
                                    headers = {
                                        'Accept': 'application/json',
                                        'Content-Type': 'application/json'
                                    }
                                    data = {
                                        'xml_string': xml_response
                                        }
                                    data = json.dumps(data)
                                    cufe_response = requests.post(
                                        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getCufe", headers=headers,data=data)
                                    #print "cufe_response ", cufe_response.json()
                                    cufe = cufe_response.json().get('cufe')
                                    stamp1 = cufe_response.json().get('fecha_timbrado1')
                                    stamp = cufe_response.json().get('fecha_timbrado')
                                    #print "_idDocto ", _idDocto
                                    #print "sifactura_response_xml.text ", xml_response
                                    #print "base64.b64decode(sifactura_response_xml.txt) ", base64.b64decode(xml_response)
                                    try:
                                        data = {
                                            'name': str(_folio) + ".xml",
                                            'datas_fname': str(_folio) + ".xml",
                                            'type': 'binary',
                                            'datas': xml_response,
                                            'db_datas': xml_response,
                                            #'index_content': str(base64.b64decode(xml_response)),
                                            'mimetype': 'application/xml',
                                            'res_model': 'hw.cfdi',
                                            'res_field': 'xml_attachment_id',
                                            'res_id': _idDocto,
                                            #'res_name': str(_folio) + ".xml",
                                        }
                                        attachmentXmlId = odooClient.Create('ir.attachment',data)
                                    except:
                                        log.error("ERROR EN METODO CREATE ir.attachment")
                                    try:
                                        data = {
                                            'name': str(_folio) + ".pdf",
                                            'datas_fname': str(_folio) + ".pdf",
                                            'type': 'binary',
                                            'datas': pdf_response,
                                            'db_datas': pdf_response,
                                            #'index_content': str(base64.b64decode(xml_response)),
                                            'mimetype': 'application/pdf',
                                            'res_model': 'hw.cfdi',
                                            'res_field': 'pdf_attachment_id',
                                            'res_id': _idDocto,
                                            #'res_name': str(_folio) + ".xml",
                                        }
                                        attachmentPDFId = odooClient.Create('ir.attachment',data)
                                    except:
                                        log.error("ERROR EN METODO CREATE ir.attachment")
                                    try:
                                        odooWebBaseUrl = odooClient.SearchRead('ir.config_parameter',[('key','=','web.base.url')],['value'])
                                    except:
                                        log.error("ERROR EN METODO SEARCH_READ ir.config_parameter ")
                                    odooWebBaseUrl = odooWebBaseUrl[0].get('value')
                                    try:
                                        local_urls = odooClient.SearchRead('ir.attachment',['|',('id','=',attachmentPDFId),('id','=',attachmentXmlId)],['local_url'])
                                    except:
                                        log.error("ERROR EN METODO SEARCH_READ ir.attachment ")
                                    urls = {}
                                    for local_url in local_urls:
                                        urls.update({local_url.get('id'): local_url.get('local_url')})
                                    #print "url ",odooWebBaseUrl + urls.get(attachmentXmlId)
                                    #print "url ",odooWebBaseUrl + urls.get(attachmentPDFId)
                                    #time.sleep(10)
                                    try:
                                        odooClient.Write('hw.cfdi',[_idDocto], {
                                            'xml_attachment_id': attachmentXmlId,
                                            #'xml': xml_response,
                                            'url_xml': odooWebBaseUrl + urls.get(attachmentXmlId),
                                            #'xml_string': str(base64.b64decode(xml_response)),
                                            'uuid': cufe,
                                            'date_stamp': stamp,
                                            'result': 'Stamped and Sent successfully'
                                            })
                                    except:
                                        log.error("ERROR EN METODO WRITE hw.cfdi,[xml, xml_string] ")
                                if pdf_response:
                                    try:
                                        odooClient.Write('hw.cfdi',[_idDocto], {
                                            #'pdf': pdf_response,
                                            'pdf_attachment_id': attachmentPDFId,
                                            'url_pdf': odooWebBaseUrl + urls.get(attachmentPDFId),
                                            })
                                    except:
                                        log.error("ERROR EN METODO WRITE hw.cfdi,[pdf] ")
                                hw_invoice_head = cfdi.get('hw_invoice_head')
                                _new_hw_invoice_head = hw_invoice_head.replace('%CUFE%', cufe).replace('%STAMPING%', stamp1)
                                _new_hw_invoice_head = json.loads(_new_hw_invoice_head)
                                _new_hw_invoice_items = cfdi.get('hw_invoice_items').replace("\n",'').replace("\r",'')
                                _new_hw_invoice_items = json.loads(_new_hw_invoice_items)
                                #print "Replace ", _new_hw_invoice_head
                                #print "Replace ", _new_hw_invoice_items
                                _trackId = cfdi.get('hw_track_id')
                                _orderNo = cfdi.get('hw_no_invoice')
                                doctos = []
                                doctos.append({
                                    'fileType': '1',
                                    'fileSuffix': 'pdf',
                                    'invoiceByte': pdf_response
                                    })
                                doctos.append({
                                    'fileType': '2',
                                    'fileSuffix': 'xml',
                                    'invoiceByte': xml_response
                                    })
                                #print "doctos ", doctos
                                try:
                                    odooClient.Write('hw.cfdi',[_idDocto], {
                                            'is_send': True,
                                            #'hw_callback': doctos,
                                            })
                                except:
                                    log.error("ERROR EN METODO WRITE hw.cfdi,[is_send, doctos] ")
                                callBack, params = putInvoiceResultLog(_trackId, _orderNo, cufe, pdf_response,False,doctos, _new_hw_invoice_head,_new_hw_invoice_items, _env)
                                try:
                                    odooClient.Write('hw.cfdi',[_idDocto],
                                        {'put_result_code': callBack.get('resultCode'),
                                            'put_message': callBack.get('message'),
                                            'put_date': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                            #'hw_callback': params
                                            })
                                except:
                                    log.error("ERROR EN METODO WRITE hw.cfdi, callback")
                                if callBack.get('resultCode') == '000000':
                                    try:
                                        odooClient.Write('hw.cfdi',[_idDocto],{'state':'success'})
                                    except:
                                        log.error("ERROR EN METODO WRITE hw.cfdi,state ")
                            _is_autorizada = True
        args = {'order': 'date_entry desc'}
        criteria = [('is_to_try','=',True),('state','=','fail'), ('folio','!=',''), ('put_result_code','=',False)]
        try:
            odooCfdi = odooClient.SearchRead('hw.cfdi', criteria,
            ['id','typeDoc','folio', 'vde_cfdi_assoc', 'hw_invoice_head', 'hw_invoice_items', 'hw_no_invoice', 'hw_track_id'],[], **args)
        except:
            log.error("ERROR EN METODO SEARCH_READ hw.cfdi ")
        #print "odooCfdi ", odooCfdi
        if len(odooCfdi)>=0:
            for cfdi in odooCfdi:
                #print "ID ", cfdi.get('id')
                doc_json = cfdi.get('vde_cfdi_assoc')
                doc_json = doc_json.replace("'",'"')
                #print "vde_cfdi_assoc ", doc_json
                #print type(doc_json)
                doc_json = {'invoices': [json.loads(doc_json)]}
                doc_json = limpia_string(json.dumps(doc_json))
                #print "CREA ESTRUCTURA JSON ", doc_json
                #print "JSON to SIFACTURA ", doc_json
                headers = {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
                if _env == 'TEST' or _env == 'test' or _env == 'Test':
                    urlSi = 'https://app.sifactura.co/api/v1/invoice/process/new/batch/json/HAB'
                    user = 'facturacionelectronica.espucal@gmail.com'
                    pwd = 'Password19'
                if _env == 'PROD' or _env == 'prod' or _env == 'Prod':
                    urlSi = 'https://app.sifactura.co/api/v1/invoice/process/new/batch/json/PROD'
                    #urlSi = 'https://app.sifactura.co/api/v1/invoice/process/new/batch/json/HAB'
                    user = 'hwco@vde-suite.com'
                    pwd = 'Password19'

                try:
                    sifactura_response = requests.post(urlSi, headers=headers, auth=HTTPBasicAuth(user, pwd), data=doc_json)
                except:
                    log.error("ERROR CONSUMIENDO API SIFACTURA ")
                #print "sifactura_response ", sifactura_response.json()
                if not sifactura_response.json().get('errors'):
                    data = {'state': 'sent', 'is_to_try':False}
                    try:
                        odooClient.Write('hw.cfdi',[cfdi.get('id')],data)
                    except:
                        log.error("ERROR METODO WRITE hw.cfdi,state,is_to_try")

#getLogs('PROD')
try:
    with open('/tmp/call_co', 'a') as flock:
        fcntl.flock(flock, fcntl.LOCK_EX | fcntl.LOCK_NB)
        getLogs('PROD')
        fcntl.flock(flock, fcntl.LOCK_UN)
except IOError as e:
    pass

