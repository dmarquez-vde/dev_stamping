# -*- coding: utf-8 -*-
import threading
from threading import *
import time
import datetime
import logging
from odooclient import client
import requests
import json, ast
from validations import *
from structure import *
from invoice import *
from logs import *


#LOG = "/var/log/vde/multi_hilos.log"
LOG = "log/multi_hilos.log"
logging.basicConfig(filename=LOG, filemode="w", level=logging.DEBUG,
    format='[%(levelname)s] (%(threadName)-s) %(message)s')


def invoices():
    logging.info("Thread INVOICES inicializado " + str(datetime.datetime.now()))
    #time.sleep(10)

    odooClient = client.OdooClient(host="192.168.200.3", port="8069", dbname="hwcopy", saas=False, debug=True)
    print "odoo ", odooClient
    odooClient.ServerInfo()
    if odooClient.Authenticate("admin", "controlVDE"):
        guid_response = requests.get(
            "http://cloud.vde-suite.com:8180/HWKeys/public/api/guid")
        guid = guid_response.json().get('guid')
        print "guid ", guid
        tipoDoc = 1
        tipoDocL = 'I'
        fstart = datetime.datetime.now()
        finicio = fstart - datetime.timedelta(hours=119) - datetime.timedelta(minutes=55)
        fechainicio = finicio.strftime("%Y-%m-%d %H:%M:%S") + "-0500"
        finicio = finicio.strftime("%Y-%m-%d %H:%M:%S")
        ffin = fstart - datetime.timedelta(minutes=1)
        fechafin = ffin.strftime("%Y-%m-%d %H:%M:%S") + "-0500"
        ffin = ffin.strftime("%Y-%m-%d %H:%M:%S")
        #fechainicio = "2020-07-07 02:29:07-0500"
        #fechafin = "2020-07-12 02:23:07-0500"
        print "Fechas: ", fechainicio
        print "Fechas: ", fechafin
        procesaDoctosResult = procesaDoctosInv(guid, tipoDoc, fechainicio, fechafin, tipoDocL,'')
        time.sleep(120)
    #logging.info("Thread suma "+str(a)+"+"+str(b)+"="+str(c))
    #logging.info("Thread INVOICES terminado " + str(datetime.datetime.now()))


def thread_invoice():
    logging.info("Thread HILO inicializado " + str(datetime.datetime.now()))
    while True:
        t1 = threading.Thread(name=datetime.datetime.now(), target=invoices)
        t1.start()
        t1.join()


def nc():
    logging.info("Thread NC inicializado " + str(datetime.datetime.now()))
    time.sleep(5)
    #logging.info("Thread suma "+str(a)+"+"+str(b)+"="+str(c))
    #logging.info("Thread NC terminado " + str(datetime.datetime.now()))


def thread_nc():
    logging.info("Thread HILO NC inicializado " + str(datetime.datetime.now()))
    while True:
        t1 = threading.Thread(name=datetime.datetime.now(), target=nc)
        t1.start()
        t1.join()


def callback():
    logging.info("Thread CALLBACK inicializado " + str(datetime.datetime.now()))
    getLogs('TEST')
    #time.sleep(3)
    #logging.info("Thread suma "+str(a)+"+"+str(b)+"="+str(c))
    #logging.info("Thread CALLBACK terminado " + str(datetime.datetime.now()))


def thread_callback():
    logging.info("Thread HILO CALLBACK inicializado " + str(datetime.datetime.now()))
    while True:
        t1 = threading.Thread(name=datetime.datetime.now(), target=callback)
        t1.start()
        t1.join()


if __name__ == '__main__':
    t1 = threading.Thread(name="hilo invoice", target=thread_invoice)
    t1.start()
    t2 = threading.Thread(name="hilo nc", target=thread_nc)
    t2.start()
    t3 = threading.Thread(name="hilo callback", target=thread_callback)
    t3.start()