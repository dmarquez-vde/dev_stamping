# -*- coding: utf-8 -*-
from odooclient import client
import requests
from requests.auth import HTTPBasicAuth
import datetime
import json, ast
from validations import *
from structure import *
import base64


def putInvoiceResult(_trackId, _orderNo, _invoiceNo, _bytePDF, _errorMessage, _invoiceFile, _invoiceHeader, _invoiceItems, _env):
    guid_response = requests.get(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/guid")
    _guid = guid_response.json().get('guid')
    print "guid ", _guid
    data = {
        'trackId': _trackId if _trackId is not False else None,
        'orderNo': _orderNo if _orderNo is not False else None,
        'invoiceNo': _invoiceNo if _invoiceNo is not False else None,
        'bytePDF': _bytePDF if _bytePDF is not False else None,
        'errorMessage': _errorMessage if _errorMessage is not False else None,
        'invoiceFile': _invoiceFile if _invoiceFile is not False else None,
        'invoiceHeader': [_invoiceHeader] if _invoiceHeader is not False else None,
        'invoiceItems': _invoiceItems if _invoiceItems is not False else None,
        'guid': _guid,
    }
    print "data ", data
    data = json_to_utf8(data)
    print "data ", data
    data = json.dumps(data)
    print "data ", data
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    params_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getParamsCallback",headers=headers, data=data)
    params = params_response.json()
    print "params ", params
    hash_ = params.get('hash')
    data={
        "country": "CO",
        "environment": _env,
        "guid": _guid}
    print "data ", data
    token_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getToken", data=data)
    print "token_response ", token_response.json()
    urlHW_I = json_to_utf8(token_response.json().get('urlCallback'))
    appKey = json_to_utf8(token_response.json().get('appKey'))
    token = json_to_utf8(token_response.json().get('token'))
    data = {
        'companyCode': 'COHW_VDE',
        'params': hash_,
        'token': token
    }
    print "data ", data
    data = json.dumps(data)
    print "data ", data
    headers = {
        'X-HW-ID': 'com.huawei.invoice_cloud',
        'X-HW-APPKEY': appKey,
        'Content-Type': 'application/json'
    }
    hw_response = requests.post(urlHW_I, headers=headers, data=data)
    print "hw_response ", hw_response.text
    return hw_response.json(), params.get('json')

def procesaDoctosInv(guid, tipoDoc, fechainicio, fechafin, tipoDocL, nextpage, _env):
    _invoiceType = 'NOTA_CREDITO'
    _operationCode = '20'
    _pointOfSale = 'SETT'
    #_pointOfSale = 'PROD'
    _customerInvoiceId = '2000020'
    env = _env
    data={
        "country": "CO",
        "environment": env,
        "guid": guid}
    token_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getToken", data=data)
    #token = json_clean(token_response.json())
    #print "token ", token_response.json()
    #token = json_to_utf8(token_response.json())
    #print "token ", token
    urlHW_I = json_to_utf8(token_response.json().get('url'))
    appKey = json_to_utf8(token_response.json().get('appKey'))
    token= json_to_utf8(token_response.json().get('token'))
    data = {
        'Type': tipoDoc,
        'StartTime': fechainicio,
        'EndTime': fechafin,
        'Guid': guid,
        'NextPage': nextpage
    }
    params_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getParams",data=data)
    #params = json_clean(params_response.json())
    params = params_response.json()
    #print "params", params
    hash_ = params.get('hash')
    str_json = params.get('json')
    #print "str_json ", str_json
    headers = {
        'X-HW-ID': 'com.huawei.invoice_cloud',
        #'X-HW-APPKEY': 'TDg5JEpGSStCM0M4X2lUVg==',
        'X-HW-APPKEY': appKey,
        'Content-Type': 'application/json'
    }
    #print "headers", headers
    datos_result = {
        'companyCode':'COHW_VDE',
        'params': hash_,
        'token': token
    }
    datos_json = json.dumps(datos_result)
    #print "datos_json ", datos_json
    ##### odoo create hw.execution
    datas = {
        'typeDoc': tipoDoc,
        'dateStart': fechainicio,
        'dateEnd': fechafin,
        'dateCron': fstart.strftime("%Y-%m-%d %H:%M:%S"),
        'dateExecution': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        'state': '0',
        'token': token,
        'params_crypt': params.get('hash'),
        'params': str_json,
        }
    #print "datas ", datas
    try:
        odooExecutionId = odooClient.Create('hw.execution', datas)
    except Exception, e:
        print "ERROR EN METODO CREATE hw.execution ", str(e)
    print "odooExecutionId ", odooExecutionId
    #urlHW_I = 'http://apigw.huawei.com/api/sg/invoice/queryinvoice'
    hw_response = requests.post(urlHW_I, headers=headers, data=datos_json)
    print "hw_response ", hw_response.text
    hw_response = hw_response.json()
    if hw_response.get('resultCode') == '500001':
        print "Llama a siguiente pagina ", hw_response.get('nextPage')
        datas = {
            'resultCode': hw_response.get('resultCode'),
            'resultData': False,
            'nextPage': False if hw_response.get('nextPage') is None else hw_response.get('nextPage'),
            'message': hw_response.get('message'),
            'execution_id': odooExecutionId,
            'guid': guid,
            #'json': json.dumps(listQueryInvoice),
        }
        #print "decodes ", json.dumps(listQueryInvoice).decode('unicode-escape').encode('utf8')
        print "datas ", datas
        try:
            odooResultId = odooClient.Create('hw.result', datas)
        except:
            print "ERROR EN METODO CREATE hw.result"
        print "odooResultId ", odooResultId
        procesaDoctosInv(guid, tipoDoc, fechainicio, fechafin, tipoDocL,
        hw_response.get('nextPage'), env)
    else:
        #print "resultData ", hw_response.get('resultData')
        data = {
            'Data': hw_response.get('resultData'),
            'Guid': guid
        }
        decrypt_response = requests.post(
            "http://cloud.vde-suite.com:8180/HWKeys/public/api/decrypt",
            data=data)
        listQueryInvoice = []
        for inv in decrypt_response.json().get('json'):
            listQueryInvoice.append(json_to_utf8(inv))
        listQueryInvoice
        datas = {
            'resultCode': hw_response.get('resultCode'),
            'resultData': hw_response.get('resultData'),
            'nextPage': False if hw_response.get('nextPage') is None else hw_response.get('nextPage'),
            'message': hw_response.get('message'),
            'execution_id': odooExecutionId,
            'guid': guid,
            'json': json.dumps(listQueryInvoice).decode('unicode-escape').encode('utf8'),
        }
        print "datas ", datas
        try:
            odooResultId = odooClient.Create('hw.result', datas)
        except:
            print "ERROR EN METODO CREATE hw.result"
        print "odooResultId ", odooResultId
        #listQueryInvoice = json.loads(listQueryInvoice)
        if len(listQueryInvoice) > 0:
            print "No. Documents ", len(listQueryInvoice)
            for invoice in listQueryInvoice:
                #print "invoice ", invoice
                _originalTrackId = invoice.get('originalTrackId')
                _originalInvoiceNo = invoice.get('originalInvoiceNo')
                _trackId = str(invoice.get('trackId'))
                _orderNo = _orderNo = invoice.get('header').get('orderNo')
                _orderNo = str(_orderNo)
                _creationDate = invoice.get('header').get('creationDate')
                _send_mail_account = invoice.get('businessInfo').get('registeredEmail')
                if invoice.get('billingAddress').get('vatFirstName'):
                    name = invoice.get('billingAddress').get('vatFirstName')
                else:
                    name = ''
                if invoice.get('billingAddress').get('vatLastName'):
                    last_name = invoice.get('billingAddress').get('vatLastName')
                else:
                    last_name = ''
                _send_mail_name =  name + " " + last_name
                _hash_cfdi = _trackId + "|"+_orderNo+"|"+_creationDate
                print '######### '+_hash_cfdi+' ########'
                #######search.read ir.sequence
                criteria = [('code','=','hw.cfdi,invoice')]
                try:
                    odooCfdiSeq = odooClient.SearchRead('ir.sequence',criteria,['number_next_actual','prefix','id'])
                except:
                    print "ERROR EN METODO SEARCH_READ ir.sequence"
                print "odooCfdiSeq ", odooCfdiSeq
                folio_actual = odooCfdiSeq[0]['number_next_actual']
                serie_actual = odooCfdiSeq[0]['prefix']
                print "folio actual", folio_actual
                #######search.read hw.cfdi
                criteria = [('hw_no_invoice','=',_orderNo),('hw_track_id','=',_trackId),('state','in',['success','sent'])]
                print "criteria ", criteria
                try:
                    odooCfdi = odooClient.SearchRead('hw.cfdi',criteria,['uuid'])
                except:
                    print "ERROR EN METODO SEARCH_READ hw.cfdi "
                print "odooCFdi ", odooCfdi
                if len(odooCfdi)==0:
                    print "PROCESAR FACTURAS"
                    ######create hw.cfdi
                    datas = {
                        'hw_no_invoice': _orderNo,
                        'hw_track_id': _trackId,
                        'state': 'pending',
                        'date_entry': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        'date_push': invoice.get('header').get('creationDate'),
                        'hw_json': json.dumps(invoice).decode('unicode-escape').encode('utf8'),
                        'execution_id': odooExecutionId
                    }
                    #print "datas ", datas
                    try:
                        odooCFDIId = odooClient.Create('hw.cfdi',datas)
                    except Exception as e:
                        print "ERROR EN METODO CREATE hw.cfdi ",e
                    print "odooCFDIId ", odooCFDIId
                    print "VALIDACIONES"
                    list_error = {}
                    list_error_desc = {}
                    if invoice.get('businessInfo').get('taxRegistrationNo')=='':
                        customer_id = validate_json('id', invoice.get('businessInfo').get('idCardNumber'))
                    else:
                        customer_id = validate_json('id', invoice.get('businessInfo').get('taxRegistrationNo'))
                    print "customer_id ", customer_id
                    list_error['id'], list_error_desc['id'] = customer_id

                    customer_idType = validate_json('idType',invoice.get('businessInfo').get('idType'))
                    list_error['idType'], list_error_desc['idType'] = customer_idType

                    customer_postalCode = validate_json('postalCode',invoice.get('billingAddress').get('zipcode') if invoice.get('billingAddress').get('zipcode') else '110111' )
                    list_error['postalCode'], list_error_desc['postalCode'] = customer_postalCode

                    customer_cityName = validate_json('cityName',invoice.get('billingAddress').get('city') if invoice.get('billingAddress').get('city') else 'BOGOTÁ, D.C.')
                    list_error['cityName'], list_error_desc['cityName'] = customer_cityName

                    customer_department = validate_json('department',invoice.get('billingAddress').get('province') if invoice.get('billingAddress').get('province') else 'Bogotá')
                    list_error['department'], list_error_desc['department'] = customer_department

                    customer_countryCode = validate_json('countryCode',invoice.get('billingAddress').get('country') if invoice.get('billingAddress').get('country') else 'None')

                    list_error['countryCode'], list_error_desc['countryCode'] = customer_countryCode
                    criteria = [('hw_no_invoice','=',_originalInvoiceNo),('hw_track_id','=',_originalTrackId),('state','=','success')]
                    try:
                        odooInvoice = odooClient.SearchRead('hw.cfdi',criteria,['folio'])
                    except:
                        print "ERROR EN METODO SEARCH_READ hw.cfdi "
                    print "Factura origen ", odooInvoice
                    if len(odooInvoice) == 0:
                        list_error['originalInvoiceNo'] = False
                        local_error_desc = {'Code': '9999', 'info': 'No Invoice '+str(_originalInvoiceNo)+" and TrackId "+str(_originalTrackId) + " not found on system"}
                        list_error_desc['originalInvoiceNo'] = local_error_desc
                    #print "list_error ", list_error
                    #print "list_error_desc ", list_error_desc
                    if False in list_error.values():
                        odooErrors = ""
                        for error in list_error_desc:
                            if isinstance(list_error_desc.get(error), dict):
                                odooErrors = odooErrors + "["+list_error_desc.get(error).get('Code')+"] => "+list_error_desc.get(error).get('info')+"\n"
                        print "ERRORS VDE ", odooErrors
                        try:
                            odooClient.Write('hw.cfdi',[odooCFDIId],
                                {'result':odooErrors,'state':'error'})
                        except:
                            print "ERROR EN METODO WRITE hw.cfdi,[result,state]"
                        #### CALLBACK
                        callBack, params = putInvoiceResult(_trackId, _orderNo,False, False,odooErrors,False, False,False, env)
                        try:
                            odooClient.Write('hw.cfdi',[odooCFDIId],
                                {'put_result_code': callBack.get('resultCode'),
                                    'put_message': callBack.get('message'),
                                    'put_date': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                    'hw_callback': params,
                                    })
                        except:
                            print "ERROR EN METODO WRITE hw.cfdi, callback"
                    else:
                        invoice_header = {
                            'orderNo': _orderNo,
                            'invoiceAttr': '2',
                            'invoiceNo': "%CUFE%",
                            'invoiceDate': "%STAMPING%",
                            'createdBy': invoice.get('header').get('createdBy'),
                            'totalItaxAmount': invoice.get('header').get('totalItaxAmount'),
                            'totalTaxAmount': invoice.get('header').get('totalTaxAmount'),
                            'description': invoice.get('header').get('description'),
                        }
                        invoice_header1 = {
                            'orderNo': _orderNo,
                            'invoiceAttr': '2',
                            'invoiceNo': '',
                            'invoiceDate': '',
                            'createdBy': invoice.get('header').get('createdBy'),
                            'totalItaxAmount': invoice.get('header').get('totalItaxAmount'),
                            'totalTaxAmount': invoice.get('header').get('totalTaxAmount'),
                            'description': invoice.get('header').get('description'),
                        }
                        _currency = invoice.get('header').get('currency')
                        _vatAmount = invoice.get('header').get('totalTaxAmount')
                        _issuedDate = invoice.get('header').get('creationDate')
                        _observation = invoice.get('header').get('description')
                        _referencedInvoiceNumber = odooInvoice[0].get('folio')
                        _purchaseOrder = invoice.get('header').get('orderNo')
                        _saleCondition = invoice.get('header').get('paymentForm')
                        _method = invoice.get('header').get('paymentMethod')
                        _reasonCode = invoice.get('header').get('businessType')
                        _amount = invoice.get('header').get('totalTaxAmount')
                        _percentage = 0.19
                        _items = invoice.get('items')
                        items_to_json, _base, invoice_items = get_items(_items)
                        if _base.get('iva').get('gift') is False:
                            #decimals function
                            _el_total = decimals(_base.get('iva').get('base') +
                                _base.get('exento').get('base') +
                                _base.get('iva').get('impuesto'))
                        else:
                            #decimals function
                            _el_total = decimals(_base.get('iva').get('with_gift_amount'))
                        jsonToSave = {
                            'invoiceType': _invoiceType,
                            'operationCode': _operationCode,
                            'pointOfSale': _pointOfSale,
                            'customerInvoiceId': folio_actual,
                            'referencedInvoiceNumber': _referencedInvoiceNumber,
                            'reasonCode': reason(_reasonCode),
                            'customer': get_customer(invoice),
                            'currency': _currency,
                            'otherTaxesTotal': 0,
                            'vatAmount': decimals(_base.get('iva').get('impuesto')),
                            'exemptAmount': decimals(_base.get('exento').get('base')),
                            'taxableAmount': decimals(_base.get('iva').get('base')),
                            'total': _el_total,
                            'paid': '',
                            'taxes': [{
                                        #{
                                            'type': 'IVA',
                                            'amount': decimals(_base.get('iva').get('impuesto')),
                                            'taxableAmount': decimals(_base.get('iva').get('base')),
                                            'percentage': decimals(_percentage * 100)
                                        #}
                                    }],
                            'issuedDate': time_zone(_issuedDate),
                            'expirationDate': time_zone(_issuedDate),
                            'saleCondition': payform(_saleCondition)+" "+paymethod(_method),
                            'seller': 'XXX',
                            'observation': _observation,
                            'purchaseOrder': _purchaseOrder,
                            'area': '',
                            'order': '',
                            'referral': '',
                            'warehouse': '',
                            'note': '',
                            'items': items_to_json
                        }
                        #print "INVOICE_HEADER ", type(json.dumps(invoice_header).decode('unicode-escape').encode('utf8'))
                        #print "INVOICE_ITEMS ", type(json.dumps(invoice_items[0]).decode('unicode-escape').encode('utf8'))
                        jsonToSave_limpio = json.dumps(jsonToSave).decode('unicode-escape').encode('utf8')
                        datas = {
                                    'vde_cfdi_assoc': jsonToSave_limpio,
                                    #'vde_cfdi_assoc': json.dumps(jsonToSave).decode('unicode-escape').encode('utf8'),
                                    'send_mail_account': _send_mail_account,
                                    'send_mail_name': _send_mail_name,
                                    #'hw_invoice_head': json_clean(json.dumps(invoice_header)),
                                    'hw_invoice_head': json.dumps(invoice_header).decode('unicode-escape').encode('utf8'),
                                    #'hw_invoice_items': json_clean(json.dumps(invoice_items[0])),
                                    'hw_invoice_items': json.dumps(invoice_items).decode('unicode-escape').encode('utf8'),
                                }
                        #print "datas ", datas
                        try:
                            odooClient.Write('hw.cfdi',[odooCFDIId], datas)
                        except Exception:
                            print "ERROR EN METODO WRITE hw.cfdi,vde_cfdi_assoc"
                        doc_json = {'invoices': [json.loads(jsonToSave_limpio)]}
                        doc_json = limpia_string(json.dumps(doc_json))
                        print "CREA ESTRUCTURA JSON"
                        #print "JSON to SIFACTURA ", doc_json
                        headers = {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        }
                        if env == 'TEST' or env == 'test' or env == 'Test':
                            urlSi = 'https://app.sifactura.co/api/v1/invoice/process/new/batch/json/HAB'
                            user = 'facturacionelectronica.espucal@gmail.com'
                            pwd = 'Password19'
                        if env == 'PROD' or env == 'prod' or env == 'Prod':
                            #urlSi = 'https://app.sifactura.co/api/v1/invoice/process/new/batch/json/PROD'
                            urlSi = 'https://app.sifactura.co/api/v1/invoice/process/new/batch/json/HAB'
                            user = 'hwco@vde-suite.com'
                            pwd = 'Password19'
                        try:
                            sifactura_response = requests.post(urlSi, headers=headers, auth=HTTPBasicAuth(user, pwd), data=doc_json)
                        except:
                            print "ERROR CONSUMIENDO API SIFACTURA "
                        print "sifactura_response ", sifactura_response.json()
                        sifactura_response = sifactura_response.json()
                        if (sifactura_response.get('errors') and len(sifactura_response.get('errors'))>0) or not sifactura_response.get('info'):
                            print "ERROR TIMBRADO"
                            odooErrors = ""
                            #if isinstance(sifactura_response.get('errors')[0].get('mensaje'), str):
                            if sifactura_response.get('errors')[0].get('mensaje'):
                                odooErrors = sifactura_response.get('errors')[0].get('mensaje')
                            if isinstance(sifactura_response.get('errors')[0].get('detalle'), list):
                                cont = 0
                                for error in sifactura_response.get('errors')[0].get('detalle'):
                                    print "error ", error
                                    odooErrors = odooErrors + "[" + str(cont) + "] => " + error + "\n"
                                    cont = cont + 1
                            print "odooErrors ",odooErrors
                            try:
                                odooClient.Write('hw.cfdi',[odooCFDIId],{'result': odooErrors, 'state':'error'})
                            except:
                                print "ERROR EN METODO WRITE hw.cfdi,[result,state] "
                            #######CALLBACK
                            callBack, params = putInvoiceResult(_trackId, _orderNo,False, False,odooErrors,False, invoice_header1,invoice_items, env)
                            try:
                                odooClient.Write('hw.cfdi',[odooCFDIId],
                                    {'put_result_code': callBack.get('resultCode'),
                                        'put_message': callBack.get('message'),
                                        'put_date': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                        'hw_callback': params,
                                        })
                            except:
                                print "ERROR EN METODO WRITE hw.cfdi, callback"
                        if sifactura_response.get('info') and not sifactura_response.get('errors'):
                            print "TIMBRADO CORRECTO"
                            try:
                                odooClient.Write('hw.cfdi', [odooCFDIId], {
                                    'folio': folio_actual,
                                    'serie': serie_actual,
                                    'state': 'sent'
                                })
                            except:
                                print "ERROR EN METODO WRITE hw.cfdi,[folio,serie,state] "
                            folio_actual = folio_actual + 1
                            try:
                                odooClient.Write('ir.sequence',
                                    [odooCfdiSeq[0]['id']], {
                                    'number_next_actual': folio_actual,
                                })
                            except:
                                print "ERROR EN METODO WRITE ir.sequence,[number_next_actual] "
                else:
                    print "FACTURA YA EXISTE " + _trackId + " | " + _orderNo
        if hw_response.get('nextPage'):
            print "RECURSIVO, NUEVA PAGINA ", str(hw_response.get('nextPage'))
            procesaDoctosInv(guid, tipoDoc, fechainicio, fechafin, tipoDocL, hw_response.get('nextPage'), env)


odooClient = client.OdooClient(host="192.168.200.3", port="8069", dbname="hwcopy", saas=False, debug=True)
print "odoo ", odooClient
odooClient.ServerInfo()
if odooClient.Authenticate("admin", "controlVDE"):
    guid_response = requests.get(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/guid")
    guid = guid_response.json().get('guid')
    print "guid ", guid
    tipoDoc = 2
    tipoDocL = 'E'
    fstart = datetime.datetime.now()
    finicio = fstart - datetime.timedelta(hours=119) - datetime.timedelta(minutes=55)
    fechainicio = finicio.strftime("%Y-%m-%d %H:%M:%S") + "-0500"
    finicio = finicio.strftime("%Y-%m-%d %H:%M:%S")
    ffin = fstart - datetime.timedelta(minutes=1)
    fechafin = ffin.strftime("%Y-%m-%d %H:%M:%S") + "-0500"
    ffin = ffin.strftime("%Y-%m-%d %H:%M:%S")
    #fechainicio = "2020-07-07 02:29:07-0500"
    #fechafin = "2020-07-12 02:23:07-0500"
    print "Fechas: ", fechainicio
    print "Fechas: ", fechafin
    procesaDoctosResult = procesaDoctosInv(guid, tipoDoc, fechainicio, fechafin, tipoDocL,'','TEST')

