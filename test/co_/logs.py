# -*- coding: utf-8 -*-
from odooclient import client
import requests
from requests.auth import HTTPBasicAuth
import datetime
import json, ast
from validations import *
from structure import *
import os.path
from os import path
import base64
import time


def putInvoiceResultLog(_trackId, _orderNo, _invoiceNo, _bytePDF, _errorMessage, _invoiceFile, _invoiceHeader, _invoiceItems, _env):
    guid_response = requests.get(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/guid")
    _guid = guid_response.json().get('guid')
    #print "guid ", _guid
    data = {
        'trackId': _trackId if _trackId is not False else None,
        'orderNo': _orderNo if _orderNo is not False else None,
        'invoiceNo': _invoiceNo if _invoiceNo is not False else None,
        'bytePDF': _bytePDF if _bytePDF is not False else None,
        'errorMessage': _errorMessage if _errorMessage is not False else None,
        'invoiceFile': _invoiceFile if _invoiceFile is not False else None,
        'invoiceHeader': [_invoiceHeader] if _invoiceHeader is not False else None,
        'invoiceItems': [_invoiceItems] if _invoiceItems is not False else None,
        'guid': _guid,
    }
    #print "data ", data
    data = json_to_utf8(data)
    #print "data ", data
    data = json.dumps(data)
    #print "data ", data
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    params_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getParamsCallback",headers=headers, data=data)
    params = params_response.json()
    #print "params ", params
    hash_ = params.get('hash')
    data={
        "country": "CO",
        "environment": _env,
        "guid": _guid}
    #print "data ", data
    token_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getToken", data=data)
    #print "token_response ", token_response.json()
    urlHW_I = json_to_utf8(token_response.json().get('urlCallback'))
    appKey = json_to_utf8(token_response.json().get('appKey'))
    token = json_to_utf8(token_response.json().get('token'))
    data = {
        'companyCode': 'COHW_VDE',
        'params': hash_,
        'token': token
    }
    #print "data ", data
    data = json.dumps(data)
    #print "data ", data
    headers = {
        'X-HW-ID': 'com.huawei.invoice_cloud',
        'X-HW-APPKEY': appKey,
        'Content-Type': 'application/json'
    }
    hw_response = requests.post(urlHW_I, headers=headers, data=data)
    print "hw_response ", hw_response.text
    return hw_response.json(), params.get('json')


def getLogs(_env):
    odooClient = client.OdooClient(host="192.168.200.3", port="8069", dbname="hwcopy", saas=False, debug=True)
    print "odoo ", odooClient
    odooClient.ServerInfo()
    if odooClient.Authenticate("admin", "controlVDE"):
        criteria = [('state','=','sent'), ('folio','!=',''), ('uuid','=',False)]
        try:
            odooCfdi = odooClient.SearchRead('hw.cfdi', criteria,
            ['id','typeDoc','folio', 'hw_invoice_head', 'hw_invoice_items', 'hw_no_invoice', 'hw_track_id'])
        except:
            "ERROR EN METODO SEARCH_READ hw.cfdi "
        #print "odooCfdi ", odooCfdi
        for cfdi in odooCfdi:
            _idDocto = cfdi.get('id')
            _docto = ''
            print "_idDocto ", _idDocto
            if cfdi.get('typeDoc') == '1' or cfdi.get('typeDoc') == 1:
                _docto = 'FACTURA'
            elif cfdi.get('typeDoc') == '2' or cfdi.get('typeDoc') == 2:
                _docto = 'NOTA_CREDITO'
            elif cfdi.get('typeDoc') == '3' or cfdi.get('typeDoc') == 3:
                _docto = 'NOTA_DEBITO'
            _folio = cfdi.get('folio')
            print "Folio: " +_folio
            print "_docto: " +_docto
            if _env == 'TEST' or _env == 'test' or _env == 'Test':
                urlSi = 'https://app.sifactura.co/api/v1/invoice/getTransactionLog/HAB/SETT/' + str(_docto)+"/"+str(_folio)
                urlSiDoc = "https://app.sifactura.co/api/v1/invoice/%doc%/sales/HAB/SETT/" + str(_docto)+"/"+str(_folio)
                user = 'facturacionelectronica.espucal@gmail.com'
                pwd = 'Password19'
            if _env == 'PROD' or _env == 'prod' or _env == 'Prod':
                #urlSi = 'https://app.sifactura.co/api/v1/invoice/getTransactionLog/PROD/PROD/' + str(_docto)+"/"+str(_folio)
                #urlSiDoc = "https://app.sifactura.co/api/v1/invoice/%doc%/sales/PROD/PROD/" + str(_docto)+"/"+str(_folio)
                urlSi = 'https://app.sifactura.co/api/v1/invoice/getTransactionLog/HAB/SETT/' + str(_docto)+"/"+str(_folio)
                urlSiDoc = "https://app.sifactura.co/api/v1/invoice/%doc%/sales/HAB/SETT/" + str(_docto)+"/"+str(_folio)
                user = 'hwco@vde-suite.com'
                pwd = 'Password19'
            try:
                sifactura_response = requests.get(urlSi, auth=HTTPBasicAuth(user, pwd))
            except:
                print "ERROR CONSUMIENDO API SIFACTURA "
            #print "sifactura_response ", sifactura_response.json()
            #print "sifactura_response ", len(sifactura_response.json())
            if len(sifactura_response.json())==0:
                continue
            else:
                print "encontro facturas"
                _is_autorizada = False
                for log in sifactura_response.json():
                    print "log ", log
                    _transactionLogId = log.get('transactionLogId')
                    _saleInvoiceId = log.get('saleInvoiceId')
                    _username = log.get('username')
                    _detail = log.get('detail')
                    _date = log.get('date')
                    _status = log.get('status')
                    _new_hw_invoice_head = ''
                    _new_hw_invoice_items = ''
                    if _status == 'Autorizada':
                        print "Status ",  _status
                        if _is_autorizada is False:
                            _path = "/opt/vde/dev_stamping/Co/"
                            _ruta = "files/"
                            _rutaE = "http://hwco.vde-suite.com:8869/vde-hw-co/files/"
                            docXMLEx = _rutaE + str(_folio) + ".xml"
                            docXML = _ruta + str(_folio) + ".xml"
                            docPDFEx = _rutaE + str(_folio) + ".pdf"
                            docPDF = _ruta + str(_folio) + ".pdf"
                            filess = _path + _ruta + str(_folio) + ".xml"
                            print filess
                            file_exists = path.exists(filess)
                            if file_exists:
                                with open(filess) as f:
                                    fileCont = f.read()
                                data = base64.b64enconde(fileCont)
                                if not data:
                                    print "No existe archivo local "
                                else:
                                    print "Si existe archivo local "
                            else:
                                try:
                                    sifactura_response_xml = requests.get(urlSiDoc.replace('%doc%','getXml'), auth=HTTPBasicAuth(user, pwd))
                                    xml_response = sifactura_response_xml.text
                                except:
                                    print "ERROR EN GET XML SIFACTURA "
                                try:
                                    sifactura_response_pdf = requests.get(urlSiDoc.replace('%doc%','getPdf'), auth=HTTPBasicAuth(user, pwd))
                                    pdf_response = sifactura_response_pdf.text
                                except:
                                    print "ERROR EN GET PDF SIFACTURA "
                                try:
                                    if xml_response:
                                        with open(docXML, "w") as fileXML:
                                            fileXML.write(base64.b64decode(xml_response))
                                except:
                                    print "ERROR AL ESCRIBIR XML "
                                try:
                                    if pdf_response:
                                        with open(docPDF, "w") as filePDF:
                                            filePDF.write(base64.b64decode(pdf_response))
                                except:
                                    print "ERROR AL ESCRIBIR PDF "
                                if xml_response:
                                    headers = {
                                        'Accept': 'application/json',
                                        'Content-Type': 'application/json'
                                    }
                                    data = {
                                        'xml_string': xml_response
                                        }
                                    data = json.dumps(data)
                                    cufe_response = requests.post(
                                        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getCufe", headers=headers,data=data)
                                    print "cufe_response ", cufe_response.json()
                                    cufe = cufe_response.json().get('cufe')
                                    stamp1 = cufe_response.json().get('fecha_timbrado1')
                                    stamp = cufe_response.json().get('fecha_timbrado')
                                    #print "_idDocto ", _idDocto
                                    #print "sifactura_response_xml.text ", xml_response
                                    #print "base64.b64decode(sifactura_response_xml.txt) ", base64.b64decode(xml_response)
                                    try:
                                        data = {
                                            'name': str(_folio) + ".xml",
                                            'datas_fname': str(_folio) + ".xml",
                                            'type': 'binary',
                                            'datas': xml_response,
                                            'db_datas': xml_response,
                                            #'index_content': str(base64.b64decode(xml_response)),
                                            'mimetype': 'application/xml',
                                            'res_model': 'hw.cfdi',
                                            'res_field': 'xml_attachment_id',
                                            'res_id': _idDocto,
                                            #'res_name': str(_folio) + ".xml",
                                        }
                                        attachmentXmlId = odooClient.Create('ir.attachment',data)
                                    except:
                                        print "ERROR EN METODO CREATE ir.attachment"
                                    try:
                                        data = {
                                            'name': str(_folio) + ".pdf",
                                            'datas_fname': str(_folio) + ".pdf",
                                            'type': 'binary',
                                            'datas': pdf_response,
                                            'db_datas': pdf_response,
                                            #'index_content': str(base64.b64decode(xml_response)),
                                            'mimetype': 'application/pdf',
                                            'res_model': 'hw.cfdi',
                                            'res_field': 'pdf_attachment_id',
                                            'res_id': _idDocto,
                                            #'res_name': str(_folio) + ".xml",
                                        }
                                        attachmentPDFId = odooClient.Create('ir.attachment',data)
                                    except:
                                        print "ERROR EN METODO CREATE ir.attachment"
                                    try:
                                        odooWebBaseUrl = odooClient.SearchRead('ir.config_parameter',[('key','=','web.base.url')],['value'])
                                    except:
                                        print "ERROR EN METODO SEARCH_READ ir.config_parameter "
                                    odooWebBaseUrl = odooWebBaseUrl[0].get('value')
                                    try:
                                        local_urls = odooClient.SearchRead('ir.attachment',['|',('id','=',attachmentPDFId),('id','=',attachmentXmlId)],['local_url'])
                                    except:
                                        print "ERROR EN METODO SEARCH_READ ir.attachment "
                                    urls = {}
                                    for local_url in local_urls:
                                        urls.update({local_url.get('id'): local_url.get('local_url')})
                                    print "url ",odooWebBaseUrl + urls.get(attachmentXmlId)
                                    print "url ",odooWebBaseUrl + urls.get(attachmentPDFId)
                                    time.sleep(10)
                                    try:
                                        odooClient.Write('hw.cfdi',[_idDocto], {
                                            'x_xml': attachmentXmlId,
                                            #'xml': xml_response,
                                            'url_xml': odooWebBaseUrl + urls.get(attachmentXmlId),
                                            'xml_string': str(base64.b64decode(xml_response)),
                                            'uuid': cufe,
                                            'date_stamp': stamp,
                                            'result': 'Stamped and Sent successfully'
                                            })
                                    except:
                                        print "ERROR EN METODO WRITE hw.cfdi,[xml, xml_string] "
                                if pdf_response:
                                    try:
                                        odooClient.Write('hw.cfdi',[_idDocto], {
                                            #'pdf': pdf_response,
                                            'x_pdf': attachmentPDFId,
                                            'url_pdf': odooWebBaseUrl + urls.get(attachmentPDFId),
                                            })
                                    except:
                                        print "ERROR EN METODO WRITE hw.cfdi,[pdf] "
                                hw_invoice_head = cfdi.get('hw_invoice_head')
                                _new_hw_invoice_head = hw_invoice_head.replace('%CUFE%', cufe).replace('%STAMPING%', stamp1)
                                _new_hw_invoice_head = json.loads(_new_hw_invoice_head)
                                _new_hw_invoice_items = cfdi.get('hw_invoice_items')
                                _new_hw_invoice_items = json.loads(_new_hw_invoice_items)
                                print "Replace ", _new_hw_invoice_head
                                print "Replace ", _new_hw_invoice_items
                                _trackId = cfdi.get('hw_track_id')
                                _orderNo = cfdi.get('hw_no_invoice')
                                doctos = []
                                doctos.append({
                                    'fileType': '1',
                                    'fileSuffix': 'pdf',
                                    'invoiceByte': pdf_response
                                    })
                                doctos.append({
                                    'fileType': '2',
                                    'fileSuffix': 'xml',
                                    'invoiceByte': xml_response
                                    })
                                #print "doctos ", doctos
                                try:
                                    odooClient.Write('hw.cfdi',[_idDocto], {
                                            'is_send': True,
                                            #'hw_callback': doctos,
                                            })
                                except:
                                    print "ERROR EN METODO WRITE hw.cfdi,[is_send, doctos] "
                                callBack, params = putInvoiceResultLog(_trackId, _orderNo, cufe, pdf_response,False,doctos, _new_hw_invoice_head,_new_hw_invoice_items, _env)
                                try:
                                    odooClient.Write('hw.cfdi',[_idDocto],
                                        {'put_result_code': callBack.get('resultCode'),
                                            'put_message': callBack.get('message'),
                                            'put_date': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                            'hw_callback': params
                                            })
                                except:
                                    print "ERROR EN METODO WRITE hw.cfdi, callback"
                                if callBack.get('resultCode') == '000000':
                                    try:
                                        odooClient.Write('hw.cfdi',[_idDocto],{'state':'success'})
                                    except:
                                        print "ERROR EN METODO WRITE hw.cfdi,state "
                            _is_autorizada = True


getLogs('TEST')