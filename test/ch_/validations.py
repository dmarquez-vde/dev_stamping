# -*- coding: utf-8 -*-
#from catalogs import vdeCity, vdeDepartment, vdeCountryCode
import locale


def validate_json(nodo, val):
    if nodo == 'type':
        if (val==50) or (val == "50"):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1000', 'info': ' "type" node error - the value sent "'+ str(val)+'": Values 50 or 51 only allowed'}
    if nodo == 'id':
        if (val != '' or val is not False):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1001', 'info': ' "id" node error - the value sent "'+ str(val)+'": cannot be empty'}
    if nodo == 'idType':
        if(val == '1' or val == '2' or val == 1 or val == 2):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1002',
            'info': ' "idType" node error - the value sent "'+ str(val)+'": Values 1 or 2 only allowed'}
    if nodo == 'postalCode':
        if(len(val) <= 6):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1003',
            'info': ' "postalCode" node error - the value sent "'+ str(val)+'": only length 6 digits'}
    if nodo == 'cityName':
        #if val.encode('utf-8') not in vdeCity:
        if val not in vdeCity:
            return_ = False
            return_desc = {'Code':'1004',
            'info': ' "cityName" node error - the value sent "'+ val.encode('utf-8').strip()+'": is not included into the DIAN list'}
        else:
            return_ = True
            return_desc = True
    if nodo == 'department':
        #if val.encode('utf-8') not in vdeDepartment:
        if val not in vdeDepartment:
            return_ = False
            return_desc = {'Code':'1005',
            'info': ' "department" node error - the value sent "'+ val.encode('utf-8').strip()+'": is not included into the DIAN list'}
        else:
            return_ = True
            return_desc = True
    if nodo == 'countryCode':
        if(val in vdeCountryCode):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1006',
            'info': ' "countryCode" node error - the value sent "'+ val.encode('utf-8').strip()+'": is not included into the DIAN list'}
    if nodo == 'referencedInvoiceNumber':
        if (val != '' or val is not False):
            return_ = True
            return_desc = True
        else:
            return_ = False
            return_desc = {'Code':'1007',
            'info': ' "referencedInvoiceNumber" node error - the value sent "'+ str(val)+'": cannot be empty'}
    return return_, return_desc


def number_format(num, places=0):
    return locale.format_string("%.*f", (places, num), True)


def decimals(numero):
    numero = float(numero)
    res = number_format(numero, 4)
    ultimos = res.split(".")
    valorStr = str(ultimos[1])
    valor = valorStr[1]
    valor3 = valorStr[2]
    ultValores = valorStr[0] + valorStr[1]
    #print(valor)
    if int(valor) < 5:
        resultado = number_format(numero, 2)
        #print(resultado)
    elif int(valor) > 5 and int(valor) <= 9:
        resultado = number_format(numero, 2)
        #print(resultado)
    elif int(valor) == 5:
        if int(valor3) % 2 == 0:
            resultado = ultimos[0] + '.' + ultValores
            #print(resultado)
        else:
            resultado = number_format(numero, 2)
            #print(resultado)
    return float(resultado)