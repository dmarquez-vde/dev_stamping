# -*- coding: utf-8 -*-
from odooclient import client
import requests
from requests.auth import HTTPBasicAuth
import datetime
import json, ast
#from validations import *
from structure import *


def procesaDoctosInv(guid, tipoDoc, fechainicio, fechafin, tipoDocL, nextpage):





    env = "TEST"
    data={
        "country": "CL",
        "environment": env,
        "guid": guid}
    token_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getToken", data=data)
    #token = json_clean(token_response.json())
    #print "token ", token_response.json()
    #token = json_to_utf8(token_response.json())
    #print "token ", token
    urlHW_I = json_to_utf8(token_response.json().get('url'))
    appKey = json_to_utf8(token_response.json().get('appKey'))
    token= json_to_utf8(token_response.json().get('token'))
    data = {
        'Type': tipoDoc,
        'StartTime': fechainicio,
        'EndTime': fechafin,
        'Guid': guid,
        'NextPage': nextpage
    }
    params_response = requests.post(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/getParams",data=data)
    #params = json_clean(params_response.json())
    params = params_response.json()
    #print "params", params
    hash_ = params.get('hash')
    str_json = params.get('json')
    #print "str_json ", str_json
    headers = {
        'X-HW-ID': 'com.huawei.invoice_cloud',
        #'X-HW-APPKEY': 'ZzBMUzk3XmpVbGVjVVl2eg==',
        'X-HW-APPKEY': appKey,
        'Content-Type': 'application/json'
    }
    #print "headers", headers
    datos_result = {
        'companyCode':'CLHW_VDE',
        'params': hash_,
        'token': token
    }
    datos_json = json.dumps(datos_result)
    #print "datos_json ", datos_json
    ##### odoo create hw.execution
    datas = {
        'typeDoc': tipoDoc,
        'dateStart': fechainicio,
        'dateEnd': fechafin,
        'dateCron': fstart.strftime("%Y-%m-%d %H:%M:%S"),
        'dateExecution': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        'state': '0',
        'token': token,
        'params_crypt': params.get('hash'),
        'params': str_json,
        }
    #print "datas ", datas
    try:
        odooExecutionId = odooClient.Create('hw.execution', datas)
    except:
        print "ERROR EN METODO CREATE hw.execution"
    print "odooExecutionId ", odooExecutionId
    #urlHW_I = 'http://apigw.huawei.com/api/sg/invoice/queryinvoice'
    hw_response = requests.post(urlHW_I, headers=headers, data=datos_json)
    print "hw_response ", hw_response.text
    hw_response = hw_response.json()
    if hw_response.get('resultCode') == '500001':
        print "Llama a siguiente pagina ", hw_response.get('nextPage')
        datas = {
            'resultCode': hw_response.get('resultCode'),
            'resultData': False,
            'nextPage': False if hw_response.get('nextPage') is None else hw_response.get('nextPage'),
            'message': hw_response.get('message'),
            'execution_id': odooExecutionId,
            'guid': guid,
            #'json': json.dumps(listQueryInvoice),
        }
        #print "decodes ", json.dumps(listQueryInvoice).decode('unicode-escape').encode('utf8')
        print "datas ", datas
        try:
            odooResultId = odooClient.Create('hw.result', datas)
        except:
            print "ERROR EN METODO CREATE hw.result"
        print "odooResultId ", odooResultId
        procesaDoctosInv(guid, tipoDoc, fechainicio, fechafin, tipoDocL,
        hw_response.get('nextPage'))
    else:
        #print "resultData ", hw_response.get('resultData')
        data = {
            'Data': hw_response.get('resultData'),
            'Guid': guid
        }
        decrypt_response = requests.post(
            "http://cloud.vde-suite.com:8180/HWKeys/public/api/decrypt",
            data=data)
        listQueryInvoice = []
        for inv in decrypt_response.json().get('json'):
            listQueryInvoice.append(json_to_utf8(inv))
        listQueryInvoice
        datas = {
            'resultCode': hw_response.get('resultCode'),
            'resultData': hw_response.get('resultData'),
            'nextPage': False if hw_response.get('nextPage') is None else hw_response.get('nextPage'),
            'message': hw_response.get('message'),
            'execution_id': odooExecutionId,
            'guid': guid,
            'json': json.dumps(listQueryInvoice).decode('unicode-escape').encode('utf8'),
        }
        print "datas ", datas
        try:
            odooResultId = odooClient.Create('hw.result', datas)
        except:
            print "ERROR EN METODO CREATE hw.result"
        print "odooResultId ", odooResultId
        #listQueryInvoice = json.loads(listQueryInvoice)
        if len(listQueryInvoice) > 0:
            print "No. Documents ", len(listQueryInvoice)
            for invoice in listQueryInvoice:
                #print "invoice ", invoice
                _originalTrackId = invoice.get('originalTrackId')
                _originalInvoiceNo = invoice.get('originalInvoiceNo')
                _trackId = str(invoice.get('trackId'))
                _orderNo = _orderNo = invoice.get('header').get('orderNo')
                _orderNo = str(_orderNo)
                _creationDate = invoice.get('header').get('creationDate')
                _type = invoice.get('header').get('type')
                _send_mail_account = invoice.get('businessInfo').get('registeredEmail')
                if invoice.get('billingAddress').get('vatFirstName'):
                    name = invoice.get('billingAddress').get('vatFirstName')
                else:
                    name = ''
                if invoice.get('billingAddress').get('vatLastName'):
                    last_name = invoice.get('billingAddress').get('vatLastName')
                else:
                    last_name = ''
                _send_mail_name =  name + " " + last_name
                _hash_cfdi = _trackId + "|"+_orderNo+"|"+_creationDate
                print '######### '+_hash_cfdi+' ########'
                #######search.read ir.sequence
                criteria = [('code','=','hw.cfdi,invoice')]
                try:
                    odooCfdiSeq = odooClient.SearchRead('ir.sequence',criteria,['number_next_actual','prefix','id'])
                except:
                    print "ERROR EN METODO SEARCH_READ ir.sequence"
                print "odooCfdiSeq ", odooCfdiSeq
                folio_actual = odooCfdiSeq[0]['number_next_actual']
                serie_actual = odooCfdiSeq[0]['prefix']
                print "folio actual", folio_actual
                #######search.read hw.cfdi
                criteria = [('hw_no_invoice','=',_orderNo),('hw_track_id','=',_trackId),('state','in',['success','sent'])]
                print "criteria ", criteria
                try:
                    odooCfdi = odooClient.SearchRead('hw.cfdi',criteria,['uuid'])
                except:
                    print "ERROR EN METODO SEARCH_READ hw.cfdi "
                print "odooCFdi ", odooCfdi
                if len(odooCfdi)==0:
                    print "PROCESAR FACTURAS"
                    ######create hw.cfdi
                    datas = {
                        'hw_no_invoice': _orderNo,
                        'hw_track_id': _trackId,
                        'state': 'pending',
                        'date_entry': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                        'date_push': invoice.get('header').get('creationDate'),
                        'hw_json': json.dumps(invoice),
                        'execution_id': odooExecutionId
                    }
                    #print "datas ", datas
                    try:
                        odooCFDIId = odooClient.Create('hw.cfdi',datas)
                    except Exception as e:
                        print "ERROR EN METODO CREATE hw.cfdi ",e
                    print "odooCFDIId ", odooCFDIId
                    print "VALIDACIONES"
                    list_error = {}
                    list_error_desc = {}
                    customer_type = validate_json('type', _type)
                    print "customer_type ", customer_type
                    if False in list_error.values():
                        odooErrors = ""
                        for error in list_error_desc:
                            if isinstance(list_error_desc.get(error), dict):
                                odooErrors = odooErrors + "["+list_error_desc.get(error).get('Code')+"] => "+list_error_desc.get(error).get('info')+"\n"
                        print "ERRORS VDE ", odooErrors
                        try:
                            odooClient.Write('hw.cfdi',[odooCFDIId],
                                {'result':odooErrors,'state':'error'})
                        except:
                            print "ERROR EN METODO WRITE hw.cfdi,[result,state]"
                        #### CALLBACK
                        #callBack, params = putInvoiceResult(_trackId, _orderNo,False, False,odooErrors,False, False,False, env)
                        #try:
                            #odooClient.Write('hw.cfdi',[odooCFDIId],
                                #{'put_result_code': callBack.get('resultCode'),
                                    #'put_message': callBack.get('message'),
                                    #'put_date': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                                    ##'hw_callback': params,
                                    #})
                        #except:
                            #log.error("ERROR EN METODO WRITE hw.cfdi, callback")
                    else:
                        #sin errores
                        encabezado = get_encabezado(invoice,_type,folio_actual)
                        print "encabezado ", encabezado

odooClient = client.OdooClient(host="192.168.200.3", port="8069", dbname="hwclpy", saas=False, debug=True)
print "odoo ", odooClient
odooClient.ServerInfo()
if odooClient.Authenticate("admin", "controlVDE"):
    guid_response = requests.get(
        "http://cloud.vde-suite.com:8180/HWKeys/public/api/guid")
    guid = guid_response.json().get('guid')
    print "guid ", guid
    tipoDoc = 1
    tipoDocL = 'I'
    fstart = datetime.datetime.now()
    #finicio = fstart - datetime.timedelta(hours=119) - datetime.timedelta(minutes=55)
    finicio = fstart - datetime.timedelta(hours=119) - datetime.timedelta(minutes=55)
    fechainicio = finicio.strftime("%Y-%m-%d %H:%M:%S") + "-0500"
    finicio = finicio.strftime("%Y-%m-%d %H:%M:%S")
    ffin = fstart - datetime.timedelta(minutes=1)
    fechafin = ffin.strftime("%Y-%m-%d %H:%M:%S") + "-0500"
    ffin = ffin.strftime("%Y-%m-%d %H:%M:%S")
    print "Fechas: ", fechainicio
    print "Fechas: ", fechafin
    procesaDoctosResult = procesaDoctosInv(guid, tipoDoc, fechainicio, fechafin, tipoDocL,'')

