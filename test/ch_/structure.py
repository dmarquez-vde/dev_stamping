# -*- coding: utf-8 -*-
from dateutil import parser
import pytz
import json, ast
from validations import *

def json_to_utf8(json_):
    #print json_
    inv = json_
    #for inv in json_:
    if isinstance(inv, dict):
        for key in inv.keys():
            #print "key ", key
            if isinstance(inv.get(key), dict):
                keys = inv.get(key).keys()
                for key_ in keys:
                    #print "key_ ", key_
                    val = inv.get(key).get(key_)
                    if  not (isinstance(val, float) or isinstance(val, int)):
                        if val == None:
                            inv.get(key).update({key_: False})
                        else:
                            inv.get(key).update({key_: unicode(val).encode('utf-8')})
            elif isinstance(inv.get(key), list):
                inv.get(key)
                subs = []
                for sub in inv.get(key):
                    #print "sub ",sub
                    recurs = json_to_utf8(sub)
                    #print "recurs ", recurs
                    subs.append(recurs)
                inv.update({key_:subs})
            else:
                val = inv.get(key)
                if  not (isinstance(val, float) or isinstance(val, int)):
                    if val == None:
                        inv.update({key: False})
                    else:
                        inv.update({key: unicode(val).encode('utf-8')})
        #print "limpio ", json_
        return inv
    else:
        if json_ is None:
            print "es None"
            return False
        #print "no es dict ", json_
        if  not (isinstance(json_, float) or isinstance(json_, int)):
            #print "no es num "
            if json_ == None:
                return False
            else:
                return unicode(json_).encode('utf-8')


def json_clean(json_):
    return ast.literal_eval(json.dumps(json_))


def time_zone_long(date):
    get_date_obj = parser.parse(date)
    #print(get_date_obj.strftime("%Y-%m-%d %H:%M:%S%z"))
    bogota = pytz.timezone('America/Bogota')
    observationTime = get_date_obj.astimezone(bogota)
    #print(observationTime.strftime("%Y-%m-%d %H:%M:%S%z"))
    #print(observationTime.strftime("%Y-%m-%d %H:%M:%S"))
    return observationTime.strftime("%Y-%m-%d %H:%M:%S")


def time_zone_short(date):
    get_date_obj = parser.parse(date)
    #print(get_date_obj.strftime("%Y-%m-%d %H:%M:%S%z"))
    bogota = pytz.timezone('America/Bogota')
    observationTime = get_date_obj.astimezone(bogota)
    #print(observationTime.strftime("%Y-%m-%d %H:%M:%S%z"))
    #print(observationTime.strftime("%Y-%m-%d %H:%M:%S"))
    return observationTime.strftime("%Y-%m-%d")


def get_encabezado(invoice, customer_type, folio, env):
    ciudad = ''
    comuna = ''
    giro = ''
    if customer_type=='50':
        doc_type = '39'
        giro = ''
    elif customer_type=='51':
        doc_type = '33'
        for extend in invoice.get('businessInfo').get('invoiceExtendList'):
            if extend.get('paramCode') == 'province':
                ciudad = extend.get('paramValue')
            if extend.get('paramCode') == 'city':
                comuna = extend.get('paramValue')
            if extend.get('paramCode') == 'industry':
                giro = extend.get('paramValue')
    encabezado = {
        'TipoRegistro': 'E',
        'TipoDocumento': doc_type,
        'NumeroFolio': folio,
        'FechaEmision': time_zone_short(invoice.get('header').get('creationDate')),
        'IndNoRebaja': ' ',
        'TipoDespacho': '0',
        'IndTipoTrasBien': ' ',
        'IndServPeri': '0',
        'IndMonBru': '0',
        'FormaPago': header.get('paymentForm'),
        'FechaCancelacion': '00000000',
        'PeriodoDesde': '00000000',
        'PeriodoHasta': '00000000',
        'MedioPago': header.get('paymentMethod'),
        'TerminosPagoC': '    ',
        'TerminosPagoD': '01',
        'FechaVencimiento': time_zone_short(invoice.get('header').get('creationDate')),
        'RutEmisor': '99535120',
        'DigVerEmi': 'K',
        'NumRes': '80' if env == 'PROD' else '0',
        'NombreEmisor': 'Huawei (Chile) S.A.',
        'GiroComercialEmisor': 'VENTA AL POR MENOR DE COMPUTADORES, EQUIPO PERIFERICO, PROGRAMAS INFORMATICOS Y EQUIPO TELECOM',
        'CodigoActEconomica':  '474100',
        'SucEmiDoc': '',
        'CodSucReg': '',
        'DireccionOrigen': 'Rosario Norte 50',
        'ComunaOrigen': 'Las Condes',
        'CiudadOrigen': 'Santiago',
        'CodigoVendedor': '',
        'RutMandante': '',
        'DigVerDemandante': '',
        'RutReceptor': 0 if customer_type == '50' and not invoice.get('businessInfo').get('taxRegistrationNo') else '' if customer_type == '51' else '',
        'DigVerReceptor': invoice.get('businessInfo').get('taxRegistrationNo').split('-')[1] if customer_type == '50' and invoice.get('businessInfo').get('taxRegistrationNo') else '0' if customer_type=='51' else '',
        'CodIntReceptor': '123',
        'NombreReceptor': invoice.get('businessInfo').get('name'),
        'GiroNegReceptor': giro,
        'ContReceptor': '',
        'DirReceptor': invoice.get('shippingAddress').get('address') if customer_type == '50' else invoice.get('businessInfo').get('registeredAddress'),
        'ComunaReceptor': invoice.get('shippingAddress').get('city') if customer_type == '50' else comuna,
        'CiudadReceptor': invoice.get('shippingAddress').get('province') if customer_type == '50' else ciudad,
        'DirPosReceptor': '',
        'ComunaPosReceptor': '',
        'CiudadPosReceptor': '',
        'RutSolicitante': invoice.get('businessInfo').get('taxRegistrationNo').split('-')[0] if customer_type == '51' else '',
        'DigSolFactura': invoice.get('businessInfo').get('taxRegistrationNo').split('-')[1] if customer_type == '51' else '',
        'PatenteTransporte': '',
        'RutTransportista': '',
        'DigTransportista': '',
        'DirDestino': '',
        'ComunaDestino': '',
        'CiudadDestino': '',
        'MontoNeto': 0,
        'MontoExento': 0,
        'MontoInformado': 0,
        'TasaIva': 0,
        'MontoIva': 0,
        'MontoIvaRetenido': 0,
        'CreditoEspecial': 0,
        'GarantiaDeposito': 0,
        'MontoTotal': 0,
        'MontoNoFacturable': 0,
        'MontoPeriodo': 0,
        'SaldoAnterior': 0,
        'ValorPagar': 0,
        'Sistema': 'VDESuite',
        'Usuario': 'Huawei',
        'TemplateImpresion': 'BOLETA',
        'VersionTempImpre': '1',
        'Impresora': '',
        'ReceptorElectronico': 0,
        'FormaAceptacion': 'A',
        'TipoFoleacion': 0,
        'MontoIvaPropio': 0,
        'MontoIvaTerceros': 0,
        'CopiasImprimir':0,
        'CodigoActEconomica2': '',
        'IdenficadorInterno': invoice.get('trackId'),
        'ImprimirCedible': ''
    }
    return encabezado







#### funciones de proyeto colombia

def limpia_string(cadena):
    cadena = cadena.replace('\u00c0','À').replace('\u00c1','Á').replace('\u00c2','Â').replace('\u00c3','Ã').replace('\u00c4','Ä').replace('\u00c5','Å').replace('\u00c6','Æ').replace('\u00c7','Ç').replace('\u00c8','È').replace('\u00c9','É').replace('\u00ca','Ê').replace('\u00cb','Ë').replace('\u00cc','Ì').replace('\u00cd','Í').replace('\u00ce','Î').replace('\u00cf','Ï').replace('\u00d1','Ñ').replace('\u00d2','Ò').replace('\u00d3','Ó').replace('\u00d4','Ô').replace('\u00d5','Õ').replace('\u00d6','Ö').replace('\u00d8','Ø').replace('\u00d9','Ù').replace('\u00da','Ú').replace('\u00db','Û').replace('\u00dc','Ü').replace('\u00dd','Ý').replace('\u00df','ß').replace('\u00e0','à').replace('\u00e1','á').replace('\u00e2','â').replace('\u00e3','ã').replace('\u00e4','ä').replace('\u00e5','å').replace('\u00e6','æ').replace('\u00e7','ç').replace('\u00e8','è').replace('\u00e9','é').replace('\u00ea','ê').replace('\u00eb','ë').replace('\u00ec','ì').replace('\u00ed','í').replace('\u00ee','î').replace('\u00ef','ï').replace('\u00f0','ð').replace('\u00f1','ñ').replace('\u00f2','ò').replace('\u00f3','ó').replace('\u00f4','ô').replace('\u00f5','õ').replace('\u00f6','ö').replace('\u00f8','ø').replace('\u00f9','ù').replace('\u00fa','ú').replace('\u00fb','û').replace('\u00fc','ü').replace('\u00fd','ý').replace('\u00ff','ÿ')
    return cadena

def validaID(val, types):
    if types == 'PASAPORTE':
        resu = '0'
    else:
        resu = val
    return resu


def validaEX(val, types):
    if types == 'PASAPORTE':
        resu = val
    else:
        resu = ''
    return resu


def types(valor):
    if valor == '1':
        idType = "CEDULA_DE_CIUDADANIA"
    if valor == '2':
        idType = "PASAPORTE"
    return idType


def get_customer(listQueryInvoice):
    #_id = listQueryInvoice.get('businessInfo').get('taxRegistrationNo')
    _id = ''
    _name = listQueryInvoice.get('businessInfo').get('name') or 'VDE'
    #_idType = types(listQueryInvoice.get('businessInfo').get('idType'))
    _idType = types('2')
    _email = listQueryInvoice.get('businessInfo').get('registeredEmail')
    _telephone = listQueryInvoice.get('businessInfo').get('registeredTelephone')
    _address = listQueryInvoice.get('billingAddress').get('address')
    _department = listQueryInvoice.get('billingAddress').get('province') or 'Bogotá'
    _postalCode = listQueryInvoice.get('billingAddress').get('zipcode') or '110111'
    _cityName = listQueryInvoice.get('billingAddress').get('city') or 'BOGOTÁ, D.C.'
    _countryCode = listQueryInvoice.get('billingAddress').get('country')
    _firstName = listQueryInvoice.get('billingAddress').get('vatFirstName') or 'DMZ'
    _lastName = listQueryInvoice.get('billingAddress').get('vatLastName') or "dmz"

    if _id == "":
        _type = 'PERSONA_NATURAL';
        _id = validaID(listQueryInvoice.get('businessInfo').get('idCardNumber'),_idType)
        _idType = _idType
        _idExport	= validaEX(listQueryInvoice.get('businessInfo').get('idCardNumber'),_idType)
        _department = _department
        _cityName = _cityName
        _postalCode = _postalCode
        _address = _address
        _countryCode = _countryCode
        _email = _email
        _telephone = _telephone
        _firstName = _name
        _lastName = _lastName
    else:
        _type = 'PERSONA_JURIDICA';
        _id = _id
        _idType = _idType
        _idExport	= ''
        _department = _department
        _cityName = _cityName
        _postalCode = _postalCode
        _address = _address
        _countryCode = _countryCode
        _email = _email
        _telephone = _telephone
        _firstName = _firstName
        _lastName = _lastName
    customer = {
        'type': _type,
        'id': _id,
        'idType': _idType,
        'idExport': _idExport,
        'name': _name,
        'department': _department,
        'cityName': _cityName,
        'postalCode': _postalCode,
        'address': _address,
        'countryCode': _countryCode,
        'email': _email,
        'telephone': _telephone,
        'firstName': _firstName,
        'lastName': _lastName
    }
    return customer


def gift(valor, percent):
    if valor == '' or valor == 0.0 or valor == 0.00:
        if percent == 100.0 or percent == 100.00:
            _isGift = True
        else:
            _isGift = False
    else:
        _isGift = False
    return _isGift

def get_items(_items):
    base_exenta = 0
    base_iva = 0;
    importe_iva = 0;
    importe_exento = 0;
    hasGift = False;
    invoice_items = []
    items = []
    giftAmount = 0
    _impuesto1 = 0
    for item in _items:
        #print "item ", item
        invoice_items.append({
            'id': item.get('id'),
            'spartName': item.get('spartName'),
            #decimals function
            'price': decimals(item.get('unitPriceNtax')),
            'quantity': item.get('quantity'),
            'discountAmount': decimals(item.get('noTaxDiscountAmount')),
            'taxRate': decimals(item.get('taxRate')),
            'taxAmount': decimals(item.get('taxAmount')),
        })
        _itemCode = item.get('skuCode')
        _payUnitPrice = item.get('payUnitPrice')
        _quantity = item.get('quantity')
        _price = item.get('price')
        _name = item.get('spartName')
        _description = item.get('spartName')
        _impuesto = item.get('taxRate')
        #decimals function
        _amount = decimals(_price/(1+_impuesto))
        _discountAmount = decimals((_price - _payUnitPrice)*_quantity)
        if item.get('taxRate') == 0:
            if _payUnitPrice == 0:
                _taxableAmount = 0
            else:
                #decimals function
                _taxableAmount = decimals(_quantity*_amount)-decimals(_discountAmount/(1+_impuesto))
            if _taxableAmount == 0:
                _discountPercent = 100
            else:
                #decimals function
                _discountPercent = decimals(((abs(_taxableAmount)/decimals(abs(_quantity)*abs(_amount)))-1)*(-100))
            items.append({
                'itemType': 'EXENTO',
                'itemCode': _itemCode,
                'quantity': _quantity,
                'amount': _amount,
                'name': _name,
                'description': _description,
                'isGift': gift(_payUnitPrice, _discountPercent),
                'taxableAmount': _taxableAmount,
                #decimals function
                'totalTax': decimals(_taxableAmount * (_impuesto)),
                #decimals function
                'total': decimals(_taxableAmount * (1 + _impuesto)),
                'clasificationId': '',
                'clasificationName': '',
                'observation': '',
                'discountPercentage': _discountPercent,
                'discountAmount': _discountAmount,
            })
            base_exenta = base_exenta + _taxableAmount;
            importe_exento = 0;
            #decimals function
            giftAmount = giftAmount + decimals(_taxableAmount) + decimals(_taxableAmount * _impuesto);
        else:
            _impuesto1 = _impuesto;
            if _payUnitPrice > 0:
                if _payUnitPrice == 0:
                    _taxableAmount = 0;
                else:
                    #decimals function
                    _taxableAmount = decimals((_quantity * _amount)-decimals((_discountAmount)/(1 + _impuesto)));
                if _taxableAmount == 0:
                    _discountPercent = 100;
                else:
                    #decimals function
                    _discountPercent =  decimals(((abs(_taxableAmount)/decimals(abs(_quantity)*abs(_amount)))-1)*(-100));
                items.append({
                    'itemType': 'GRAVADO',
                    'itemCode': _itemCode,
                    'quantity': _quantity,
                    'amount': _amount,
                    'name': _name,
                    'description': _description,
                    'isGift': gift(_payUnitPrice, _discountPercent),
                    'taxableAmount': _taxableAmount,
                    #decimals function
                    'totalTax': decimals(_taxableAmount * (_impuesto)),
                    #decimals function
                    'total': decimals(_taxableAmount * (1 + _impuesto)),
                    'clasificationId': '',
                    'clasificationName': '',
                    'observation': '',
                    'discountPercentage': _discountPercent,
                    #decimals function
                    'discountAmount': decimals((_discountAmount) / (1 + _impuesto)),
                    'taxes': [{
                                #{
                                    'type': 'IVA',
                                    #decimals function
                                    'amount': decimals(_taxableAmount * _impuesto),
                                    #decimals function
                                    'taxableAmount': decimals(_taxableAmount),
                                    #decimals function
                                    'percentage': decimals(_impuesto * 100)
                                #}
                            }],
                })
                base_iva = base_iva + _taxableAmount
                #decimals function
                importe_iva = importe_iva + decimals(_taxableAmount * _impuesto)
                #decimals function
                giftAmount = giftAmount + decimals(_taxableAmount) + decimals(_taxableAmount * _impuesto)
            else:
                hasGift = True
                items.append({
                    'itemType': 'GRAVADO',
                    'itemCode': _itemCode,
                    'quantity': _quantity,
                    'amount': _amount,
                    'name'	: _name,
                    'description': _description,
                    'isGift': True,
                    #decimals function
                    'taxableAmount': decimals(_quantity * _amount),
                    #decimals function
                    'totalTax': decimals(_quantity * _amount * (_impuesto)),
                    #decimals function
                    'total': decimals(_quantity * _amount * (_impuesto)),
                    'clasificationId': '',
                    'clasificationName': '',
                    'observation': '',
                    'discountPercentage': 0,
                    'discountAmount': 0,
                    'taxes': [{
                                #{
                                    'type': 'IVA',
                                    #decimals function
                                    'amount': decimals(_quantity * _amount * (_impuesto)),
                                    #decimals function
                                    'taxableAmount': decimals(_quantity * _amount),
                                    #decimals function
                                    'percentage': decimals(_impuesto * 100)
                                #}
                            }],
                })
                #decimals function
                base_iva = base_iva + decimals(_quantity * _amount)
                importe_iva = importe_iva + decimals(_quantity * _amount * (_impuesto))
                giftAmount = giftAmount + decimals(_quantity * _amount * (_impuesto))
    returning = {
            "exento": {"base": base_exenta, "impuesto": importe_exento},
            "iva": {"base": base_iva,
                    #decimals function
                    "impuesto": decimals(base_iva * _impuesto1),
                    "gift": hasGift, "with_gift_amount": giftAmount},
    }
    return items, returning, invoice_items


def payform(valor):
    if valor == '1':
        res = "CONTADO"
    return res


def paymethod(valor):
    if valor == '47':
        res = "TRANSFERENCIA"
    return res