# -*- coding: utf-8 -*-
import threading
from threading import *
import time
import datetime
import logging


LOG = "/var/log/vde/multi_hilos.log"
logging.basicConfig(filename=LOG, filemode="w", level=logging.DEBUG,
    format='[%(levelname)s] (%(threadName)-s) %(message)s')


def invoices():
    logging.info("Thread INVOICES inicializado " + str(datetime.datetime.now()))
    time.sleep(10)
    #logging.info("Thread suma "+str(a)+"+"+str(b)+"="+str(c))
    #logging.info("Thread INVOICES terminado " + str(datetime.datetime.now()))


def thread_invoice():
    logging.info("Thread HILO inicializado " + str(datetime.datetime.now()))
    while True:
        t1 = threading.Thread(name=datetime.datetime.now(), target=invoices)
        t1.start()
        t1.join()


def nc():
    logging.info("Thread NC inicializado " + str(datetime.datetime.now()))
    time.sleep(5)
    #logging.info("Thread suma "+str(a)+"+"+str(b)+"="+str(c))
    #logging.info("Thread NC terminado " + str(datetime.datetime.now()))


def thread_nc():
    logging.info("Thread HILO NC inicializado " + str(datetime.datetime.now()))
    while True:
        t1 = threading.Thread(name=datetime.datetime.now(), target=nc)
        t1.start()
        t1.join()


def callback():
    logging.info("Thread CALLBACK inicializado " + str(datetime.datetime.now()))
    time.sleep(3)
    #logging.info("Thread suma "+str(a)+"+"+str(b)+"="+str(c))
    #logging.info("Thread CALLBACK terminado " + str(datetime.datetime.now()))


def thread_callback():
    logging.info("Thread HILO CALLBACK inicializado " + str(datetime.datetime.now()))
    while True:
        t1 = threading.Thread(name=datetime.datetime.now(), target=callback)
        t1.start()
        t1.join()


if __name__ == '__main__':
    t1 = threading.Thread(name="hilo invoice", target=thread_invoice)
    t1.start()
    t2 = threading.Thread(name="hilo nc", target=thread_nc)
    t2.start()
    t3 = threading.Thread(name="hilo callback", target=thread_callback)
    t3.start()